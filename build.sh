#!/bin/bash
cd Parser

# create ast
mkdir -p spec-gen
for f in spec/*.rats
do
    f=$(basename "$f")
    grep "^////" spec/$f > spec-gen/${f%.*}.ast
done
sed -i 's|//// ||' spec-gen/*.ast

# run jastadd
mkdir -p src-gen/org/tud/forty/ast
sed -i 's|class PrettyPrint|aspect PrettyPrint|' spec/Printing.jadd
cd src-gen
java -jar ../tools/jastadd/jastadd2.jar --package="org.tud.forty.ast" --indent='4space' ../spec-gen/*.ast ../spec/*.jadd
cd ..
sed -i 's|aspect PrettyPrint|class PrettyPrint|' spec/Printing.jadd

# run rats
mkdir -p src-gen/org/tud/forty/parser
cd spec
java -jar ../tools/rats.jar -out ../src-gen/org/tud/forty/parser SlottableFortranParser.rats
cd ..
