#!/usr/bin/env python3
# -*- coding: utf-8 -*-

for l in range(97,97+26):
    letter = chr(l)
    LETTER = letter.upper()
    print("transient void {1} = ('{0}'/'{1}');".format(letter, LETTER))


rules = list()

with open("t.rats") as f:
    for i in f.readlines():
        w = i.rstrip().split(" ")
        w.append(" ".join(list(w[-2])))
        w.append("SS;")
        rules.append(" ".join(w))

rules.sort()

for line in rules:
    line = line.replace(" _ ", " UNDERSCORE ")
    print(line)
