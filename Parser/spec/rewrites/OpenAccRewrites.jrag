aspect OpenAccRewrites {
  rewrite AccParallelLoopConstruct {
    to AccParallelConstruct {
      List<AccClause> parallelClauses = new List<AccClause>();
      List<AccClause> loopClauses = new List<AccClause>();
      for (AccClause clause : getAccParallelLoopStmt().getAccClauseList()) {
        if (clause.prefersLoop()) {
          loopClauses.add(clause);
        } else {
          parallelClauses.add(clause);
        }
      }
      AccLoopConstruct loop = new AccLoopConstruct(new AccLoopStmt(loopClauses), getDoConstruct(), new Opt<>(new AccEndLoopStmt()));
      return new AccParallelConstruct(
          new AccParallelStmt(parallelClauses),
          new Block(new List<ExecutionPartConstruct>().add(new ExecutableConstruct_ExecutionPartConstruct(loop))),
          new AccEndParallelStmt()
          );
    }
  }

  rewrite AccKernelsLoopConstruct {
    to AccKernelsConstruct {
      List<AccClause> kernelsClauses = new List<AccClause>();
      List<AccClause> loopClauses = new List<AccClause>();
      for (AccClause clause : getAccKernelsLoopStmt().getAccClauseList()) {
        if (clause.prefersLoop()) {
          loopClauses.add(clause);
        } else {
          kernelsClauses.add(clause);
        }
      }
      AccLoopConstruct loop = new AccLoopConstruct(new AccLoopStmt(loopClauses), getDoConstruct(), new Opt<>(new AccEndLoopStmt()));
      return new AccKernelsConstruct(
          new AccKernelsStmt(kernelsClauses),
          new Block(new List<ExecutionPartConstruct>().add(new ExecutableConstruct_ExecutionPartConstruct(loop))),
          new AccEndKernelsStmt()
      );
    }
  }

  syn boolean AccClause.prefersLoop();
  eq AccDataClause.prefersLoop() = false;
  eq AccClause.prefersLoop() = false;
  eq AccCollapseClause.prefersLoop() = true;
  eq AccExecutionModeClause.prefersLoop() = true;
  eq AccSeqClause.prefersLoop() = true;
  eq AccAutoClause.prefersLoop() = true;
  eq AccTileClause.prefersLoop() = true;
  eq AccDeviceTypeClause.prefersLoop() = true;
  eq AccIndependentClause.prefersLoop() = true;
  eq AccPrivateClause.prefersLoop() = true;
  eq AccReductionClause.prefersLoop() = true;

  rewrite AccStmt {
    when (containedRemovableClause() != null)
    to AccStmt {
      ASTNode parent = containedRemovableClause().getParent();
      int i = parent.getIndexOfChild(containedRemovableClause());
      if(i>-1){
        parent.removeChild(i);
      }
      return this;
    }
  }

  syn AccClause AccStmt.containedRemovableClause();
  eq AccStmt.containedRemovableClause() = null;
  eq AccAbstractDataStmt.containedRemovableClause() {
    for (AccClause clause: getAccClauseList()) {
      if (clause.canBeRemoved()) {
        return clause;
      }
    }
    return null;
  }
  eq AccLoopStmt.containedRemovableClause() {
    for (AccClause clause: getAccClauseList()) {
      if (clause.canBeRemoved()) {
        return clause;
      }
    }
    return null;
  }

  syn boolean AccClause.canBeRemoved() = false;
  eq AccDataClause.canBeRemoved() {
    return (getVariableList().numChildren() == 0);
  }
  eq AccCollapseClause.canBeRemoved() {
    return getCount().getDigitString().getDigits().equals("1");
  }

  rewrite AccLoopStmt {
    when (containedRemovableClause() != null)
    to AccLoopStmt {
    ASTNode parent = containedRemovableClause().getParent();
      int i = parent.getIndexOfChild(containedRemovableClause());
      if(i>-1){
        parent.removeChild(i);
      }
      return this;
    }
  }
}