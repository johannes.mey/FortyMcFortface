aspect Debug {

    public String ASTNode.getAstStringWithSource() {
        String[] lines = getAstString(true).split("\n");
        StringBuilder out = new StringBuilder();
        for (String line : lines) {
            String[] pair = line.split("<<COLUMN>>");
            String left = pair[0];
            String right = (pair.length>1) ? pair[1] : "";
            left = String.format("%-" + (left.length()/60+1)*60 + "s", left);
            out.append(left).append(right).append("\n");
        }
        return out.toString();
    }

    public String ASTNode.getAstString() {
        return getAstString(new StringBuilder(), new StringBuffer(), false).toString();
    }

    protected String ASTNode.getAstString(boolean source) {
        return getAstString(new StringBuilder(), new StringBuffer(), source).toString();
    }

    public StringBuilder ASTNode.getAstString(StringBuilder s, StringBuffer indent) {
        return this.getAstString(s, indent, false);
    }

    protected StringBuilder ASTNode.getAstString(StringBuilder s, StringBuffer indent, boolean source) {
        if (source) {
            this.getNodeTypeSource(s);
        } else {
            s.append(getNodeType());
        }
        s.append("\n");

        if (getNumChildNoTransform() > 0) {
            for(int childIndex = 0; childIndex < getNumChildNoTransform(); childIndex++) {
                if (childIndex < getNumChildNoTransform() - 1) {
                    s.append(indent).append("\u251c\u2574");
                } else {
                    s.append(indent).append("\u2570\u2574");
                }

                ASTNode<?> child = getChildNoTransform(childIndex);
                if (child==null) {
                    s.append("NULL\n");
                } else {
                    if (childIndex < getNumChildNoTransform() - 1) {
                        indent.append("\u2502 ");
                    } else {
                        indent.append("  ");
                    }
                    child.getAstString(s, indent, source);
                    indent.delete(indent.length()-2, indent.length());
                }
            }
        }
        return s;
    }

    protected String Opt.getNodeType() {
        // TODO print the opt type
        return this.getClass().getSimpleName();
    }

    protected String List.getNodeType() {
        // TODO print the list type
        return this.getClass().getSimpleName();
    }

    protected String ASTNode.getNodeType() {
        return this.getClass().getSimpleName();
    }

    protected StringBuilder Opt.getNodeTypeSource(StringBuilder b) {
        return b.append(this.getNodeType());
    }

    protected StringBuilder List.getNodeTypeSource(StringBuilder b) {
        return b.append(this.getNodeType());
    }

    protected StringBuilder ASTNode.getNodeTypeSource(StringBuilder b) {
        b.append(this.getNodeType()).append(" <<COLUMN>>");
        PrettyPrinter sw = new PrettyPrinter("  ", false);
        try {
            this.prettyPrint(sw);
            b.append(sw.toString().trim().split("\n")[0]);
        } catch (RuntimeException e) {
            b.append(e.getMessage());
        }
        return b;
    }

    protected String ASTNode.getPrettyPrintLine() {
        PrettyPrinter sw = new PrettyPrinter("  ", false);
        try {
            this.prettyPrint(sw);
            return sw.toString().trim().split("\n")[0];
        } catch (RuntimeException e) {
            return e.getMessage();
        }
    }
}
