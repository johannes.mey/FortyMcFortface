module OpenAcc();

modify ConditionalLines;
import OpenAccTokens;

// allow OpenACC constructs as executable_constructs
ExecutableConstruct executable_construct +=
    <AccConstruct> acc_construct
/   <AccDoConstruct> acc_do_construct
/   <BEGINNING> ...
;

// allow OpenACC directives as action statements
ExecutableConstruct executable_construct +=
    <AccDirective> acc_directive
/   <BEGINNING> ...
;

DoConstruct do_construct +=
    <AccLoopConstruct> acc_loop_construct
/   <BEGINNING> ...
;

// =============================================================================
//                               OpenACC
// =============================================================================

List<Variable> var_list =
    v:variable vl:(COMMA yyValue:variable)*
        {
            yyValue = new List().add(v).addAll(vl.list());
        }
;

List<Variable> p_var_list =
    LPAREN v:variable vl:(COMMA yyValue:variable)* RPAREN
        {
            yyValue = new List().add(v).addAll(vl.list());
        }
/   LPAREN RPAREN
        {
            yyValue = new List();
        }
;

List<Expr> expr_list =
    e:expr el:(COMMA yyValue:expr)*
        {
            yyValue = new List().add(e).addAll(el.list());
        }
;

//// // An AccStmt does not have a label or a comment (unlike Fortran statements).
//// // It must, however, begin with ""!$acc" and end with a line break
//// abstract AccStmt ;
//// abstract AccAbstractDataStmt:AccStmt ::= AccClause* ;

// constructs ------------------------------------------------------------------

//// abstract AccConstruct:ExecutableConstruct ;
AccConstruct acc_construct =
 	  acc_parallel_construct
/   acc_kernels_construct
/   acc_data_construct
/   acc_atomic_construct
/   acc_host_data_construct
;

//// // ACC 2.5.1
//// AccParallelConstruct:AccConstruct ::= AccParallelStmt Block AccEndParallelStmt ;
AccParallelConstruct acc_parallel_construct =
	p:acc_parallel_stmt
	b:block
	e:acc_end_parallel_stmt
		{
			yyValue = new AccParallelConstruct(p, b, e);
		}
;

//// AccParallelStmt:AccAbstractDataStmt ;
AccParallelStmt acc_parallel_stmt =
    A_P A_PARALLEL LB
        {
            yyValue = new AccParallelStmt(new List());
        }
/   A_P A_PARALLEL cl:acc_clause_list LB
        {
            yyValue = new AccParallelStmt(cl);
        }
;

//// AccEndParallelStmt:AccStmt ;
AccEndParallelStmt acc_end_parallel_stmt =
    A_P A_END A_PARALLEL LB
        {
            yyValue = new AccEndParallelStmt();
        }
;

//// // ACC 2.5.2
//// AccKernelsConstruct:AccConstruct ::= AccKernelsStmt Block AccEndKernelsStmt ;
AccKernelsConstruct acc_kernels_construct =
	p:acc_kernels_stmt
	b:block
	e:acc_end_kernels_stmt
		{
			yyValue = new AccKernelsConstruct(p, b, e);
		}
;

//// AccKernelsStmt:AccAbstractDataStmt ;
AccKernelsStmt acc_kernels_stmt =
    A_P A_KERNELS LB
        {
            yyValue = new AccKernelsStmt(new List());
        }
/   A_P A_KERNELS cl:acc_clause_list LB
        {
            yyValue = new AccKernelsStmt(cl);
        }
;

//// AccEndKernelsStmt:AccStmt ;
AccEndKernelsStmt acc_end_kernels_stmt =
    A_P A_END A_KERNELS LB
        {
            yyValue = new AccEndKernelsStmt();
        }
;

//// // ACC 2.6.3
//// AccDataConstruct:AccConstruct ::= AccDataStmt Block AccEndDataStmt ;
AccDataConstruct acc_data_construct =
	p:acc_data_stmt
	b:block
	e:acc_end_data_stmt
		{
			yyValue = new AccDataConstruct(p, b, e);
		}
;

//// AccDataStmt:AccAbstractDataStmt ;
AccDataStmt acc_data_stmt =
    A_P A_DATA LB
        {
            yyValue = new AccDataStmt(new List());
        }
/   A_P A_DATA cl:acc_clause_list LB
        {
            yyValue = new AccDataStmt(cl);
        }
;

//// AccEndDataStmt:AccStmt ;
AccEndDataStmt acc_end_data_stmt =
    A_P A_END A_DATA LB
        {
            yyValue = new AccEndDataStmt();
        }
;

//// // ACC 2.8
//// AccHostDataConstruct:AccConstruct ::= AccHostDataStmt Block AccEndHostDataStmt ;
AccHostDataConstruct acc_host_data_construct =
	p:acc_host_data_stmt
	b:block
	e:acc_end_host_data_stmt
		{
			yyValue = new AccHostDataConstruct(p, b, e);
		}
;

//// AccHostDataStmt:AccStmt ::= AccClause* ;
AccHostDataStmt acc_host_data_stmt =
    A_P A_HOST_DATA LB
        {
            yyValue = new AccHostDataStmt(new List());
        }
/   A_P A_HOST_DATA cl:acc_clause_list LB
        {
            yyValue = new AccHostDataStmt(cl);
        }
;

//// AccEndHostDataStmt:AccStmt ;
AccEndHostDataStmt acc_end_host_data_stmt =
    A_P A_END A_HOST_DATA LB
        {
            yyValue = new AccEndHostDataStmt();
        }
;


//// // ACC 2.12
//// abstract AccAtomicConstruct:AccConstruct ;
//// AccUnaryAtomicConstruct:AccAtomicConstruct ::= AccUnaryAtomicStmt ActionStmt [AccEndAtomicStmt] ;
//// AccAtomicCaptureConstruct:AccAtomicConstruct ::= AccAtomicCaptureStmt First:ActionStmt Second:ActionStmt AccEndAtomicStmt ;
AccAtomicConstruct acc_atomic_construct =
    acc_atomic_capture_construct
/   acc_unary_atomic_construct
;

AccUnaryAtomicConstruct acc_unary_atomic_construct =
	p:acc_unary_atomic_stmt
	s:action_stmt
	e:acc_end_atomic_stmt?
		{
		    Opt<AccEndAtomicStmt> e_opt = (e==null) ? new Opt<AccEndAtomicStmt>() : new Opt<AccEndAtomicStmt>(e);
			yyValue = new AccUnaryAtomicConstruct(p, s, e_opt);
		}
;

AccAtomicCaptureConstruct acc_atomic_capture_construct =
	p:acc_atomic_capture_stmt
	f:action_stmt
	s:action_stmt
	e:acc_end_atomic_stmt
		{
			yyValue = new AccAtomicCaptureConstruct(p, f, s, e);
		}
;

//// abstract AccUnaryAtomicStmt:AccStmt ;
//// AccReadAtomicStmt:AccUnaryAtomicStmt ;
//// AccWriteAtomicStmt:AccUnaryAtomicStmt ;
//// AccUpdateAtomicStmt:AccUnaryAtomicStmt ;
AccUnaryAtomicStmt acc_unary_atomic_stmt =
    A_P A_ATOMIC A_READ LB
        {
            yyValue = new AccReadAtomicStmt();
        }
/   A_P A_ATOMIC A_WRITE LB
        {
            yyValue = new AccWriteAtomicStmt();
        }
/   A_P A_ATOMIC A_UPDATE? LB
        {
            yyValue = new AccUpdateAtomicStmt();
        }
;

//// AccAtomicCaptureStmt:AccStmt ;
AccAtomicCaptureStmt acc_atomic_capture_stmt =
    A_P A_ATOMIC A_CAPTURE LB
        {
            yyValue = new AccAtomicCaptureStmt();
        }
;

//// AccEndAtomicStmt:AccStmt ;
AccEndAtomicStmt acc_end_atomic_stmt =
    A_P A_END A_ATOMIC LB
        {
            yyValue = new AccEndAtomicStmt();
        }
;

// loop constructs -------------------------------------------------------------
//// abstract AccDoConstruct:DoConstruct ::= DoConstruct ;
AccDoConstruct acc_do_construct =
    acc_loop_construct
/   acc_parallel_loop_construct
/   acc_kernels_loop_construct
;


//// // ACC 2.9
//// AccLoopConstruct:AccDoConstruct ::= AccLoopStmt DoConstruct [AccEndLoopStmt] ;
AccLoopConstruct acc_loop_construct =
	p:acc_loop_stmt
	d:do_construct
	e:acc_end_loop_stmt?
		{
		    Opt<AccEndLoopStmt> e_opt = (e==null) ? new Opt<AccEndLoopStmt>() : new Opt<AccEndLoopStmt>(e);
			yyValue = new AccLoopConstruct(p, d, e_opt);
		}
;

//// AccLoopStmt:AccStmt ::= AccClause* ;
AccLoopStmt acc_loop_stmt =
    A_P A_LOOP LB
        {
            yyValue = new AccLoopStmt(new List());
        }
/   A_P A_LOOP cl:acc_clause_list LB
        {
            yyValue = new AccLoopStmt(cl);
        }
;

//// AccEndLoopStmt:AccStmt ;
AccEndLoopStmt acc_end_loop_stmt =
    A_P A_END A_LOOP LB
        {
            yyValue = new AccEndLoopStmt();
        }
;

//// // ACC 2.11
//// AccParallelLoopConstruct:AccDoConstruct ::= AccParallelLoopStmt DoConstruct [AccEndParallelLoopStmt] ;
AccParallelLoopConstruct acc_parallel_loop_construct =
	p:acc_parallel_loop_stmt
	d:do_construct
	e:acc_end_parallel_loop_stmt?
		{
		    Opt<AccEndParallelLoopStmt> e_opt = (e==null) ? new Opt<AccEndParallelLoopStmt>() : new Opt<AccEndParallelLoopStmt>(e);
			yyValue = new AccParallelLoopConstruct(p, d, e_opt);
		}
;

//// AccParallelLoopStmt:AccAbstractDataStmt ;
AccParallelLoopStmt acc_parallel_loop_stmt =
    A_P A_PARALLEL A_LOOP LB
        {
            yyValue = new AccParallelLoopStmt(new List());
        }
/   A_P A_PARALLEL A_LOOP cl:acc_clause_list LB
        {
            yyValue = new AccParallelLoopStmt(cl);
        }
;

//// AccEndParallelLoopStmt:AccStmt ;
AccEndParallelLoopStmt acc_end_parallel_loop_stmt =
    A_P A_END A_PARALLEL A_LOOP LB
        {
            yyValue = new AccEndParallelLoopStmt();
        }
;

//// AccKernelsLoopConstruct:AccDoConstruct ::= AccKernelsLoopStmt DoConstruct [AccEndKernelsLoopStmt] ;
AccKernelsLoopConstruct acc_kernels_loop_construct =
	p:acc_kernels_loop_stmt
	d:do_construct
	e:acc_end_kernels_loop_stmt?
		{
			Opt<AccEndKernelsLoopStmt> e_opt = (e==null) ? new Opt<AccEndKernelsLoopStmt>() : new Opt<AccEndKernelsLoopStmt>(e);
            yyValue = new AccKernelsLoopConstruct(p, d, e_opt);
		}
;

//// AccKernelsLoopStmt:AccAbstractDataStmt ;
AccKernelsLoopStmt acc_kernels_loop_stmt =
    A_P A_KERNELS A_LOOP LB
        {
            yyValue = new AccKernelsLoopStmt(new List());
        }
/   A_P A_KERNELS A_LOOP cl:acc_clause_list LB
        {
            yyValue = new AccKernelsLoopStmt(cl);
        }
;

//// AccEndKernelsLoopStmt:AccStmt ;
AccEndKernelsLoopStmt acc_end_kernels_loop_stmt =
    A_P A_END A_KERNELS A_LOOP LB
        {
            yyValue = new AccEndKernelsLoopStmt();
        }
;

// action statements --------------------------------------------------------------

//// abstract AccDirective:ActionStmt ;
AccDirective acc_directive =
    acc_enter_data_directive
/   acc_exit_data_directive
/   acc_cache_directive
/   acc_update_directive
/   acc_declare_directive
/   acc_routine_directive
/   acc_wait_directive
;

//// // ACC 2.6.4
//// AccEnterDataDirective:AccDirective ::= AccClause* ;
AccEnterDataDirective acc_enter_data_directive =
	A_P A_ENTER A_DATA clauses:acc_clause_list LB
		{
			yyValue = new AccEnterDataDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), clauses);
		}
/	A_P A_ENTER A_DATA LB
		{
			yyValue = new AccEnterDataDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List());
		}
;

//// AccExitDataDirective:AccDirective ::= AccClause* ;
AccExitDataDirective acc_exit_data_directive =
	A_P A_EXIT A_DATA clauses:acc_clause_list LB
		{
			yyValue = new AccExitDataDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), clauses);
		}
/	A_P A_EXIT A_DATA LB
		{
			yyValue = new AccExitDataDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List());
		}
;

//// // ACC 2.10
//// AccCacheDirective:AccDirective ::= Variable* ;
AccCacheDirective acc_cache_directive =
	A_P A_CACHE list:p_var_list LB
		{
			yyValue = new AccCacheDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), list);
		}
;

//// // ACC 2.14.4
//// AccUpdateDirective:AccDirective ::= AccClause* ;
AccUpdateDirective acc_update_directive =
	A_P A_UPDATE clauses:acc_clause_list LB
		{
			yyValue = new AccUpdateDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), clauses);
		}
/	A_P A_UPDATE LB
		{
			yyValue = new AccUpdateDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List());
		}
;



//// // ACC 2.13
//// AccDeclareDirective:AccDirective ::= AccClause* ;
AccDeclareDirective acc_declare_directive =
	A_P A_DECLARE clauses:acc_clause_list LB
		{
			yyValue = new AccDeclareDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), clauses);
		}
/	A_P A_DECLARE LB
		{
			yyValue = new AccDeclareDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List());
		}
;

//// // ACC 2.15.1
//// AccRoutineDirective:AccDirective ::= [Name] AccClause* ;
AccRoutineDirective acc_routine_directive =
 	A_P A_ROUTINE n:(LPAREN yyValue:name RPAREN)? clauses:acc_clause_list LB
		{
		    Opt<Name> n_opt = (n==null) ? new Opt<Name>() : new Opt<Name>(n);
			yyValue = new AccRoutineDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), n_opt, clauses);
		}
/	A_P A_ROUTINE n:(LPAREN yyValue:name RPAREN) LB
		{
		    Opt<Name> n_opt = (n==null) ? new Opt<Name>() : new Opt<Name>(n);
			yyValue = new AccRoutineDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), n_opt, new List());
		}
;

//// // ACC 2.16.3
//// AccWaitDirective:AccDirective ::= Expr* AccClause* ;
AccWaitDirective acc_wait_directive =
	A_P A_WAIT clauses:acc_clause_list LB
		{
			yyValue = new AccWaitDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List(), clauses);
		}
/	A_P A_WAIT LPAREN list:expr_list RPAREN clauses:acc_clause_list LB
		{
			yyValue = new AccWaitDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), list, clauses);
		}
/	A_P A_WAIT LB
		{
			yyValue = new AccWaitDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), new List(), new List());
		}
/	A_P A_WAIT  LPAREN list:expr_list RPAREN LB
		{
			yyValue = new AccWaitDirective(new Opt<Label>(), new Comment(new List<CommentContent>()), list, new List());
		}
;

// clauses ---------------------------------------------------------------------

List acc_clause_list =
	first:acc_clause clauses:(COMMA? yyValue:acc_clause)*
	{
		yyValue = new List().add(first).addAll(clauses.list());
	}
;

//// abstract AccClause ;
public AccClause acc_clause =
<BEGINNING>
	acc_if_clause
/	acc_reduction_clause
/	acc_device_clause
/	acc_firstprivate_clause
/	acc_private_clause
/	acc_use_device_clause
/	acc_default_none_clause
/	acc_device_type_clause
/	acc_tile_clause
/	acc_wait_clause
/	acc_collapse_clause
/	acc_async_clause
/	acc_num_gangs_clause
/	acc_num_workers_clause
/	acc_vector_length_clause
/	acc_independent_clause
/	acc_gang_clause
/	acc_worker_clause
/	acc_vector_clause
/	acc_host_clause
/	acc_self_clause
/	acc_device_resident_clause
/	acc_link_clause
/	acc_nohost_clause
/	acc_bind_clause
/	acc_seq_clause
/	acc_shortloop_clause
/ acc_auto_clause
/ acc_data_clause
;

//// abstract AccDataClause:AccClause ::= Variable* ;
AccDataClause acc_data_clause =
  acc_deviceptr_clause
/	acc_present_clause
/	acc_copy_clause
/	acc_copyin_clause
/	acc_copyout_clause
/	acc_create_clause
/	acc_delete_clause
;

//// AccDeviceptrClause:AccDataClause ;
AccDeviceptrClause acc_deviceptr_clause =
    A_DEVICEPTR list:p_var_list
        {
            yyValue = new AccDeviceptrClause(list);
        }
;

//// AccPresentClause:AccDataClause ;
AccPresentClause acc_present_clause =
	A_PRESENT list:p_var_list
		{
			yyValue = new AccPresentClause(list);
		}
;

//// AccCopyClause:AccDataClause ;
//// AccPresentOrCopyClause:AccCopyClause ;
AccCopyClause acc_copy_clause =
	A_COPY list:p_var_list
		{
			yyValue = new AccCopyClause(list);
		}
/	(A_PCOPY / A_PRESENT_OR_COPY) list:p_var_list
		{
			yyValue = new AccPresentOrCopyClause(list);
		}
;

//// AccCopyInClause:AccDataClause ;
//// AccPresentOrCopyInClause:AccCopyInClause ;
AccCopyInClause acc_copyin_clause =
	A_COPYIN list:p_var_list
		{
			yyValue = new AccCopyInClause(list);
		}
/	(A_PCOPYIN / A_PRESENT_OR_COPYIN) list:p_var_list
		{
			yyValue = new AccPresentOrCopyInClause(list);
		}
;

//// AccCopyOutClause:AccDataClause ;
//// AccPresentOrCopyOutClause:AccCopyOutClause ;
AccCopyOutClause acc_copyout_clause =
	A_COPYOUT list:p_var_list
		{
			yyValue = new AccCopyOutClause(list);
		}
/	(A_PCOPYOUT / A_PRESENT_OR_COPYOUT) list:p_var_list
		{
			yyValue = new AccPresentOrCopyOutClause(list);
		}
;

//// AccCreateClause:AccDataClause ;
//// AccPresentOrCreateClause:AccCreateClause ;
AccCreateClause acc_create_clause =
	A_CREATE list:p_var_list
		{
			yyValue = new AccCreateClause(list);
		}
/	(A_PCREATE / A_PRESENT_OR_CREATE) list:p_var_list
		{
			yyValue = new AccPresentOrCreateClause(list);
		}
;

//// AccDeleteClause:AccDataClause ;
AccDeleteClause acc_delete_clause =
	A_DELETE list:p_var_list
		{
			yyValue = new AccDeleteClause(list);
		}
;

//// AccIfClause:AccClause ::= Condition:Expr ;
AccIfClause acc_if_clause =
	A_IF  LPAREN condition:expr RPAREN
		{
			yyValue = new AccIfClause(condition);
		}
;

//// AccAutoClause:AccClause ;
AccAutoClause acc_auto_clause =
	A_AUTO
		{
			yyValue = new AccAutoClause();
		}
;

//// AccReductionClause:AccClause ::= AccOperator Variable* ;
AccReductionClause acc_reduction_clause =
	A_REDUCTION LPAREN op:acc_operator COLON list:var_list RPAREN
		{
			yyValue = new AccReductionClause(op, list);
		}
;

//// abstract AccOperator ::= ;
//// AccOperatorPlus:AccOperator ::= ;
//// AccOperatorTimes:AccOperator ::= ;
//// AccOperatorMax:AccOperator ::= ;
//// AccOperatorMin:AccOperator ::= ;
//// AccOperatorIAnd:AccOperator ::= ;
//// AccOperatorIOr:AccOperator ::= ;
//// AccOperatorIEor:AccOperator ::= ;
//// AccOperatorAnd:AccOperator ::= ;
//// AccOperatorOr:AccOperator ::= ;
//// AccOperatorEqv:AccOperator ::= ;
//// AccOperatorNEqv:AccOperator ::= ;
AccOperator acc_operator =
	A_PLUS { yyValue = new AccOperatorPlus(); }
/	A_TIMES { yyValue = new AccOperatorTimes(); }
/	A_MAX { yyValue = new AccOperatorMax(); }
/	A_MIN { yyValue = new AccOperatorMin(); }
/	A_IAND { yyValue = new AccOperatorIAnd(); }
/	A_IOR { yyValue = new AccOperatorIOr(); }
/	A_IEOR { yyValue = new AccOperatorIEor(); }
/	A_AND { yyValue = new AccOperatorAnd(); }
/	A_OR { yyValue = new AccOperatorOr(); }
/	A_EQV { yyValue = new AccOperatorEqv(); }
/	A_NEQV { yyValue = new AccOperatorNEqv(); }
;

//// AccDeviceClause:AccClause ::= Variable* ;
AccDeviceClause acc_device_clause =
	A_DEVICE list:p_var_list
		{
			yyValue = new AccDeviceClause(list);
		}
;

//// AccFirstPrivateClause:AccClause ::= Variable* ;
AccFirstPrivateClause acc_firstprivate_clause =
	A_FIRSTPRIVATE list:p_var_list
		{
			yyValue = new AccFirstPrivateClause(list);
		}
;

//// AccPrivateClause:AccClause ::= Variable* ;
AccPrivateClause acc_private_clause =
	A_PRIVATE list:p_var_list
		{
			yyValue = new AccPrivateClause(list);
		}
;

//// AccUseDeviceClause:AccClause ::= Variable* ;
AccUseDeviceClause acc_use_device_clause =
	A_USEDEVICE list:p_var_list
		{
			yyValue = new AccUseDeviceClause(list);
		}
;

//// AccDefaultNoneClause:AccClause ::= ;
AccDefaultNoneClause acc_default_none_clause =
	A_DEFAULT LPAREN A_NONE RPAREN
		{
			yyValue = new AccDefaultNoneClause();
		}
;

//// abstract AccDeviceTypeClause:AccClause ::= ;
//// AccDeviceTypeStarClause:AccDeviceTypeClause ::= ;
//// AccDeviceTypeListClause:AccDeviceTypeClause ::= Variable* ;
AccDeviceTypeClause acc_device_type_clause =
	(A_DEVICE_TYPE / A_DTYPE) LPAREN ASTERISK RPAREN
		{
			yyValue = new AccDeviceTypeStarClause();
		}
/	(A_DEVICE_TYPE / A_DTYPE) list:p_var_list
		{
			yyValue = new AccDeviceTypeListClause(list);
		}
;

//// AccTileClause:AccClause ::= Expr* ;
AccTileClause acc_tile_clause =
	A_TILE LPAREN list:expr_list RPAREN
		{
			yyValue = new AccTileClause(list);
		}
;

//// AccWaitClause:AccClause ::= Expr* ;
AccWaitClause acc_wait_clause =
	A_WAIT LPAREN list:expr_list RPAREN
		{
			yyValue = new AccWaitClause(list);
		}
;

//// AccCollapseClause:AccClause ::= Count:IntLiteralConstant ;
AccCollapseClause acc_collapse_clause =
	A_COLLAPSE LPAREN n:int_literal_constant RPAREN
		{
			yyValue = new AccCollapseClause(n);
		}
;

//// AccAsyncClause:AccClause ::= [Expr] ;
AccAsyncClause acc_async_clause =
	A_ASYNC e:(LPAREN yyValue:expr RPAREN)?
		{
		    Opt<Expr> e_opt = (e==null) ? new Opt<Expr>() : new Opt<Expr>(e);
			yyValue = new AccAsyncClause(e_opt);
		}
;

//// AccNumGangsClause:AccClause ::= Expr ;
AccNumGangsClause acc_num_gangs_clause =
	A_NUMGANGS LPAREN e:expr RPAREN
		{
			yyValue = new AccNumGangsClause(e);
		}
;

//// AccNumWorkersClause:AccClause ::= Expr ;
AccNumWorkersClause acc_num_workers_clause =
	A_NUMWORKERS LPAREN e:expr RPAREN
		{
			yyValue = new AccNumWorkersClause(e);
		}
;

//// AccVectorLengthClause:AccClause ::= Expr ;
AccVectorLengthClause acc_vector_length_clause =
	A_VECTORLENGTH LPAREN e:expr RPAREN
		{
			yyValue = new AccVectorLengthClause(e);
		}
;

//// AccSelfClause:AccClause ::= Variable* ;
AccSelfClause acc_self_clause =
	A_SELF list:p_var_list
		{
			yyValue = new AccSelfClause(list);
		}
;

//// AccHostClause:AccClause ::= Variable* ;
AccHostClause acc_host_clause =
	A_HOST list:p_var_list
		{
			yyValue = new AccHostClause(list);
		}
;

//// AccIndependentClause:AccClause ;
AccIndependentClause acc_independent_clause =
	A_INDEPENDENT
		{
			yyValue = new AccIndependentClause();
		}
;

//// AccShortloopClause:AccClause ;
AccShortloopClause acc_shortloop_clause =
	A_SHORTLOOP
		{
			yyValue = new AccShortloopClause();
		}
;

//// AccNoHostClause:AccClause ;
AccNoHostClause acc_nohost_clause =
	A_NOHOST
		{
			yyValue = new AccNoHostClause();
		}
;

//// AccSeqClause:AccClause ;
AccSeqClause acc_seq_clause =
	A_SEQ
		{
			yyValue = new AccSeqClause();
		}
;

//// abstract AccExecutionModeClause:AccClause ::= [Num:IntLiteralConstant] ;
//// AccGangClause:AccExecutionModeClause ;
AccGangClause acc_gang_clause =
	A_GANG LPAREN n:int_literal_constant RPAREN
		{
			yyValue = new AccGangClause(new Opt<IntLiteralConstant>(n));
		}
/	A_GANG
		{
			yyValue = new AccGangClause(new Opt<IntLiteralConstant>());
		}
;

//// AccWorkerClause:AccExecutionModeClause;
AccWorkerClause acc_worker_clause =

	A_WORKER LPAREN n:int_literal_constant RPAREN
		{
			yyValue = new AccWorkerClause(new Opt<IntLiteralConstant>(n));
		}
/	A_WORKER
		{
			yyValue = new AccWorkerClause(new Opt<IntLiteralConstant>());
		}
;

//// AccVectorClause:AccExecutionModeClause ;
AccVectorClause acc_vector_clause =
	A_VECTOR LPAREN n:int_literal_constant RPAREN
		{
			yyValue = new AccVectorClause(new Opt<IntLiteralConstant>(n));
		}
/	A_VECTOR
		{
			yyValue = new AccVectorClause(new Opt<IntLiteralConstant>());
		}
;

//// AccDeviceResidentClause:AccClause ::= Variable* ;
AccDeviceResidentClause acc_device_resident_clause =
	A_DEVICE_RESIDENT list:p_var_list
		{
			yyValue = new AccDeviceResidentClause(list);
		}
;

//// AccLinkClause:AccClause ::= Variable* ;
AccLinkClause acc_link_clause =
	A_LINK list:p_var_list
		{
			yyValue = new AccLinkClause(list);
		}
;

//// abstract AccBindClause:AccClause ;
//// AccBindNameClause:AccBindClause ::= Name ;
//// AccBindStringClause:AccBindClause ::= CharLiteralConstant ;
AccBindClause acc_bind_clause =
	A_BIND LPAREN c:char_literal_constant RPAREN
		{
			yyValue = new AccBindStringClause(c);
		}
/		A_BIND LPAREN n:name RPAREN
		{
			yyValue = new AccBindNameClause(n);
		}
;
