#!/bin/bash
cd src-gen/org/tud/forty/parser
sed -r ':a; s%(.*)/\*.*\*/%\1%; ta; /\/\*/ !b; N; ba' SlottableFortranParser.java > sp.tmp
sed '/  \/\/ ==/d' sp.tmp > sp2.tmp
sed '/^$/d' sp2.tmp > SlottableFortranParser.java
rm sp.tmp sp2.tmp
