
  do i = 1, n(5)
    x = 1
  end do

  !$acc loop
  do i = 1, n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop
  !$acc loop
  do i = 1, n(5)
    x = 1
  end do
  !$acc end loop
  !$acc end loop

  !$acc kernels loop
  do i = 1, n(5)
    x = 1
  end do
  !$acc end kernels loop

  !$acc parallel loop
  do i = 1, n(5)
    x = 1
  end do
  !$acc end parallel loop

  !$acc parallel loop vector
  do i = 1, n(5)
    x = 1
  end do
  !$acc end parallel loop

  !$acc parallel loop vector gang
  do i = 1, n(5)
    x = 1
  end do
  !$acc end parallel loop

  ! !$acc parallel loop vector,gang
  !$acc parallel loop vector gang
  do i = 1, n(5)
    x = 1
  end do
  !$acc end parallel loop

  ! !$acc parallel loop vector   ,      gang
  !$acc parallel loop vector gang
  do i = 1, n(5)
    x = 1
  end do
  !$acc end parallel loop

end
