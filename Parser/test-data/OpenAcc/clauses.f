! OpenACC clause test

  x = 0

  ! if ===========================================================================

  !$acc parallel if(1)
  x = 1
  !$acc end parallel

  !$acc parallel if(a)
  x = 1
  !$acc end parallel

  !$acc parallel if(a + 1)
  x = 1
  !$acc end parallel

  !$acc parallel if(a%b)
  x = 1
  !$acc end parallel

  !$acc parallel if(a(1,2))
  x = 1
  !$acc end parallel

  ! default(none) ================================================================

  !$acc parallel default(none)
  x = 1
  !$acc end parallel

  ! device type ==================================================================

  !$acc parallel device_type(t1)
  x = 1
  !$acc end parallel

  !$acc parallel device_type(t1,t2)
  x = 1
  !$acc end parallel

  !$acc parallel device_type(*)
  x = 1
  !$acc end parallel

  ! short versions are expanded

  ! !$acc parallel dtype(t1)
  ! x = 1
  ! !$acc end parallel

  ! !$acc parallel dtype(t1,t2)
  ! x = 1
  ! !$acc end parallel

  ! !$acc parallel dtype(*)
  ! x = 1
  ! !$acc end parallel

  ! tile =========================================================================

  !$acc loop tile(myTile)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  ! wait =========================================================================

  !$acc parallel wait(x + 1,2,y)
  x = 1
  !$acc end parallel

  ! collapse =====================================================================

  !$acc loop collapse(4)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop collapse(0)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop collapse(#TEST#)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  ! expression clauses ===========================================================

  !$acc parallel async(1 + x)
  x = 1
  !$acc end parallel

  !$acc parallel num_gangs(1 + x)
  x = 1
  !$acc end parallel

  !$acc parallel num_workers(1 + x)
  x = 1
  !$acc end parallel

  !$acc parallel vector_length(1 + x)
  x = 1
  !$acc end parallel

  ! reduction ====================================================================

  ! operators

  !$acc parallel reduction(+:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(*:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(max:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(min:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(iand:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(ior:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(ieor:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(.and.:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(.or.:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(.eqv.:x)
  x = 1
  !$acc end parallel

  !$acc parallel reduction(.neqv.:x)
  x = 1
  !$acc end parallel

  ! variabes

  !$acc parallel reduction(+:x,y,z)
  x = 1
  !$acc end parallel


  ! variable list clauses ========================================================

  !$acc enter data copy(x,y,z)
  !$acc enter data copyin(x,y)
  !$acc enter data copyout(x)
  !$acc enter data create(x)
  !$acc enter data delete(x)
  !$acc enter data device(x)
  !$acc enter data firstprivate(x)
  !$acc enter data host(x)
  !$acc enter data present(x)
  !$acc enter data present_or_copy(x)
  !$acc enter data present_or_copyin(x)
  !$acc enter data present_or_copyout(x)
  !$acc enter data present_or_create(x)
  !$acc enter data private(x)
  !$acc enter data self(x)
  !$acc enter data use_device(x)

  ! short versions are expanded
  ! !$acc enter data pcopy(x)
  ! !$acc enter data pcopyin(x)
  ! !$acc enter data pcopyout(x)
  ! !$acc enter data pcreate(x)

  ! loop clauses =================================================================

  !$acc loop gang
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop worker
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop vector
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop independent
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop gang(3)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop worker(3)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop vector(3)
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  !$acc loop shortloop
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  ! seq ==========================================================================

  !$acc routine(myRoutine) seq

  ! bind =========================================================================

  !$acc routine(myRoutine) bind(aName)

  !$acc routine(myRoutine) bind('some string...! x = 1')

  ! nohost =======================================================================

  !$acc routine nohost

  ! device_resident ==============================================================

  !$acc declare device_resident(x,y,z)

  ! link =========================================================================

  !$acc declare link(x,y,z)

end
