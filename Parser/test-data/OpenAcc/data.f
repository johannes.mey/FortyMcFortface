
  !$acc data
  x = 1
  !$acc end data

  !$acc data
  !$acc data
  x = 1
  !$acc end data
  !$acc end data

  !$acc data if(1)
  x = 1
  !$acc end data

  !$acc data if(a)
  x = 1
  !$acc end data

  !$acc data present(T,r,f,stiffness_mat,global_mass_inv,mass_mat_diag)
  x = 1
  !$acc end data

end
