! acc atomic directive (2.12)

  !$acc atomic
  x = 1
  !$acc end atomic

  !$acc atomic read
  x = 1
  !$acc end atomic

  !$acc atomic write
  x = 1
  !$acc end atomic

  !$acc atomic update
  x = 1
  !$acc end atomic

  !$acc atomic capture
  x = 1
  x = 1
  !$acc end atomic

end program
