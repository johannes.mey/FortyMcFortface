! OpenACC clause test

  x = 0

  ! if ===========================================================================

  !$acc loop collapse(1) vector
  do i = 1,n(5)
    x = 1
  end do
  !$acc end loop

  ! reduction ====================================================================

  !$acc parallel copyin()
  x = 1
  !$acc end parallel

end
