
  !$acc wait

  ! does not work because empty list is omitted
  ! !$acc wait ()

  !$acc wait(1,x,1 + 1)

  !$acc wait(1,x,1 + 1) if(x)

  !$acc wait if(x)

end
