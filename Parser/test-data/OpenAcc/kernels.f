
  !$acc kernels
  x = 1
  !$acc end kernels

  !$acc kernels
  !$acc kernels
  x = 1
  !$acc end kernels
  !$acc end kernels

  !$acc kernels if(1)
  x = 1
  !$acc end kernels

  !$acc kernels if(a)
  x = 1
  !$acc end kernels

end
