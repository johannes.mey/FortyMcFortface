
  !$acc parallel
  x = 1
  !$acc end parallel

  !$acc parallel
  !$acc parallel
  x = 1
  !$acc end parallel
  !$acc end parallel

  !$acc parallel if(1)
  x = 1
  !$acc end parallel

  !$acc parallel if(a)
  x = 1
  !$acc end parallel

end
