!! R559 implicit-stmt
!    is IMPLICIT implicit-spec-list
!    or IMPLICIT NONE
!
!  implicit-spec
!    is declaration-type-spec ( letter-spec-list )
!
!  letter-spec
!    is letter [ – letter ]

10 IMPLICIT None

implicit real (X, y, Z), integer (i, j, k-L)

end

