!! R562 namelist-stmt
!    is NAMELIST
!         / namelist-group-name / namelist-group-object-list
!         [ [ , ] / namelist-group-name / namelist-group-object-list ] ...

11 NAMELIST /NameOne/ i,j, /NameTwo/k,l
namelist /namelistname/ a,b,c

end
