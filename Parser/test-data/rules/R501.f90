!! R501 type-declaration-stmt
!    is declaration-type-spec [ [ , attr-spec ] ... :: ] entity-decl-list

! intrinsic-type-spec, no attrs, w/ double colon
integer :: x
integer y
integer, pointer :: xptr
integer, pointer, allocatable :: yptr

! test entity_decl_list
real :: a,b
end
