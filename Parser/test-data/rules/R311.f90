!! R311 defined-operator
!    is defined-unary-op        R703
!    or defined-binary-op       R723
!    or extended-intrinsic-op   R310

interface operator(.unaryop.)
end interface operator(.unaryop.)
interface operator(.binaryop.)
end interface operator(.binaryop.)
interface operator(**)
end interface operator(**)
interface operator(//)
end interface operator(//)
end
