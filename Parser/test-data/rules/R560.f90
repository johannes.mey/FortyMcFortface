!! R560 implicit-spec
!    is declaration-type-spec ( letter-spec-list )

IMPLICIT NONE
IMPLICIT integer(kind=4) (i)
implicit TYPE(FooBar) (i)
implicit integer (i,j), real(k,l,m), TYPE(INTEGER)(s-w,z)
implicit real (a)

end
