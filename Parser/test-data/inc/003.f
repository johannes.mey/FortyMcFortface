!> \file       helmholtz_1D_SEM.f
!> \brief      Parallel spectral element method for the 1D Helmholtz equation
!> \author     I. Huismann, J. Stiller, J. Froehlich
!> \date       2013/09/02
!> \copyright  Institute of Fluid Mechanics
!>             TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This is a minimal parallel program for calculating the propagation of a
!> reaction front in one dimension. The governing equation is the
!> reaction-diffusion equation
!>
!> dT/dt - lambda T'' = r(T)
!>
!> where T is the normalized Temperature, lambda is the heat conductivity and
!> r is the reaction heat solely depending on the Temperature T.              \n
!> The temperature is normalized such that it ranges from 0 to 1 where a value
!> of one implies reacted matter.                                             \n
!> The reaction-diffusion equation is solved on the domain [0, x_max] in the
!> time interval [0, time_end].                                               \n
!> As the initial condition a stable reaction front is imposed, with T = 1 for
!> x < x_rf and an exponential decay for x >= x_rf.                           \n
!> As boundary conditions a Dirichlet condition is set at x = 0 such that
!> T(x = 0) = 1 and at x = x_max a homogeneous Neumann boundary condition is
!> imposed, resulting in (dT/dx)(x = x_max) = 0. Note that the latter condition
!> is only valid if the reaction front is sufficiently far away from x_max.   \n
!> The resulting phenomenon is a reaction front traveling from left to right
!> with a speed of unity.
!>
!> The time integration method employed is an explicit backward-differencing
!> scheme of second order (BDF2). The stability limit for time step width scales
!> as he^2/po^4, where he is the element width and po the polynomial order.
!>
!> The equations and the modeling assumptions can be found in                 \n
!> N. Peters, J. Warnatz:
!> Numerical methods in laminar flame propagation: a GAMM workshop,
!> Vieweg, 1982, pages 1-14: Test problem A.
!>
!> Discretization follows                                                     \n
!> M.O. Deville, P.F. Fischer, E.H. Mund:
!> High-Order Methods for Incompressible Fluid Flow,
!> Cambridge Univ. Press, 2002.
!>
!> The problem and discretization parameters are read from the file 'input'.
!>
!> Requires a Fortran 2003 compiler with a MPI library for parallelization and
!> OpenMP support for hybrid parallelization.
!>
!> Please mind that we utilize Fortran's implicit type conversion inside this
!> program while changing the code and be aware of Fortran's integer division's
!> rounding.                                                                  \n
!> Example: 0.5 = 1/real(2,RNP) /= 1/2 = 0 in Fortran.
!>
!> GNU Octave or Matlab are needed for plotting with 'make plot'.
!> Invoking 'make run' builds the program, starts it and generates a PDF plot
!> file named result.pdf.
!===============================================================================

program Helmholtz_1D_SEM
  use Constants,    only: RNP, RDP, THIRD, ZERO
  ! bla
  use Gauss_Jacobi, only: GLL_points, GLL_Weights, GLL_DiffMatrix
  use MPI
  use Exception_Handling, only: Warning
  implicit none
end program Helmholtz_1D_SEM
