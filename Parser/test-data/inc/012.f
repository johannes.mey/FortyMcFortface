!> \file       helmholtz_1D_SEM.f
!> \brief      Parallel spectral element method for the 1D Helmholtz equation
!> \author     I. Huismann, J. Stiller, J. Froehlich
!> \date       2013/09/02
!> \copyright  Institute of Fluid Mechanics
!>             TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This is a minimal parallel program for calculating the propagation of a
!> reaction front in one dimension. The governing equation is the
!> reaction-diffusion equation
!>
!> dT/dt - lambda T'' = r(T)
!>
!> where T is the normalized Temperature, lambda is the heat conductivity and
!> r is the reaction heat solely depending on the Temperature T.              \n
!> The temperature is normalized such that it ranges from 0 to 1 where a value
!> of one implies reacted matter.                                             \n
!> The reaction-diffusion equation is solved on the domain [0, x_max] in the
!> time interval [0, time_end].                                               \n
!> As the initial condition a stable reaction front is imposed, with T = 1 for
!> x < x_rf and an exponential decay for x >= x_rf.                           \n
!> As boundary conditions a Dirichlet condition is set at x = 0 such that
!> T(x = 0) = 1 and at x = x_max a homogeneous Neumann boundary condition is
!> imposed, resulting in (dT/dx)(x = x_max) = 0. Note that the latter condition
!> is only valid if the reaction front is sufficiently far away from x_max.   \n
!> The resulting phenomenon is a reaction front traveling from left to right
!> with a speed of unity.
!>
!> The time integration method employed is an explicit backward-differencing
!> scheme of second order (BDF2). The stability limit for time step width scales
!> as he^2/po^4, where he is the element width and po the polynomial order.
!>
!> The equations and the modeling assumptions can be found in                 \n
!> N. Peters, J. Warnatz:
!> Numerical methods in laminar flame propagation: a GAMM workshop,
!> Vieweg, 1982, pages 1-14: Test problem A.
!>
!> Discretization follows                                                     \n
!> M.O. Deville, P.F. Fischer, E.H. Mund:
!> High-Order Methods for Incompressible Fluid Flow,
!> Cambridge Univ. Press, 2002.
!>
!> The problem and discretization parameters are read from the file 'input'.
!>
!> Requires a Fortran 2003 compiler with a MPI library for parallelization and
!> OpenMP support for hybrid parallelization.
!>
!> Please mind that we utilize Fortran's implicit type conversion inside this
!> program while changing the code and be aware of Fortran's integer division's
!> rounding.                                                                  \n
!> Example: 0.5 = 1/real(2,RNP) /= 1/2 = 0 in Fortran.
!>
!> GNU Octave or Matlab are needed for plotting with 'make plot'.
!> Invoking 'make run' builds the program, starts it and generates a PDF plot
!> file named result.pdf.
!===============================================================================

program Helmholtz_1D_SEM
  use Constants,    only: RNP, RDP, THIRD, ZERO
  use Gauss_Jacobi, only: GLL_points, GLL_Weights, GLL_DiffMatrix
  use MPI
  use Exception_Handling, only: Warning
  implicit none

  ! the file to read the input parameters from
  character(len=5), parameter :: INPUT_FILE = 'input'

  ! adjustable discretization parameters, optionally set in 'input'
  integer   :: po            =  7    ! polynomial degree
  integer   :: num_ele_total = 60    ! total number of elements in sumulation
  real(RNP) :: dcn           = .1    ! diffusion number < 0.25
  real(RNP) :: x_max         = 30    ! maximum x in domain

  ! adjustable problem parameters, optionally set in 'input'
  real(RNP) :: beta          = 10    ! approx. inverse flame width
  real(RNP) :: alpha         = .8    ! T. ratio (burned - unb.) /burn.
  real(RNP) :: time_end      = 20    ! end of simulation in phys. time
  real(RNP) :: x_rf          =  2    ! initial position of reaction front
  real(RNP) :: lambda        =  1    ! heat conductivity
  real(RNP) :: dt_out        =  5    ! time interval between outputs
  
  ! field variables
  real(RNP), allocatable :: x(:,:)   ! collocation points
  real(RNP), allocatable :: T(:,:,:) ! temperature at collocation points,
                                     ! -1 previous, 0 present, 1 next time level
  real(RNP), allocatable :: r(:,:)   ! reaction heat
  real(RNP), allocatable :: f(:,:)   ! approx. to the time derivative of T
                                            
  ! system matrices
  real(RNP), allocatable :: mass_mat_diag  (:)   ! local mass matrix (diagonal)
  real(RNP), allocatable :: stiffness_mat  (:,:) ! local stiffness matrix (full)
  real(RNP), allocatable :: global_mass_inv(:,:) ! inverted global mass matrix
                                            
  ! time-stepping variables
  integer   :: nt            ! number of time steps
  real(RNP) :: time = 0      ! current time
  real(RNP) :: dt            ! time step width
  real(RNP) :: time_out      ! time of next output
  integer   :: i_time        ! time-loop index

  ! MPI variables
  integer   :: my_rank       ! MPI rank of this process, first one is 0
  integer   :: num_procs     ! number of MPI processes in simulation
  integer   :: communicator  ! MPI communicator, cartesian
  integer   :: RNP_MPI       ! MPI type for RNP-reals
  integer   ::  left_process ! MPI rank of the left  process
  integer   :: right_process ! MPI rank of the right process
  integer   :: ierr          ! return value of the MPI routines

  ! auxiliary variables
  integer   :: num_ele_prior ! number of elements in front of this task
  integer   :: num_ele       ! number of elements on this node
  real(RNP) :: he            ! element width
  real(RNP) :: sum           ! a reduction variable for matrix multiplications
  integer   :: i, j, k       ! loop indices
  real(RDP) :: runtime(3)    ! time measurements

  ! prologue ...................................................................
  ! Link all processes into one MPI environment
  call MPI_Init(ierr)
 
  ! get the walltime, start time measurements
  runtime(1) = MPI_WTime()

  ! initialize the communicator, get the left and right neighbours
  call Initialize_MPI_Environment

  ! read the input file, allocate the field variables and matrices
  call ReadInputAndAllocate

  ! get the discrete operators and variables
  call InitializeDiscretization

  ! information about the run
  if(my_rank == 0) then
    print '(A)'         , 'Reaction-diffusion equation discretized with a '//'one-dimensional spectral element method'
    print *
    print '(2X,A,2X,I7)', 'polynomial order:           ', po
    print '(2X,A,2X,I7)', 'number of elements:         ', num_ele_total
    print '(2X,A,2X,I7)', 'number of time steps:       ', nt
    print *
    print '(2X,A,2X,I7)', 'number of computation nodes:', num_procs
    print '(2X,A,2X,I7)', 'number of elements per node:', num_ele
    print *
  end if

  ! set the initial distribution, generate an output of the initial distribution
  T(:,:,0) = StableReactionFront(x, x_rf, time)
  call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
  time_out = dt_out

  ! start of computation .......................................................

  runtime(2) = MPI_WTime()

  ! explicit treatment for the first time step
  call FirstTimeStep

  ! generate an output if necessary
  if(time > time_out) then
    call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
    ! the next output is after another dt_out
    time_out = time_out + dt_out
  end if

  ! time marching loop for all other time steps

  time_marching: do i_time = 2, nt

    ! approximate the temperature at the end of the time step
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,1) = 2 * T(i,k,0) - T(i,k,-1)
    end do

    ! calculate the temperature update f by evaluating the reaction heat and the
    ! laplacian of the temperature
    do concurrent(k = 1:num_ele, i = 0:po)
      ! calculate the reaction heat and the resulting weighted time derivative
      r(i,k) = ReactionHeat(alpha, beta, T(i,k,1))
      f(i,k) = mass_mat_diag(i) * r(i,k) * he / 2

      ! add the derivative resulting from the laplacian of the temperature
      sum = 0
      do j = 0, po
        sum = sum - lambda*stiffness_mat(i,j) * T(j,k,1)*2 / he
      end do
      f(i,k) = f(i,k) + sum
    end do


    ! assemble the weighted time derivative at the element boundaries
    do concurrent(k = 2:num_ele)
      f(po,k-1) = f(po,k-1) + f(0,k)
      f( 0,k  ) = f(po,k-1)
    end do

    ! assemble f on the edges of the computation nodes
    call AddLeftRight(communicator,left_process,right_process,RNP_MPI,f)

    ! Dirichlet BC at x = 0
    if(my_rank == 0) f(0,1) = 0  ! f = 0 => T stays the same

    ! homogeneous Neumann BC at x = x_end
    ! an inhomogeneous neumann BC would be added here
    if(my_rank +1 == num_procs) f(po,num_ele) = f(po,num_ele)


    ! use the data to calculate the temperature at the next time level (BDF2)
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,1) = 4 * THIRD * T(i,k,0)  -       THIRD * T(i,k,-1)  +   THIRD * 2 * dt * global_mass_inv(i,k) * f(i,k)
    end do

    ! advance the temperature to the next time step
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,-1) = T(i,k,0)
      T(i,k, 0) = T(i,k,1)
    end do

    ! advance time, generate output if necessary
    time = time + dt

    ! generate output?
    if(time > time_out) then
      call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
      ! the next output is after another dt_out
      time_out = time_out + dt_out
    end if

  end do time_marching

  runtime(3) = MPI_WTime()

  ! epilogue ...................................................................

  ! information about the runtimes
  runtime = [runtime(3)-runtime(1),runtime(2)-runtime(1),runtime(3)-runtime(2)]

  if(my_rank == 0) then
    print '(2X,A,2X,ES10.3,A)','Helmholtz 1D SEM finished after',runtime(1),' s'
    print '(2X,A,2X,ES10.3,A)','The initialization needed      ',runtime(2),' s'
    print '(2X,A,2X,ES10.3,A)','The computation took           ',runtime(3),' s'
  end if

  ! generate last output
  call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))

  ! close the MPI environment
  call MPI_Finalize(ierr)

contains

  !-----------------------------------------------------------------------------
  !> \brief Calculation of the reaction heat due to the traveling reaction front
  !> \author Immo Huismann
  !>
  !> \details
  !> The reaction heat is the source term on the right-hand side of the
  !> reaction-diffusion equation. As we use a simplified equation set, the
  !> reaction heat can directly be derived from the temperature. This assumes that
  !> the temperature distribution is homogeneous at the start of the run and that
  !> the only contribution to it is from the reaction (and the diffusion).
  !> For the formula see J. Froehlich, J. Lang:
  !> Two-dimensional cascadic finite element computations of combustion problems,
  !> Comp. Meth. Appl. Mech. Engineering, 158, 255-267, 1998,
  !> equations (3.1) and (3.2) with Le = 1, y = 1 - phi.

  pure function ReactionHeat(alpha, beta, phi) result(r)
    real(RNP), intent(in) :: alpha !< Temperature ratio (burned - unb.) /burn.
    real(RNP), intent(in) :: beta  !< approx. inverse flame width
    real(RNP), intent(in) :: phi   !< Temperature normalized to [0, 1]
    real(RNP)             :: r     !< the reaction heat resulting from phi

    r =         beta ** 2 * (  1 - phi)   *    exp( beta      * (phi -   1) / (1 + alpha * (phi - 1) ) ) / 2

  end function ReactionHeat

  !-----------------------------------------------------------------------------
  !> \brief  Reads the input file, allocates all data fields accordingly
  !> \author Immo Huismann
  !>
  !> \details
  !> This subroutine reads the input file (normally 'input'), sets all available
  !> discretization and problem parameters, calculates the number of elements
  !> (in total and on this process).
  !> Afterwards all field variables and system matrices are allocated.
  !> This subroutine requires the variables my_rank and num_procs to be set
  !> correctly, thus the MPI system has to be initialized prior to calling this
  !> routine.

  subroutine ReadInputAndAllocate
    integer :: uni ! input unit to use

    ! the file format of the input file
    namelist /parameters/ po,num_ele_total,x_max,dcn,lambda,x_rf,alpha,beta,time_end,dt_out

    ! read the input file, set parameters
    open(newunit = uni, file = INPUT_FILE)
    read(uni,NML=parameters)
    close(uni)

    ! distribute the elements homogeneously over the computation nodes
    ! mind the integer arithmetic a = b * a div b + a mod b
    num_ele = num_ele_total / num_procs
    if(my_rank + 1 <= mod(num_ele_total , num_procs)) num_ele = num_ele +1

    ! compute number of elements in front of this node
    num_ele_prior =     my_rank       * (num_ele_total / num_procs)  + min(my_rank    , mod(num_ele_total , num_procs))

    ! allocate the field variables
    allocate(T(0:po,1:num_ele,-1:1), r(0:po,1:num_ele))
    allocate(f(0:po,1:num_ele     ), x(0:po,1:num_ele))
    ! allocate the system matrices
    allocate(mass_mat_diag(0:po), stiffness_mat(0:po,0:po))
    allocate(global_mass_inv(0:po,1:num_ele))

    ! touch the variables so that they are distributed in memory according to the
    ! processors that operate on them (relevant for CCNUMA)
    do concurrent(k = 1:num_ele)
      T(:, i,-1)            = 0
      T(:, i, 0)            = 0
      T(:, i, 1)            = 0
      r(:, i)               = 0
      f(:, i)               = 0
      global_mass_inv(:, i) = 0
    end do

  end subroutine ReadInputAndAllocate

  !-------------------------------------------------------------------------------
  !> \brief  Initialization of discrete operators and variables
  !> \author Immo Huismann
  !>
  !> \details
  !> Initializes the nodes x as-well as all necessary matrices and auxiliary
  !> variables for the computation. \n
  !> For details on the discretization see Fischer, Mund, Deville.

  subroutine InitializeDiscretization
    real(RNP), allocatable :: xi      (:)   ! Gauss-Lobatto-Legendre (GLL) points
    real(RNP), allocatable :: w       (:)   ! GLL quadrature weights
    real(RNP), allocatable :: mass_mat(:,:) ! standard mass matrix
    real(RNP), allocatable :: diff    (:,:) ! standard differentiation matrix

    ! temporary variables
    allocate(xi(0:po),w(0:po),mass_mat(0:po,0:po),diff(0:po,0:po))

    ! set collocation points .......................................................

    ! node distribution in the standard element
    xi   = GLL_points(po)

    ! width of one element
    he = x_max / (num_ele_total)

    ! collocation points
    do i = 1, num_ele
      x(:,i) = (i-1)*he + (xi+1) / 2 * he + num_ele_prior * he
    end do

    ! system matrices ..............................................................

    w    = GLL_Weights   (xi)
    diff = GLL_DiffMatrix(xi)

    ! standard mass matrix as a matrix
    mass_mat = ZERO
    do i = 0, po
      mass_mat(i,i) = w(i)
    end do

    ! standard stiffness matrix
    stiffness_mat = matmul(transpose(diff), matmul(mass_mat,diff))

    ! main diagonal of the mass matrix
    mass_mat_diag = w

    ! generate the global mass matrix (main diagonal) from the local version
    do i = 1, num_ele
      global_mass_inv(:,i) = mass_mat_diag * he / 2
    end do

    do i = 2, num_ele
      global_mass_inv( 0, i  ) = global_mass_inv(0, i) + global_mass_inv(po, i-1)
      global_mass_inv(po, i-1) = global_mass_inv(0, i)
    end do

    ! invert the global mass matrix as we only need it's inverse
    global_mass_inv = 1 / global_mass_inv

    ! equate the size of the time step and the necessary number of time steps to
    ! meet the stability condition specified by dcn
    dt = (x(1,1) - x(0,1)) ** 2 * dcn / lambda
    nt = ceiling(time_end / dt)

    ! equate the exact time step width for reaching time_end
    dt = time_end / nt

    ! ensure the deallocation of the subroutine variables
    deallocate(xi, w, mass_mat, diff)

  end subroutine InitializeDiscretization

  !-------------------------------------------------------------------------------
  !> \brief  Returnes the temperature distribution of a stable reaction front
  !> \author Immo Huismann
  !>
  !> \details
  !> Uses the nodes x and the current time to invoke a stable flame front in the
  !> variable phi.
  !> For x <  x_start + current_time the matter is reacted and phi thus 1. For
  !> x >= x_start + current_time an exponential decay is used.                  \n
  !> This distribution is only exact for beta -> infinity.

  pure function StableReactionFront(x, x_start, current_time) result(phi)
    real(RNP), intent(in) :: x(0:,1:)            !< nodes
    real(RNP), intent(in) :: x_start             !< initial pos. of reaction front
    real(RNP), intent(in) :: current_time        !< the current time
    real(RNP) :: phi(0:ubound(x,1),1:ubound(x,2))!< temperature at point x

    real(RNP)              :: rf_pos             ! reaction front position

    rf_pos = current_time + x_start

    phi = 1
    phi = max(sign(phi, rf_pos - x), ZERO)  - min(sign(phi, rf_pos - x), ZERO) * exp(rf_pos - x)

  end function StableReactionFront

  !-------------------------------------------------------------------------------
  !> \brief  Generates a minimal output
  !> \author Immo Huismann
  !>
  !> \details
  !> The nodes, current temperature distribution and the reaction heat are written
  !> node wise into an output file. The current time, the starting point of the
  !> flame front and the domain width are supplied, as-well as the number of
  !> elements used, the polynomial degree and the diffusivity.                  \n
  !> The output is written into the file 'raw-result-nnnn_mmmm' with nnnn replaced
  !> with a counter for the output and mmmm replaced by the MPI rank.           \n
  !> Please note that this subroutine is not thread safe und thus has to be
  !> invoked using only one thread at a time.

  subroutine GenerateOutput(time, num_ele_total, alpha, beta, x, T)
    ! parameters
    real(RNP), intent(in) :: time          !< the time of the snapshot
    integer  , intent(in) :: num_ele_total !< total number of elements
    real(RNP), intent(in) :: alpha         !< Temp. ratio (burned - unb.) /burn.
    real(RNP), intent(in) :: beta          !< approx. inverse flame width
    ! field variables
    real(RNP), intent(in) :: x(0:,:)       !< node locations
    real(RNP), intent(in) :: T(0:,:)       !< Temperature

    character(len=20) :: filename          ! filename to use
    integer           :: output            ! output unit
    integer, save     :: n_out = 0         ! output index
    integer           :: po                ! polynomial degree
    integer           :: num_ele           ! number of elements

    ! get the bounds of the arrays
    po      = ubound(x,1)
    num_ele = ubound(x,2)

    ! generate the filename (has to be done this way in fortran)
    write(filename, '(A11,I4.4,A1,I4.4)') 'raw-result-', n_out, '_', my_rank

    ! write the output
    open (newunit = output, file = filename)
    write(output,'(F15.7,2I5)') time, num_ele_total, po
    do j = 1, num_ele
      do k = 0, po
        write(output,'(3F15.7)') x(k,j), T(k,j), ReactionHeat(alpha,beta,T(k,j))
      end do
    end do
    close(output)

    ! increase the output counter
    n_out = n_out + 1

  end subroutine GenerateOutput

  !-------------------------------------------------------------------------------
  !> \brief  Special treatment for the first time step
  !> \author Immo Huismann
  !>
  !> \details
  !> The first time step has to be treated differently as the initialization does
  !> not include the time step before t = 0. An explicit Euler scheme is employed.
  !> This scheme is only of first order if applied to all time steps. But if only
  !> a single time step is conducted with a scheme of one order lower than the
  !> scheme in the main time loop the overall convergence order is not affected,
  !> see                                                                        \n
  !> R. Peyret:
  !> Spectral Methods for Incompressible Viscous Flow
  !> Springer, 2001
  !> Chapters 4.4 - 4.5.

  subroutine FirstTimeStep
    integer :: k, i  ! loop indices

    ! calculate the temperature update f by evaluating the reaction heat and the
    ! laplacian of the temperature
    do concurrent(k = 1:num_ele, i = 0:po)
      ! calculate the reaction heat and the resulting weighted time derivative
      r(i,k) = ReactionHeat(alpha, beta, T(i,k,0))

      ! Calculate the derivative resulting from the laplacian of the temperature
      sum = 0
      do j = 0, po
        sum = sum - lambda*stiffness_mat(i,j) * T(j,k,0) * 2 / he
      end do

      ! add both parts
      f(i,k) = mass_mat_diag(i) * r(i,k) * he / 2 + sum
    end do

    ! assemble the weighted time derivative at the element boundaries
    do concurrent(k = 2:num_ele)
      f(po,k-1) = f(po,k-1) + f(0,k)
      f( 0,k  ) = f(po,k-1)
    end do

    ! assemble f on the edges of the computation nodes
    call AddLeftRight(communicator,left_process,right_process,RNP_MPI,f)

    ! Dirichlet BC at x = 0
    if(my_rank == 0) f(0,1) = 0  ! f = 0 => T stays the same

    ! homogeneous Neumann BC at x = x_end
    ! an inhomogeneous neumann BC would be added here
    if(my_rank +1 == num_procs) f(po,num_ele) = f(po,num_ele)

    ! use the data to calculate the temperature at the next time level
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,1) = T(i,k,0) + dt * global_mass_inv(i,k) * f(i,k)
    end do

    ! advance the temperature to the next time step
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,-1) = T(i,k,0)
      T(i,k, 0) = T(i,k,1)
    end do

    ! advance time
    time = time + dt

  end subroutine FirstTimeStep

  !-------------------------------------------------------------------------------
  !> \brief  Initializes MPI, creates a cartesian MPI topology
  !> \author Immo Huismann
  !>
  !> \details
  !> This routine initializes the whole MPI environment AFTER MPI_Init has been
  !> called.
  !> The rank of the process, the number are inquired and a cartesian cummonicator
  !> is constructed to simplify communications. The variables left_rank and
  !> right_rank refer to this topology. It is not periodic.
  !>
  !> Last but not least a MPI datatype is created to communicate RNP reals safely
  !> and a check is performed whether a hybrid parallelization is supported.

  subroutine Initialize_MPI_Environment
    integer :: ierr                        ! error flag for MPI
    integer :: number_dimensions = 1       ! the communicator is one dimensional
    logical :: periodic(1)       = .false. ! the setup is not periodic
    logical :: reorder_processes = .true.  ! allow MPI to reorder the processes

    ! get the size of the standard communicator
    call MPI_Comm_Size(MPI_COMM_WORLD,num_procs,ierr)

    ! create a cartesian communicator named communicator out of MPI_COMM_WORLD
    call MPI_Cart_Create(MPI_COMM_WORLD, number_dimensions, [num_procs], periodic, reorder_processes, communicator, ierr)

    ! get the MPI rank in this new communicator
    call MPI_Comm_Rank(communicator, my_rank, ierr)

    ! get left and right neighbours of current process
    call MPI_Cart_Shift(communicator, 1, 1, left_process, right_process, ierr)

    ! generate a costum MPI data type to communicate reals of precision RNP
    call MPI_Type_Create_F90_REAL(precision(ZERO), MPI_UNDEFINED, RNP_MPI, ierr)

    ! check whether the hybrid version is supported
    if(MPI_THREAD_FUNNELED /= 1) then
      call Warning('InitializeMPIEnvironment', 'Hybrid parallelization not supported, check MPI library.')
    end if

  end subroutine Initialize_MPI_Environment

  !-------------------------------------------------------------------------------
  !> \brief Adds the left- and rightmost elements in an array to those on the
  !>        nodes in the corresponding direction.
  !> \author Immo Huismann
  !>
  !> \details
  !> The spectral element method requires the assembly of the system, resulting in
  !> adding the values on the left and right side of an element boundary.
  !> As this program uses MPI for communication, the values at the ends of each
  !> nodes domain have to be communicated. This is done in this routine.

  subroutine AddLeftRight(cart_comm, left_proc, right_proc, RNP_MPI, array)
    integer  , intent(in)    :: cart_comm    !< cartesian communicator to use
    integer  , intent(in)    :: left_proc    !< left neighbour in cart_comm
    integer  , intent(in)    :: right_proc   !< right neighbour in cart_comm
    integer  , intent(in)    :: RNP_MPI      !< MPI datatype to communicate
    real(RNP), intent(inout) :: array(0:,1:) !< array to communicate

    integer   :: po, num_ele                 ! bounds of array
    real(RNP) :: left_tmp    , right_tmp     ! temporary storages
    integer   :: left_tag = 1, right_tag = 2 ! tag for left/right communication
    integer   :: requests(4)                 ! communication requests
    integer   :: status(MPI_STATUS_SIZE,4)   ! MPI status for each request

    ! get the bounds of the array
    po      = ubound(array,1)
    num_ele = ubound(array,2)

    ! clear the temporary variables
    left_tmp = 0
    right_tmp = 0

    ! send to left and right process
    call MPI_ISend(array( 0,      1),1,RNP_MPI,  left_proc,  left_tag, cart_comm, requests(1), ierr)
    call MPI_ISend(array(po,num_ele),1,RNP_MPI, right_proc, right_tag, cart_comm, requests(2), ierr)

    ! receive from right and left, store in the temporaries
    call MPI_IRecv( left_tmp, 1,RNP_MPI,  left_proc, right_tag, cart_comm, requests(3), ierr)
    call MPI_IRecv(right_tmp, 1,RNP_MPI, right_proc,  left_tag, cart_comm, requests(4), ierr)

    ! ensure that communication is over
    call MPI_WaitAll(4,requests, status, ierr)

    ! add on the left and right side
    array( 0,      1) = array( 0,      1) +  left_tmp
    array(po,num_ele) = array(po,num_ele) + right_tmp

  end subroutine AddLeftRight

end program Helmholtz_1D_SEM
