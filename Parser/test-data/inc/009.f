!> \file       helmholtz_1D_SEM.f
!> \brief      Parallel spectral element method for the 1D Helmholtz equation
!> \author     I. Huismann, J. Stiller, J. Froehlich
!> \date       2013/09/02
!> \copyright  Institute of Fluid Mechanics
!>             TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This is a minimal parallel program for calculating the propagation of a
!> reaction front in one dimension. The governing equation is the
!> reaction-diffusion equation
!>
!> dT/dt - lambda T'' = r(T)
!>
!> where T is the normalized Temperature, lambda is the heat conductivity and
!> r is the reaction heat solely depending on the Temperature T.              \n
!> The temperature is normalized such that it ranges from 0 to 1 where a value
!> of one implies reacted matter.                                             \n
!> The reaction-diffusion equation is solved on the domain [0, x_max] in the
!> time interval [0, time_end].                                               \n
!> As the initial condition a stable reaction front is imposed, with T = 1 for
!> x < x_rf and an exponential decay for x >= x_rf.                           \n
!> As boundary conditions a Dirichlet condition is set at x = 0 such that
!> T(x = 0) = 1 and at x = x_max a homogeneous Neumann boundary condition is
!> imposed, resulting in (dT/dx)(x = x_max) = 0. Note that the latter condition
!> is only valid if the reaction front is sufficiently far away from x_max.   \n
!> The resulting phenomenon is a reaction front traveling from left to right
!> with a speed of unity.
!>
!> The time integration method employed is an explicit backward-differencing
!> scheme of second order (BDF2). The stability limit for time step width scales
!> as he^2/po^4, where he is the element width and po the polynomial order.
!>
!> The equations and the modeling assumptions can be found in                 \n
!> N. Peters, J. Warnatz:
!> Numerical methods in laminar flame propagation: a GAMM workshop,
!> Vieweg, 1982, pages 1-14: Test problem A.
!>
!> Discretization follows                                                     \n
!> M.O. Deville, P.F. Fischer, E.H. Mund:
!> High-Order Methods for Incompressible Fluid Flow,
!> Cambridge Univ. Press, 2002.
!>
!> The problem and discretization parameters are read from the file 'input'.
!>
!> Requires a Fortran 2003 compiler with a MPI library for parallelization and
!> OpenMP support for hybrid parallelization.
!>
!> Please mind that we utilize Fortran's implicit type conversion inside this
!> program while changing the code and be aware of Fortran's integer division's
!> rounding.                                                                  \n
!> Example: 0.5 = 1/real(2,RNP) /= 1/2 = 0 in Fortran.
!>
!> GNU Octave or Matlab are needed for plotting with 'make plot'.
!> Invoking 'make run' builds the program, starts it and generates a PDF plot
!> file named result.pdf.
!===============================================================================

program Helmholtz_1D_SEM
  use Constants,    only: RNP, RDP, THIRD, ZERO
  use Gauss_Jacobi, only: GLL_points, GLL_Weights, GLL_DiffMatrix
  use MPI
  use Exception_Handling, only: Warning
  implicit none

  ! the file to read the input parameters from
  character(len=5), parameter :: INPUT_FILE = 'input'

  ! adjustable discretization parameters, optionally set in 'input'
  integer   :: po            =  7    ! polynomial degree
  integer   :: num_ele_total = 60    ! total number of elements in sumulation
  real(RNP) :: dcn           = .1    ! diffusion number < 0.25
  real(RNP) :: x_max         = 30    ! maximum x in domain

  ! adjustable problem parameters, optionally set in 'input'
  real(RNP) :: beta          = 10    ! approx. inverse flame width
  real(RNP) :: alpha         = .8    ! T. ratio (burned - unb.) /burn.
  real(RNP) :: time_end      = 20    ! end of simulation in phys. time
  real(RNP) :: x_rf          =  2    ! initial position of reaction front
  real(RNP) :: lambda        =  1    ! heat conductivity
  real(RNP) :: dt_out        =  5    ! time interval between outputs
  
  ! field variables
  real(RNP), allocatable :: x(:,:)   ! collocation points
  real(RNP), allocatable :: T(:,:,:) ! temperature at collocation points,
                                     ! -1 previous, 0 present, 1 next time level
  real(RNP), allocatable :: r(:,:)   ! reaction heat
  real(RNP), allocatable :: f(:,:)   ! approx. to the time derivative of T
                                            
  ! system matrices
  real(RNP), allocatable :: mass_mat_diag  (:)   ! local mass matrix (diagonal)
  real(RNP), allocatable :: stiffness_mat  (:,:) ! local stiffness matrix (full)
  real(RNP), allocatable :: global_mass_inv(:,:) ! inverted global mass matrix
                                            
  ! time-stepping variables
  integer   :: nt            ! number of time steps
  real(RNP) :: time = 0      ! current time
  real(RNP) :: dt            ! time step width
  real(RNP) :: time_out      ! time of next output
  integer   :: i_time        ! time-loop index

  ! MPI variables
  integer   :: my_rank       ! MPI rank of this process, first one is 0
  integer   :: num_procs     ! number of MPI processes in simulation
  integer   :: communicator  ! MPI communicator, cartesian
  integer   :: RNP_MPI       ! MPI type for RNP-reals
  integer   ::  left_process ! MPI rank of the left  process
  integer   :: right_process ! MPI rank of the right process
  integer   :: ierr          ! return value of the MPI routines

  ! auxiliary variables
  integer   :: num_ele_prior ! number of elements in front of this task
  integer   :: num_ele       ! number of elements on this node
  real(RNP) :: he            ! element width
  real(RNP) :: sum           ! a reduction variable for matrix multiplications
  integer   :: i, j, k       ! loop indices
  real(RDP) :: runtime(3)    ! time measurements

  ! prologue ...................................................................
  ! Link all processes into one MPI environment
  call MPI_Init(ierr)
 
  ! get the walltime, start time measurements
  runtime(1) = MPI_WTime()

  ! initialize the communicator, get the left and right neighbours
  call Initialize_MPI_Environment

  ! read the input file, allocate the field variables and matrices
  call ReadInputAndAllocate

  ! get the discrete operators and variables
  call InitializeDiscretization

  ! information about the run
  if(my_rank == 0) then
    print '(A)'         , 'Reaction-diffusion equation discretized with a '//'one-dimensional spectral element method'
    print *
    print '(2X,A,2X,I7)', 'polynomial order:           ', po
    print '(2X,A,2X,I7)', 'number of elements:         ', num_ele_total
    print '(2X,A,2X,I7)', 'number of time steps:       ', nt
    print *
    print '(2X,A,2X,I7)', 'number of computation nodes:', num_procs
    print '(2X,A,2X,I7)', 'number of elements per node:', num_ele
    print *
  end if

  ! set the initial distribution, generate an output of the initial distribution
  T(:,:,0) = StableReactionFront(x, x_rf, time)
  call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
  time_out = dt_out

  ! start of computation .......................................................

  runtime(2) = MPI_WTime()

  ! explicit treatment for the first time step
  call FirstTimeStep

  ! generate an output if necessary
  if(time > time_out) then
    call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
    ! the next output is after another dt_out
    time_out = time_out + dt_out
  end if

  ! time marching loop for all other time steps

  time_marching: do i_time = 2, nt

    ! approximate the temperature at the end of the time step
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,1) = 2 * T(i,k,0) - T(i,k,-1)
    end do

    ! calculate the temperature update f by evaluating the reaction heat and the
    ! laplacian of the temperature
    do concurrent(k = 1:num_ele, i = 0:po)
      ! calculate the reaction heat and the resulting weighted time derivative
      r(i,k) = ReactionHeat(alpha, beta, T(i,k,1))
      f(i,k) = mass_mat_diag(i) * r(i,k) * he / 2

      ! add the derivative resulting from the laplacian of the temperature
      sum = 0
      do j = 0, po
        sum = sum - lambda*stiffness_mat(i,j) * T(j,k,1)*2 / he
      end do
      f(i,k) = f(i,k) + sum
    end do


    ! assemble the weighted time derivative at the element boundaries
    do concurrent(k = 2:num_ele)
      f(po,k-1) = f(po,k-1) + f(0,k)
      f( 0,k  ) = f(po,k-1)
    end do

    ! assemble f on the edges of the computation nodes
    call AddLeftRight(communicator,left_process,right_process,RNP_MPI,f)

    ! Dirichlet BC at x = 0
    if(my_rank == 0) f(0,1) = 0  ! f = 0 => T stays the same

    ! homogeneous Neumann BC at x = x_end
    ! an inhomogeneous neumann BC would be added here
    if(my_rank +1 == num_procs) f(po,num_ele) = f(po,num_ele)


    ! use the data to calculate the temperature at the next time level (BDF2)
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,1) = 4 * THIRD * T(i,k,0)  -       THIRD * T(i,k,-1)  +   THIRD * 2 * dt * global_mass_inv(i,k) * f(i,k)
    end do

    ! advance the temperature to the next time step
    do concurrent(k = 1:num_ele, i = 0:po)
      T(i,k,-1) = T(i,k,0)
      T(i,k, 0) = T(i,k,1)
    end do

    ! advance time, generate output if necessary
    time = time + dt

    ! generate output?
    if(time > time_out) then
      call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))
      ! the next output is after another dt_out
      time_out = time_out + dt_out
    end if

  end do time_marching

  runtime(3) = MPI_WTime()

  ! epilogue ...................................................................

  ! information about the runtimes
  runtime = [runtime(3)-runtime(1),runtime(2)-runtime(1),runtime(3)-runtime(2)]

  if(my_rank == 0) then
    print '(2X,A,2X,ES10.3,A)','Helmholtz 1D SEM finished after',runtime(1),' s'
    print '(2X,A,2X,ES10.3,A)','The initialization needed      ',runtime(2),' s'
    print '(2X,A,2X,ES10.3,A)','The computation took           ',runtime(3),' s'
  end if

  ! generate last output
  call GenerateOutput(time, num_ele_total, alpha, beta, x, T(:,:,0))

  ! close the MPI environment
  call MPI_Finalize(ierr)

contains

  !-----------------------------------------------------------------------------
  !> \brief Calculation of the reaction heat due to the traveling reaction front
  !> \author Immo Huismann
  !>
  !> \details
  !> The reaction heat is the source term on the right-hand side of the
  !> reaction-diffusion equation. As we use a simplified equation set, the
  !> reaction heat can directly be derived from the temperature. This assumes that
  !> the temperature distribution is homogeneous at the start of the run and that
  !> the only contribution to it is from the reaction (and the diffusion).
  !> For the formula see J. Froehlich, J. Lang:
  !> Two-dimensional cascadic finite element computations of combustion problems,
  !> Comp. Meth. Appl. Mech. Engineering, 158, 255-267, 1998,
  !> equations (3.1) and (3.2) with Le = 1, y = 1 - phi.

  pure function ReactionHeat(alpha, beta, phi) result(r)
    real(RNP), intent(in) :: alpha !< Temperature ratio (burned - unb.) /burn.
    real(RNP), intent(in) :: beta  !< approx. inverse flame width
    real(RNP), intent(in) :: phi   !< Temperature normalized to [0, 1]
    real(RNP)             :: r     !< the reaction heat resulting from phi

    r =         beta ** 2 * (  1 - phi)  *    exp( beta      * (phi -   1) / (1 + alpha * (phi - 1) ) ) / 2

  end function ReactionHeat

end program Helmholtz_1D_SEM
