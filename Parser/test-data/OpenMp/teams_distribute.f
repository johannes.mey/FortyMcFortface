! OMP 2.10.6

  !$omp teams distribute
  do i = 1,10
    x = 1
  end do

  !$omp teams distribute
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute

  !$omp teams distribute
  !$omp teams distribute
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute
  !$omp end teams distribute

end program
