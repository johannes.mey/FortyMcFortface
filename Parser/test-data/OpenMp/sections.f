! OMP 2.7.2

  !$omp sections
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end sections

  ! test all clauses

  !$omp sections private(a, b, c)
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end sections

  !$omp sections firstprivate(a, b, c)
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end sections

  !$omp sections lastprivate(a, b, c)
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end sections

  !$omp sections reduction(.eqv.:x, y, z)
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end sections

end program
