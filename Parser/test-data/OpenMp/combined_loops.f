! OMP 2.7.1

  !$omp parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do

  ! OMP 2.10.2

  !$omp parallel sections
    !$omp section
      x = 1
    !$omp section
      x = 1
  !$omp end parallel sections

  ! OMP 2.10.3

  !$omp parallel workshare
  x = 1
  !$omp end parallel workshare

  ! OMP 2.10.4

  !$omp parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do simd

  ! OMP 2.10.5

  !$omp target teams
  x = 1
  !$omp end target teams

  ! OMP 2.10.6

  !$omp teams distribute
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute

  ! OMP 2.10.7

  !$omp teams distribute simd
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute simd

  ! OMP 2.10.8

  !$omp target teams distribute
  do i = 1,10
    x = 1
  end do
  !$omp end target teams distribute

  ! OMP 2.10.9

  !$omp target teams distribute simd
  do i = 1,10
    x = 1
  end do
  !$omp end target teams distribute simd

  ! OMP 2.10.10

  !$omp teams distribute parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute parallel do

  ! OMP 2.10.11

  !$omp target teams distribute parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end target teams distribute parallel do

  ! OMP 2.10.12

  !$omp teams distribute parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end teams distribute parallel do simd

  ! OMP 2.10.13

  !$omp target teams distribute parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end target teams distribute parallel do simd

end program
