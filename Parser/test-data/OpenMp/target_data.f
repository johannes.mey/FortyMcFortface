! OMP 2.9.1

  !$omp target data
  x = 1
  !$omp end target data

  !$omp target data if(x)
  x = 1
  !$omp end target data

  !$omp target data if(x) if(y) if(z)
  x = 1
  !$omp end target data

  !$omp target data if(x)
  !$omp target data if(x)
  !$omp target data if(x)
  x = 1
  !$omp end target data
  !$omp end target data
  !$omp end target data

  ! test all clauses

  !$omp target data device(3 / 2)
  x = 1
  !$omp end target data

  !$omp target data map(to: a, b, c)
  x = 1
  !$omp end target data

  !$omp target data if(5)
  x = 1
  !$omp end target data

end program
