! OMP 2.8.1

  !$omp simd
  do i = 1,10
    x = 1
  end do

  !$omp simd
  do i = 1,10
    x = 1
  end do
  !$omp end simd

  !$omp simd
  !$omp simd
  do i = 1,10
    x = 1
  end do
  !$omp end simd
  !$omp end simd

  ! test all clauses

  !$omp simd safelen(7)
  do i = 1,10
    x = 1
  end do

  !$omp simd linear(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp simd linear(a, b, c:5 + 2)
  do i = 1,10
    x = 1
  end do

  !$omp simd aligned(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp simd aligned(a, b, c:d)
  do i = 1,10
    x = 1
  end do


  !$omp simd private(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp simd lastprivate(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp simd reduction(somethingelse:x, y, z)
  do i = 1,10
    x = 1
  end do

  !$omp simd collapse(25 * 2)
  do i = 1,10
    x = 1
  end do

end program
