! OMP 2.9.6

  !$omp distribute
  do i = 1,10
    x = 1
  end do

  !$omp distribute
  do i = 1,10
    x = 1
  end do
  !$omp end distribute

  !$omp distribute
  !$omp distribute
  do i = 1,10
    x = 1
  end do
  !$omp end distribute
  !$omp end distribute

  ! test all clauses

  !$omp distribute private(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp distribute firstprivate(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp distribute collapse(25 * 2)
  do i = 1,10
    x = 1
  end do

  !$omp distribute dist_schedule(static, 3 + 4)
  do i = 1,10
    x = 1
  end do

end program
