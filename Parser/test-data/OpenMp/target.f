! OMP 2.9.2

  !$omp target
  x = 1
  !$omp end target

  !$omp target if(x)
  x = 1
  !$omp end target

  !$omp target if(x) if(y) if(z)
  x = 1
  !$omp end target

  !$omp target if(x)
  !$omp target if(x)
  !$omp target if(x)
  x = 1
  !$omp end target
  !$omp end target
  !$omp end target

  ! test all clauses

  !$omp target device(3 / 2)
  x = 1
  !$omp end target

  !$omp target map(to: a, b, c)
  x = 1
  !$omp end target

  !$omp target if(5)
  x = 1
  !$omp end target

end program
