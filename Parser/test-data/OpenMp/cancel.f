! OMP 2.13.1

  !$omp cancel parallel
  !$omp cancel parallel if(.true.)

  !$omp cancel sections
  !$omp cancel sections if(.true.)

  !$omp cancel do
  !$omp cancel do if(.true.)

  !$omp cancel taskgroup
  !$omp cancel taskgroup if(.true.)

end program
