! OMP 2.12.2

  !$omp critical
  x = 1
  !$omp end critical

  !$omp critical(myName)
  x = 1
  !$omp end critical(myName)

end program
