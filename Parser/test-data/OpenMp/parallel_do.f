! OMP 2.7.1

  !$omp parallel do
  do i = 1,10
    x = 1
  end do

  !$omp parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do

  !$omp parallel do
  !$omp parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do
  !$omp end parallel do

end program
