! OMP 2.11.1

  !$omp task
  x = 1
  !$omp end task

  ! test all clauses

  !$omp task if(23)
  x = 1
  !$omp end task

  !$omp task final(1000)
  x = 1
  !$omp end task

  !$omp task untied
  x = 1
  !$omp end task

  !$omp task default(none)
  x = 1
  !$omp end task

  !$omp task mergeable
  x = 1
  !$omp end task

  !$omp task private(a, b, c)
  x = 1
  !$omp end task

  !$omp task firstprivate(d, e, f)
  x = 1
  !$omp end task

  !$omp task shared(g, h, i)
  x = 1
  !$omp end task

  !$omp task depend(out: a, b, c)
  x = 1
  !$omp end task

end program
