! OMP 2.9.5

  !$omp teams
  x = 1
  !$omp end teams

  !$omp teams private(x)
  x = 1
  !$omp end teams

  !$omp teams private(x) private(x) private(x)
  x = 1
  !$omp end teams

  !$omp teams private(x)
  !$omp teams private(x)
  !$omp teams private(x)
  x = 1
  !$omp end teams
  !$omp end teams
  !$omp end teams

  ! test all clauses

  !$omp teams num_teams(23)
  x = 1
  !$omp end teams

  !$omp teams thread_limit(2)
  x = 1
  !$omp end teams

  !$omp teams default(shared)
  x = 1
  !$omp end teams

  !$omp teams private(x, y)
  x = 1
  !$omp end teams

  !$omp teams firstprivate(a, b, c)
  x = 1
  !$omp end teams

  !$omp teams shared(a)
  x = 1
  !$omp end teams

  !$omp teams reduction(max:a, b, c)
  x = 1
  !$omp end teams

end program
