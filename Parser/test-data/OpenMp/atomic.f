! OMP 2.12.6

  !$omp atomic read
    x = 1
  !$omp end atomic

  !$omp atomic read seq_cst
    x = 1
  !$omp end atomic

  !$omp atomic read
    x = 1

  !$omp atomic read seq_cst
    x = 1

  !$omp atomic write
    x = 1
  !$omp end atomic

  !$omp atomic write seq_cst
    x = 1
  !$omp end atomic

  !$omp atomic write
    x = 1

  !$omp atomic write seq_cst
    x = 1

  !$omp atomic update
    x = 1
  !$omp end atomic

  !$omp atomic update seq_cst
    x = 1
  !$omp end atomic

  !$omp atomic update
    x = 1

  !$omp atomic update seq_cst
    x = 1

  !$omp atomic capture
    x = 1
    x = 2
  !$omp end atomic

  !$omp atomic capture seq_cst
    x = 1
    x = 2
  !$omp end atomic

end program
