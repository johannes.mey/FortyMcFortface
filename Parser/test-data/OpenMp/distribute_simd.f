! OMP 2.9.7

  !$omp distribute simd
  do i = 1,10
    x = 1
  end do

  !$omp distribute simd
  do i = 1,10
    x = 1
  end do
  !$omp end distribute simd

  !$omp distribute simd
  !$omp distribute simd
  do i = 1,10
    x = 1
  end do
  !$omp end distribute simd
  !$omp end distribute simd

end program
