! OMP 2.7.3

  !$omp single
  x = 1
  !$omp end single

  ! test all clauses

  !$omp single private(a, b, c)
  x = 1
  !$omp end single

  !$omp single firstprivate(a, b, c)
  x = 1
  !$omp end single

  !$omp single
  x = 1
  !$omp end single copyprivate(a, b, c)

  !$omp single
  x = 1
  !$omp end single nowait

end program
