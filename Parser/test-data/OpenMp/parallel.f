! OMP 2.5

  !$omp parallel
  x = 1
  !$omp end parallel

  !$omp parallel if(x)
  x = 1
  !$omp end parallel

  !$omp parallel if(x) if(y) if(z)
  x = 1
  !$omp end parallel

  !$omp parallel if(x)
  !$omp parallel if(x)
  !$omp parallel if(x)
  x = 1
  !$omp end parallel
  !$omp end parallel
  !$omp end parallel

  ! test all clauses

  !$omp parallel if(x)
  x = 1
  !$omp end parallel

  !$omp parallel num_threads(x)
  x = 1
  !$omp end parallel

  !$omp parallel default(private)
  x = 1
  !$omp end parallel

  !$omp parallel private(x)
  x = 1
  !$omp end parallel

  !$omp parallel firstprivate(x)
  x = 1
  !$omp end parallel

  !$omp parallel shared(x)
  x = 1
  !$omp end parallel

  !$omp parallel copyin(x)
  x = 1
  !$omp end parallel

  !$omp parallel reduction(+:x)
  x = 1
  !$omp end parallel

  !$omp parallel proc_bind(master)
  x = 1
  !$omp end parallel

end program
