! OMP 2.9.3

! test all clauses

  !$omp target update to(a, b, c)

  !$omp target update from(a, b, c)

  !$omp target update device(23)

  !$omp target update if(2 + 2)

end program
