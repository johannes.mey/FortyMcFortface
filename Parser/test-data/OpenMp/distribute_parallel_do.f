! OMP 2.9.8

  !$omp distribute parallel do
  do i = 1,10
    x = 1
  end do

  !$omp distribute parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end distribute parallel do

  !$omp distribute parallel do
  !$omp distribute parallel do
  do i = 1,10
    x = 1
  end do
  !$omp end distribute parallel do
  !$omp end distribute parallel do

end program
