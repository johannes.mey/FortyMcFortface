! OMP 2.9.8

  !$omp distribute parallel do simd
  do i = 1,10
    x = 1
  end do

  !$omp distribute parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end distribute parallel do simd

  !$omp distribute parallel do simd
  !$omp distribute parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end distribute parallel do simd
  !$omp end distribute parallel do simd

end program
