! OMP 2.10.4

  !$omp parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do simd

  !$omp parallel do simd
  !$omp parallel do simd
  do i = 1,10
    x = 1
  end do
  !$omp end parallel do simd
  !$omp end parallel do simd

end program
