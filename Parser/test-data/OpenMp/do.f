! OMP 2.7.1

  !$omp do
  do i = 1,10
    x = 1
  end do

  !$omp do
  do i = 1,10
    x = 1
  end do
  !$omp end do

  !$omp do
  do i = 1,10
    x = 1
  end do
  !$omp end do nowait

  !$omp do
  !$omp do
  do i = 1,10
    x = 1
  end do
  !$omp end do nowait
  !$omp end do nowait

  ! test all clauses

  !$omp do private(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp do firstprivate(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp do lastprivate(a, b, c)
  do i = 1,10
    x = 1
  end do

  !$omp do reduction(somethingelse:x, y, z)
  do i = 1,10
    x = 1
  end do

  !$omp do schedule(static, 3 + 4)
  do i = 1,10
    x = 1
  end do

  !$omp do collapse(25 * 2)
  do i = 1,10
    x = 1
  end do

  !$omp do ordered ordered
  do i = 1,10
    x = 1
  end do

end program
