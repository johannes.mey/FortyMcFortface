! OMP 2.7.1

  !$omp do simd
  do i = 1,10
    x = 1
  end do

  !$omp do simd private(a, b, c)
  do i = 1,10
    x = 1
  end do
  !$omp end do simd

  !$omp do simd
  do i = 1,10
    x = 1
  end do
  !$omp end do simd nowait

  !$omp do simd
  !$omp do simd
  do i = 1,10
    x = 1
  end do
  !$omp end do simd nowait
  !$omp end do simd nowait

end program
