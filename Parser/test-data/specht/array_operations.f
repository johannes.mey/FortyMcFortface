!> \file      array_operations.f
!> \brief     Standard operations with arrays
!> \author    Immo Huismann
!> \date      2015/01/05
!> \copyright Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> ## Rationale ##
!> This module provides operations on arrays that work on the targeted hardware,
!> providing an abstraction for subsequent modules.
!> As the PGI compiler is completely useless when compiling simple things like
!>
!>    !$acc kernels
!>    x = y * z
!>    !$acc end kernels
!>
!> leading to working, but very slow variants of the operation. This module
!> provides an easy way to get things working with the compiler.
!>
!> ## Programming ##
!> To enable the compilers to do some more optimization, all public routines
!> possess an explicit shape interface, while their called background
!> implementation uses an explicit size interface and only one loop.
!>
!> All routines inside this module possess a similar interface. The first array
!> passed is the value that will be set. Every other array will have intent(in).
!> If these vectors receive a factor, it will be passed before the specific
!> vector.
!>
!> Inside this module all arrays are named x, y, or z (and w in one instance).
!> Scalars are a, b, c, etc.
!>
!> \todo
!> * Most routines have an interface now. Those who do not, need to be purged
!>   or moved to a legacy module that can get purged with enough time.
!> * Redo the comments on all routines that don't sport formulas in brief.
!===============================================================================

module Array_Operations
  ! HiBASE modules
  use Kind_Parameters, only: RDP
  use Constants,       only: ONE

  ! Specht base modules
  use ACC_Parameters,  only: ACC_EXEC_QUEUE
  implicit none
  private

  public :: AddScaledVector
  public :: Scale
  public :: ScaleAndAddVector
  public :: MultiplyWithVector

  public :: SetToZero
  public :: SetToValue
  public :: SetToVector
  public :: SetToScaledVector
  public :: SetToTwoScaledVectors
  public :: SetToThreeScaledVectors
  public :: SetToMultipliedVectors

  public :: PointWiseInversion
  public :: PointWiseSqrt

  !-----------------------------------------------------------------------------
  !> \brief   Sets a vector to zero: x <- 0
  !> \author  Immo Huismann

  interface SetToZero
    module procedure SetToZero_1
    module procedure SetToZero_2
    module procedure SetToZero_3
    module procedure SetToZero_4

    module procedure SetToZero_432
  end interface SetToZero

  !-----------------------------------------------------------------------------
  !> \brief   Sets a vector to another one: x <- y
  !> \author  Immo Huismann

  interface SetToVector
    module procedure SetToVector_2
    module procedure SetToVector_3
    module procedure SetToVector_4
  end interface SetToVector

  !-----------------------------------------------------------------------------
  !> \brief   Scales a vector x <- a x
  !> \author  Immo Huismann

  interface Scale
    module procedure Scale_2
    module procedure Scale_3
    module procedure Scale_4

    module procedure Scale_432
  end interface Scale
  
  !-----------------------------------------------------------------------------
  !> \brief   Scales a vector and adds another to it: x <- a * x + y
  !> \author  Immo Huismann

  interface ScaleAndAddVector
    module procedure ScaleAndAddVector_2
    module procedure ScaleAndAddVector_3
    module procedure ScaleAndAddVector_4

    module procedure ScaleAndAddVector_432
  end interface ScaleAndAddVector

  !-----------------------------------------------------------------------------
  !> \brief   Adds a scaled vector to a vector: x <- x + a * y
  !> \author  Immo Huismann

  interface AddScaledVector
    module procedure AddScaledVector_2
    module procedure AddScaledVector_3
    module procedure AddScaledVector_4

    module procedure AddScaledVector_432
  end interface AddScaledVector

  !-----------------------------------------------------------------------------
  !> \brief   Sets to a scaled vector: x <- a * y
  !> \author  Immo Huismann

  interface SetToScaledVector
    module procedure SetToScaledVector_2
    module procedure SetToScaledVector_3
    module procedure SetToScaledVector_4

    module procedure SetToScaledVector_432
  end interface SetToScaledVector

  !-----------------------------------------------------------------------------
  !> \brief   Sets to the elementwise product of two vectors: x_i <- y_i * z_i
  !> \author  Immo Huismann

  interface SetToMultipliedVectors
    module procedure SetToMultipliedVectors_2
    module procedure SetToMultipliedVectors_3
    module procedure SetToMultipliedVectors_4

    module procedure SetToMultipliedVectors_432
  end interface SetToMultipliedVectors

  !-----------------------------------------------------------------------------
  !> \brief   Elementwise product of two vectors: x_i <- x_i * y_i
  !> \author  Immo Huismann

  interface MultiplyWithVector
    module procedure MultiplyWithVector_2
    module procedure MultiplyWithVector_3
    module procedure MultiplyWithVector_4

    module procedure MultiplyWithVector_432
  end interface MultiplyWithVector

  !-----------------------------------------------------------------------------
  !> \brief   Elementwise inversion of vector: x_i <- 1 / x_i
  !> \author  Immo Huismann

  interface PointWiseInversion
    module procedure PointWiseInversion_2
    module procedure PointWiseInversion_3
    module procedure PointWiseInversion_4

    module procedure PointWiseInversion_432
  end interface PointWiseInversion

  !-----------------------------------------------------------------------------
  !> \brief   Elementwise inversion of vector: x_i <- 1 / x_i
  !> \author  Immo Huismann

  interface PointWiseSqrt
    module procedure PointWiseSqrt_2
    module procedure PointWiseSqrt_3
    module procedure PointWiseSqrt_4

    module procedure PointWiseSqrt_432
  end interface PointWiseSqrt

  !-----------------------------------------------------------------------------
  !> \brief   Sets the variable to a given value: x_i <- a
  !> \author  Immo Huismann

  interface SetToValue
    module procedure SetToValue_2
    module procedure SetToValue_3
    module procedure SetToValue_4
  end interface SetToValue

contains

!===============================================================================
! Set some vector to zero

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero: x <- 0
!> \author  Immo Huismann

subroutine SetToZero_1(x)
  real(RDP), intent(out) :: x(:) !< x <- 0

  call SetToZero_ES(size(x),x)

end subroutine SetToZero_1

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero: x <- 0
!> \author  Immo Huismann

subroutine SetToZero_2(x)
  real(RDP), intent(out) :: x(:,:) !< x <- 0

  call SetToZero_ES(size(x),x)

end subroutine SetToZero_2

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero: x <- 0
!> \author  Immo Huismann

subroutine SetToZero_3(x)
  real(RDP), intent(out) :: x(:,:,:) !< x <- 0

  call SetToZero_ES(size(x),x)

end subroutine SetToZero_3

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero: x <- 0
!> \author  Immo Huismann

subroutine SetToZero_4(x)
  real(RDP), intent(out) :: x(:,:,:,:) !< x <- 0

  call SetToZero_ES(size(x),x)

end subroutine SetToZero_4

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero: x <- 0
!> \author  Immo Huismann

subroutine SetToZero_432(x_4,x_3,x_2)
  real(RDP), intent(out) :: x_2(:,:)     !< x <- 0
  real(RDP), intent(out) :: x_3(:,:,:)   !< x <- 0
  real(RDP), intent(out) :: x_4(:,:,:,:) !< x <- 0

  call SetToZero_ES(size(x_2),x_2)
  call SetToZero_ES(size(x_3),x_3)
  call SetToZero_ES(size(x_4),x_4)

end subroutine SetToZero_432

!-------------------------------------------------------------------------------
!> \brief   Sets a vector to zero, explicit size
!> \author  Immo Huismann

subroutine SetToZero_ES(n_points,x)
  integer,   intent(in)  ::   n_points  !< number of points to work on
  real(RDP), intent(out) :: x(n_points) !< x_i <- 0

  integer :: i

  !$acc data present(x)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i  = 1, n_points
    x(i) = 0
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToZero_ES

!===============================================================================
! Add scaled vectors

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable x <- x + a * y
!> \author  Immo Huismann

subroutine AddScaledVector_2(x,a,y)
  real(RDP), intent(inout) :: x(:,:) !< x <- x + a * y
  real(RDP), intent(in)    :: a      !< scaling factor
  real(RDP), intent(in)    :: y(:,:) !< array to scale and add to x

  call AddScaledVector_ES(size(x),x,a,y)

end subroutine AddScaledVector_2

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable x <- x + a * y
!> \author  Immo Huismann

subroutine AddScaledVector_3(x,a,y)
  real(RDP), intent(inout) :: x(:,:,:) !< x <- x + a * y
  real(RDP), intent(in)    :: a        !< scaling factor
  real(RDP), intent(in)    :: y(:,:,:) !< array to scale and add to x

  call AddScaledVector_ES(size(x),x,a,y)

end subroutine AddScaledVector_3

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable x <- x + a * y
!> \author  Immo Huismann

subroutine AddScaledVector_4(x,a,y)
  real(RDP), intent(inout) :: x(:,:,:,:) !< x <- x + a * y
  real(RDP), intent(in)    :: a          !< scaling factor
  real(RDP), intent(in)    :: y(:,:,:,:) !< array to scale and add to x

  call AddScaledVector_ES(size(x),x,a,y)

end subroutine AddScaledVector_4

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable x <- x + a * y
!> \author  Immo Huismann

subroutine AddScaledVector_432(x_4,x_3,x_2,a,y_4,y_3,y_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x <- x + a * y
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x <- x + a * y
  real(RDP), intent(inout) :: x_2(:,:)     !< x <- x + a * y
  real(RDP), intent(in)    :: a            !< scaling factor
  real(RDP), intent(in)    :: y_4(:,:,:,:) !< array to scale and add to x
  real(RDP), intent(in)    :: y_3(:,:,:)   !< array to scale and add to x
  real(RDP), intent(in)    :: y_2(:,:)     !< array to scale and add to x

  call AddScaledVector_ES(size(x_4),x_4,a,y_4)
  call AddScaledVector_ES(size(x_3),x_3,a,y_3)
  call AddScaledVector_ES(size(x_2),x_2,a,y_2)

end subroutine AddScaledVector_432

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable
!> \author  Immo Huismann

subroutine AddScaledVector_ES(n_points,x,a,y)
  integer,   intent(in)    ::   n_points  !< number of points to work on
  real(RDP), intent(inout) :: x(n_points) !< x_i <- x_i + a * y_i
  real(RDP), intent(in)    :: a           !< scaling factor
  real(RDP), intent(in)    :: y(n_points) !< array to scale and add to x

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i  = 1, n_points
    x(i) = x(i) + a * y(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine AddScaledVector_ES

!===============================================================================
! Scales a vector

!-------------------------------------------------------------------------------
!> \brief   Scales a vector x <- a * x
!> \author  Immo Huismann

subroutine Scale_2(x,a)
  real(RDP), intent(inout) :: x(:,:) !< vector to scale
  real(RDP), intent(in)    :: a      !< scaling factor

  call Scale_ES(size(x),x,a)

end subroutine Scale_2

!-------------------------------------------------------------------------------
!> \brief   Scales a vector x <- a * x
!> \author  Immo Huismann

subroutine Scale_3(x,a)
  real(RDP), intent(inout) :: x(:,:,:) !< vector to scale
  real(RDP), intent(in)    :: a        !< scaling factor

  call Scale_ES(size(x),x,a)

end subroutine Scale_3

!-------------------------------------------------------------------------------
!> \brief   Scales a vector x <- a * x
!> \author  Immo Huismann

subroutine Scale_4(x,a)
  real(RDP), intent(inout) :: x(:,:,:,:) !< vector to scale and add to
  real(RDP), intent(in)    :: a          !< scaling factor

  call Scale_ES(size(x),x,a)

end subroutine Scale_4

!-------------------------------------------------------------------------------
!> \brief   Scales a vector x <- a * x
!> \author  Immo Huismann

subroutine Scale_432(x_4,x_3,x_2,a)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x <- x
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x <- x
  real(RDP), intent(inout) :: x_2(:,:)     !< x <- x
  real(RDP), intent(in)    :: a            !< scaling factor

  call Scale_ES(size(x_4),x_4,a)
  call Scale_ES(size(x_3),x_3,a)
  call Scale_ES(size(x_2),x_2,a)

end subroutine Scale_432

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it (aka daxpy)
!> \author  Immo Huismann

subroutine Scale_ES(n_points,x,a)
  integer,   intent(in)    ::   n_points  !< number of points in the vector
  real(RDP), intent(inout) :: x(n_points) !< vector to scale and add to
  real(RDP), intent(in)    :: a           !< scaling factor

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i  = 1, n_points
    x(i) = a * x(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine Scale_ES

!===============================================================================
! Scale and add vectors

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it x <- a * x + y
!> \author  Immo Huismann

subroutine ScaleAndAddVector_2(x,a,y)
  real(RDP), intent(inout) :: x(:,:) !< vector to scale and add to
  real(RDP), intent(in)    :: a      !< scaling factor
  real(RDP), intent(in)    :: y(:,:) !< vector to add to x

  call ScaleAndAddVector_ES(size(x),x,a,y)

end subroutine ScaleAndAddVector_2

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it x <- a * x + y
!> \author  Immo Huismann

subroutine ScaleAndAddVector_3(x,a,y)
  real(RDP), intent(inout) :: x(:,:,:) !< vector to scale and add to
  real(RDP), intent(in)    :: a        !< scaling factor
  real(RDP), intent(in)    :: y(:,:,:) !< vector to add to x

  call ScaleAndAddVector_ES(size(x),x,a,y)

end subroutine ScaleAndAddVector_3

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it x <- a * x + y
!> \author  Immo Huismann

subroutine ScaleAndAddVector_4(x,a,y)
  real(RDP), intent(inout) :: x(:,:,:,:) !< vector to scale and add to
  real(RDP), intent(in)    :: a          !< scaling factor
  real(RDP), intent(in)    :: y(:,:,:,:) !< vector to add to x

  call ScaleAndAddVector_ES(size(x),x,a,y)

end subroutine ScaleAndAddVector_4

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it x <- a * x + y
!> \author  Immo Huismann

subroutine ScaleAndAddVector_432(x_4,x_3,x_2,a,y_4,y_3,y_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x <- x + a * y
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x <- x + a * y
  real(RDP), intent(inout) :: x_2(:,:)     !< x <- x + a * y
  real(RDP), intent(in)    :: a            !< scaling factor
  real(RDP), intent(in)    :: y_4(:,:,:,:) !< array to scale and add to x
  real(RDP), intent(in)    :: y_3(:,:,:)   !< array to scale and add to x
  real(RDP), intent(in)    :: y_2(:,:)     !< array to scale and add to x

  call ScaleAndAddVector_ES(size(x_4),x_4,a,y_4)
  call ScaleAndAddVector_ES(size(x_3),x_3,a,y_3)
  call ScaleAndAddVector_ES(size(x_2),x_2,a,y_2)

end subroutine ScaleAndAddVector_432

!-------------------------------------------------------------------------------
!> \brief   Scales a vector and adds another one to it (aka daxpy)
!> \author  Immo Huismann

subroutine ScaleAndAddVector_ES(n_points,x,a,y)
  integer,   intent(in)    ::   n_points  !< number of points in the vector
  real(RDP), intent(inout) :: x(n_points) !< vector to scale and add to
  real(RDP), intent(in)    :: a           !< scaling factor
  real(RDP), intent(in)    :: y(n_points) !< vector to add to x

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i  = 1, n_points
    x(i) = a * x(i) + y(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine ScaleAndAddVector_ES

!===============================================================================
! Set to scaled vectors

!-------------------------------------------------------------------------------
!> \brief   Sets x <- a * y
!> \author  Immo Huismann

subroutine SetToScaledVector_2(x,a,y)
  real(RDP), intent(out) :: x(:,:) !< x <- a * y
  real(RDP), intent(in)  :: a      !< scaling factor for y
  real(RDP), intent(in)  :: y(:,:) !< vector to scale and store in x

  call SetToScaledVector_ES(size(x),x,a,y)

end subroutine SetToScaledVector_2

!-------------------------------------------------------------------------------
!> \brief   Sets \f$ x <- a * y \f$
!> \author  Immo Huismann

subroutine SetToScaledVector_3(x,a,y)
  real(RDP), intent(out) :: x(:,:,:) !< x <- a * y
  real(RDP), intent(in)  :: a        !< scaling factor for y
  real(RDP), intent(in)  :: y(:,:,:) !< vector to scale and store in x

  call SetToScaledVector_ES(size(x),x,a,y)

end subroutine SetToScaledVector_3

!-------------------------------------------------------------------------------
!> \brief   Sets x <- a * y
!> \author  Immo Huismann

subroutine SetToScaledVector_4(x,a,y)
  real(RDP), intent(in)  :: y(:,:,:,:) !< x <- a * y
  real(RDP), intent(in)  :: a          !< scaling factor for y
  real(RDP), intent(out) :: x(:,:,:,:) !< vector to scale and store in x

  call SetToScaledVector_ES(size(x),x,a,y)

end subroutine SetToScaledVector_4

!-------------------------------------------------------------------------------
!> \brief   Sets x <- a * y
!> \author  Immo Huismann

subroutine SetToScaledVector_432(x_4,x_3,x_2,a,y_4,y_3,y_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x <- a * y
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x <- a * y
  real(RDP), intent(inout) :: x_2(:,:)     !< x <- a * y
  real(RDP), intent(in)    :: a            !< scaling factor for y
  real(RDP), intent(in)    :: y_4(:,:,:,:) !< vector to scale and store in x
  real(RDP), intent(in)    :: y_3(:,:,:)   !< vector to scale and store in x
  real(RDP), intent(in)    :: y_2(:,:)     !< vector to scale and store in x

  call SetToScaledVector_ES(size(x_4),x_4,a,y_4)
  call SetToScaledVector_ES(size(x_3),x_3,a,y_3)
  call SetToScaledVector_ES(size(x_2),x_2,a,y_2)

end subroutine SetToScaledVector_432

!-------------------------------------------------------------------------------
!> \brief   Sets x <- a * y - explicit size subroutine
!> \author  Immo Huismann

subroutine SetToScaledVector_ES(n_points,x,a,y)
  integer,   intent(in)  ::   n_points  !< number of points to work on
  real(RDP), intent(out) :: x(n_points) !< x_i <- a * y_i
  real(RDP), intent(in)  :: a           !< scaling factor for y
  real(RDP), intent(in)  :: y(n_points) !< vector to scale and store in x

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_points
    x(i) = a * y(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToScaledVector_ES


!===============================================================================
! Multiply two vectors, store in first

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication  x_i <- x_i * y_i
!> \author  Immo Huismann

subroutine MultiplyWithVector_2(x,y)
  real(RDP), intent(inout) :: x(:,:) !< vector to save into
  real(RDP), intent(in)    :: y(:,:) !< vector to multiply it with

  call MultiplyWithVector_ES(size(x),x,y)

end subroutine MultiplyWithVector_2

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication  x_i <- x_i * y_i
!> \author  Immo Huismann

subroutine MultiplyWithVector_3(x,y)
  real(RDP), intent(inout) :: x(:,:,:) !< vector to save into
  real(RDP), intent(in)    :: y(:,:,:) !< vector to multiply it with

  call MultiplyWithVector_ES(size(x),x,y)

end subroutine MultiplyWithVector_3

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication  x_i <- x_i * y_i
!> \author  Immo Huismann

subroutine MultiplyWithVector_4(x,y)
  real(RDP), intent(inout) :: x(:,:,:,:) !< vector to save into
  real(RDP), intent(in)    :: y(:,:,:,:) !< vector to multiply it with

  call MultiplyWithVector_ES(size(x),x,y)

end subroutine MultiplyWithVector_4

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication  x_i <- x_i * y_i
!> \author  Immo Huismann

subroutine MultiplyWithVector_432(x_4,x_3,x_2,y_4,y_3,y_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< vector to save into
  real(RDP), intent(inout) :: x_3(:,:,:)   !< vector to save into
  real(RDP), intent(inout) :: x_2(:,:)     !< vector to save into
  real(RDP), intent(in)    :: y_4(:,:,:,:) !< vector to multiply it with
  real(RDP), intent(in)    :: y_3(:,:,:)   !< vector to multiply it with
  real(RDP), intent(in)    :: y_2(:,:)     !< vector to multiply it with

  call MultiplyWithVector_ES(size(x_4),x_4,y_4)
  call MultiplyWithVector_ES(size(x_3),x_3,y_3)
  call MultiplyWithVector_ES(size(x_2),x_2,y_2)

end subroutine MultiplyWithVector_432

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication  x_i <- x_i * y_i
!> \author  Immo Huismann

subroutine MultiplyWithVector_ES(n_points,x,y)
  integer,   intent(in)    ::   n_points  !< number of points in the vectors
  real(RDP), intent(inout) :: x(n_points) !< first vector, store result in it
  real(RDP), intent(in)    :: y(n_points) !< second vector, multiply with it

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i = 1, n_points
    x(i) = x(i) * y(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine MultiplyWithVector_ES

!===============================================================================
! Set a vector to the elementwise product of two other vectors

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication x_i <- y_i * z_i
!> \author  Immo Huismann

subroutine SetToMultipliedVectors_2(x,y,z)
  real(RDP), intent(out) :: x(:,:) !< array to store in
  real(RDP), intent(in)  :: y(:,:)
  real(RDP), intent(in)  :: z(:,:)

  call SetToMultipliedVectors_ES(size(x),x,y,z)

end subroutine SetToMultipliedVectors_2

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication x_i <- y_i * z_i
!> \author  Immo Huismann

subroutine SetToMultipliedVectors_3(x,y,z)
  real(RDP), intent(out) :: x(:,:,:) !< array to store in
  real(RDP), intent(in)  :: y(:,:,:)
  real(RDP), intent(in)  :: z(:,:,:)

  call SetToMultipliedVectors_ES(size(x),x,y,z)

end subroutine SetToMultipliedVectors_3

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication x_i <- y_i * z_i
!> \author  Immo Huismann

subroutine SetToMultipliedVectors_4(x,y,z)
  real(RDP), intent(out) :: x(:,:,:,:) !< array to store in
  real(RDP), intent(in)  :: y(:,:,:,:)
  real(RDP), intent(in)  :: z(:,:,:,:)

  call SetToMultipliedVectors_ES(size(x),x,y,z)

end subroutine SetToMultipliedVectors_4

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication x_i <- y_i * z_i
!> \author Immo Huismann

subroutine SetToMultipliedVectors_432(x_4,x_3,x_2, y_4,y_3,y_2, z_4,z_3,z_2)
  real(RDP), intent(out) :: x_4(:,:,:,:) !< x_i <- y_i * z_i
  real(RDP), intent(out) :: x_3(:,:,:)   !< x_i <- y_i * z_i
  real(RDP), intent(out) :: x_2(:,:)     !< x_i <- y_i * z_i
  real(RDP), intent(in)  :: y_4(:,:,:,:)
  real(RDP), intent(in)  :: y_3(:,:,:)
  real(RDP), intent(in)  :: y_2(:,:)
  real(RDP), intent(in)  :: z_4(:,:,:,:)
  real(RDP), intent(in)  :: z_3(:,:,:)
  real(RDP), intent(in)  :: z_2(:,:)

  call SetToMultipliedVectors_ES(size(x_4),x_4,y_4,z_4)
  call SetToMultipliedVectors_ES(size(x_3),x_3,y_3,z_3)
  call SetToMultipliedVectors_ES(size(x_2),x_2,y_2,z_2)

end subroutine SetToMultipliedVectors_432

!-------------------------------------------------------------------------------
!> \brief   Pointwise multiplication x_i <- y_i * z_i -  explicit size routine
!> \author  Immo Huismann

subroutine SetToMultipliedVectors_ES(n_points,x,y,z)
  integer,   intent(in)  ::   n_points  !< number of points to work on
  real(RDP), intent(out) :: x(n_points) !< x_i <- y_i * z_i
  real(RDP), intent(in)  :: y(n_points) !< First array in multiplication
  real(RDP), intent(in)  :: z(n_points) !< Second array in multiplication

  integer :: i

  !$acc data present(x,y,z)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i = 1, n_points
    x(i) = y(i) * z(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToMultipliedVectors_ES

!===============================================================================
! Copy a vector

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value, x <- y
!> \author Immo Huismann

subroutine SetToVector_2(x,y)
  real(RDP), intent(out) :: x(:,:) !< x <- y
  real(RDP), intent(in)  :: y(:,:) !< vector to set it to

  call SetToVector_ES(size(x),x,y)

end subroutine SetToVector_2

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value, x <- y
!> \author Immo Huismann

subroutine SetToVector_3(x,y)
  real(RDP), intent(out) :: x(:,:,:) !< x <- y
  real(RDP), intent(in)  :: y(:,:,:) !< vector to set it to

  call SetToVector_ES(size(x),x,y)

end subroutine SetToVector_3

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value, x <- y
!> \author Immo Huismann

subroutine SetToVector_4(x,y)
  real(RDP), intent(out) :: x(:,:,:,:) !< x <- y
  real(RDP), intent(in)  :: y(:,:,:,:) !< vector to set it to

  call SetToVector_ES(size(x),x,y)

end subroutine SetToVector_4

!-------------------------------------------------------------------------------
!> \brief  Sets containing n entries to zero
!> \author Immo Huismann

subroutine SetToVector_ES(n_point,x,y)
  integer,   intent(in)  ::   n_point  !< number of points to work on
  real(RDP), intent(out) :: x(n_point) !< x_i <- y_i
  real(RDP), intent(in)  :: y(n_point) !< vector to set x to

  integer :: i

  !$acc data present(x,y)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do private(i)
  do i = 1, n_point
    x(i) = y(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToVector_ES

!===============================================================================
! Pointwise inversion

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array, x_i <- 1 / x_i
!> \author Immo Huismann

subroutine PointWiseInversion_2(x)
  real(RDP), intent(inout) :: x(:,:) !< x_i <- 1 / x_i

  call PointWiseInversion_ES(size(x),x)

end subroutine PointWiseInversion_2

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array, x_i <- 1 / x_i
!> \author Immo Huismann

subroutine PointWiseInversion_3(x)
  real(RDP), intent(inout) :: x(:,:,:) !< x_i <- 1 / x_i

  call PointWiseInversion_ES(size(x),x)

end subroutine PointWiseInversion_3

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array.
!> \author Immo Huismann

subroutine PointWiseInversion_4(x)
  real(RDP), intent(inout) :: x(:,:,:,:) !< x_i <- 1 / x_i

  call PointWiseInversion_ES(size(x),x)

end subroutine PointWiseInversion_4

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array.
!> \author Immo Huismann

subroutine PointWiseInversion_432(x_4,x_3,x_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x_i <- 1 / x_i
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x_i <- 1 / x_i
  real(RDP), intent(inout) :: x_2(:,:)     !< x_i <- 1 / x_i

  call PointWiseInversion_ES(size(x_4),x_4)
  call PointWiseInversion_ES(size(x_3),x_3)
  call PointWiseInversion_ES(size(x_2),x_2)

end subroutine PointWiseInversion_432

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array - explicit size version
!> \author Immo Huismann

subroutine PointWiseInversion_ES(n_point,x)
  integer,   intent(in)    ::   n_point  !< number of points to process
  real(RDP), intent(inout) :: x(n_point) !< x_i <- 1 / x_i

  integer :: i

  !$acc data present(x)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_point
    x(i) = ONE / x(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine PointWiseInversion_ES

!===============================================================================
! Sqrt

!-------------------------------------------------------------------------------
!> \brief  Square root of every element of an array, x_i <- sqrt(x_i)
!> \author Immo Huismann

subroutine PointWiseSqrt_2(x)
  real(RDP), intent(inout) :: x(:,:) !< x_i <- sqrt(x_i)

  call PointWiseSqrt_ES(size(x),x)

end subroutine PointWiseSqrt_2

!-------------------------------------------------------------------------------
!> \brief  Square root of every element of an array, x_i <- sqrt(x_i)
!> \author Immo Huismann

subroutine PointWiseSqrt_3(x)
  real(RDP), intent(inout) :: x(:,:,:) !< x_i <- sqrt(x_i)

  call PointWiseSqrt_ES(size(x),x)

end subroutine PointWiseSqrt_3

!-------------------------------------------------------------------------------
!> \brief  Square root of every element of an array, x_i <- sqrt(x_i)
!> \author Immo Huismann

subroutine PointWiseSqrt_4(x)
  real(RDP), intent(inout) :: x(:,:,:,:) !< x_i <- sqrt(x_i)

  call PointWiseSqrt_ES(size(x),x)

end subroutine PointWiseSqrt_4

!-------------------------------------------------------------------------------
!> \brief  Square root of every element of an array, x_i <- sqrt(x_i)
!> \author Immo Huismann

subroutine PointWiseSqrt_432(x_4,x_3,x_2)
  real(RDP), intent(inout) :: x_4(:,:,:,:) !< x_i <- sqrt(x_i)
  real(RDP), intent(inout) :: x_3(:,:,:)   !< x_i <- sqrt(x_i)
  real(RDP), intent(inout) :: x_2(:,:)     !< x_i <- sqrt(x_i)

  call PointWiseSqrt_ES(size(x_4),x_4)
  call PointWiseSqrt_ES(size(x_3),x_3)
  call PointWiseSqrt_ES(size(x_2),x_2)

end subroutine PointWiseSqrt_432

!-------------------------------------------------------------------------------
!> \brief  Inverts every element of an array - explicit size version
!> \author Immo Huismann

subroutine PointWiseSqrt_ES(n_point,x)
  integer,   intent(in)    ::   n_point  !< number of points to work on
  real(RDP), intent(inout) :: x(n_point) !< x_i <- sqrt(x_i)

  integer :: i

  !$acc data present(x)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_point
    x(i) = sqrt(x(i))
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine PointWiseSqrt_ES

!===============================================================================
! x <- a * y + b * z

!-------------------------------------------------------------------------------
!> \brief   Explicit size routine for x <- a * y + b * z
!> \author  Immo Huismann

subroutine SetToTwoScaledVectors_ES(n_points,x,a,y,b,z)
  integer,   intent(in)  ::   n_points
  real(RDP), intent(out) :: x(n_points) !< array to set
  real(RDP), intent(in)  :: a           !< scaling factor for y
  real(RDP), intent(in)  :: y(n_points) !< array to set x to
  real(RDP), intent(in)  :: b           !< scaling factor for z
  real(RDP), intent(in)  :: z(n_points) !< array to scale and add to x

  integer :: i

  !$acc data present(x,y,z)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_points
    x(i) = a * y(i) + b * z(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToTwoScaledVectors_ES

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable w <- a * x + b * y + c * z
!> \author  Immo Huismann

subroutine SetToThreeScaledVectors(w,a,x,b,y,c,z)
  real(RDP), intent(out) :: w(:,:,:,:) !< array to set
  real(RDP), intent(in)  :: a          !< scaling factor for x
  real(RDP), intent(in)  :: x(:,:,:,:) !< first array to set x to
  real(RDP), intent(in)  :: b          !< scaling factor for z
  real(RDP), intent(in)  :: y(:,:,:,:) !< second array to scale and add to w
  real(RDP), intent(in)  :: c          !< scaling factor for z
  real(RDP), intent(in)  :: z(:,:,:,:) !< third array to scale and add to w

  call SetToThreeScaledVectors_ES(size(x),w,a,x,b,y,c,z)

end subroutine SetToThreeScaledVectors

!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable w <- a * x + b * y + c * z
!> \author  Immo Huismann

subroutine SetToThreeScaledVectors_ES(n_points,w,a,x,b,y,c,z)
  integer,   intent(in)  ::   n_points  !< number of points to process
  real(RDP), intent(out) :: w(n_points) !< array to set
  real(RDP), intent(in)  :: a           !< scaling factor for x
  real(RDP), intent(in)  :: x(n_points) !< first array to set x to
  real(RDP), intent(in)  :: b           !< scaling factor for z
  real(RDP), intent(in)  :: y(n_points) !< second array to scale and add to w
  real(RDP), intent(in)  :: c           !< scaling factor for z
  real(RDP), intent(in)  :: z(n_points) !< third array to scale and add to w

  integer :: i

  !$acc data present(w,x,y,z)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_points
    w(i) = a * x(i) + b * y(i) + c * z(i)
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToThreeScaledVectors_ES


!-------------------------------------------------------------------------------
!> \brief   Adds a scaled vector to the variable x <- a * y + b * z
!> \author  Immo Huismann

subroutine SetToTwoScaledVectors(x,a,y,b,z)
  real(RDP), intent(out) :: x(:,:,:,:) !< array to set
  real(RDP), intent(in)  :: a          !< scaling factor for y
  real(RDP), intent(in)  :: y(:,:,:,:) !< array to set x to
  real(RDP), intent(in)  :: b          !< scaling factor for z
  real(RDP), intent(in)  :: z(:,:,:,:) !< array to scale and add to x

  call SetToTwoScaledVectors_ES(size(x),x,a,y,b,z)

end subroutine SetToTwoScaledVectors

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value: x_i <- a
!> \author Immo Huismann

subroutine SetToValue_2(x,a)
  real(RDP), intent(out) :: x(:,:)
  real(RDP), intent(in)  :: a

  call SetToValue_ES(size(x),x,a)

end subroutine SetToValue_2

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value: x_i <- a
!> \author Immo Huismann

subroutine SetToValue_3(x,a)
  real(RDP), intent(out) :: x(:,:,:)
  real(RDP), intent(in)  :: a

  call SetToValue_ES(size(x),x,a)

end subroutine SetToValue_3

!-------------------------------------------------------------------------------
!> \brief  Sets the variable to a given value: x_i <- a
!> \author Immo Huismann

subroutine SetToValue_4(x,a)
  real(RDP), intent(out) :: x(:,:,:,:)
  real(RDP), intent(in)  :: a

  call SetToValue_ES(size(x),x,a)

end subroutine SetToValue_4

!-------------------------------------------------------------------------------
!> \brief  Sets containing n entries to a, x_i <- a
!> \author Immo Huismann

subroutine SetToValue_ES(n_point,x,a)
  integer,   intent(in)  ::   n_point  !< number of points in array
  real(RDP), intent(out) :: x(n_point) !< array to set to a
  real(RDP), intent(in)  :: a          !< value to set array to

  integer :: i

  !$acc data present(x)
  !$acc parallel async(ACC_EXEC_QUEUE)
  !$acc loop
  !$omp do   private(i)
  do i = 1, n_point
    x(i) = a
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel
  !$acc end data

end subroutine SetToValue_ES

!===============================================================================

end module Array_Operations
