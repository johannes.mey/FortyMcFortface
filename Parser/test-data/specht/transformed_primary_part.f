!> \file      eigenspace_primary_part.f
!> \brief     Primary part for condensed face eigenspace solver
!> \author    Immo Huismann
!> \date      2015/05/20
!> \copyright Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This module provides the primary part for the condensed system eigenspace
!> solver.
!>
!> \todo
!> * The OpenACC queues could be used for latency hiding via task parallelism
!===============================================================================

module Transformed_Primary_Part
  ! HiBASE
  use Kind_Parameters,             only: RDP

  ! Specht
  use ACC_Parameters,              only: ACC_EXEC_QUEUE
  use Condensed_SEM_Operators,     only: CondensedOperators1D
  use Element_Boundary_Parameters, only: N_FACES, N_EDGES, N_VERTICES
  implicit none
  private

  public :: AddTransformedPrimaryPart

contains

!-------------------------------------------------------------------------------
!> \brief   Adds the primary part to the result vector.
!> \author  Immo Huismann
!>
!> \details
!> * Assumes that d,u_f,u_e,u_v,r_f,r_e,r_v are on the accelerator

subroutine AddTransformedPrimaryPart(cop,d,u_f,u_e,u_v,r_f,r_e,r_v)
  class(CondensedOperators1D), intent(in) :: cop     !< 1D condensed operators
  real(RDP),                   intent(in) :: d(0:,:) !< Helmholtz coefficents
  real(RDP), intent(in)    :: u_f(:,:,:,:)           !< u values on faces
  real(RDP), intent(in)    :: u_e(  :,:,:)           !< u values on edges
  real(RDP), intent(in)    :: u_v(    :,:)           !< u values on vertices
  real(RDP), intent(inout) :: r_f(:,:,:,:)           !< r values on faces
  real(RDP), intent(inout) :: r_e(  :,:,:)           !< r values on edges
  real(RDP), intent(inout) :: r_v(    :,:)           !< r values on vertices

  ! Add the single contributions
  
  call AddFaceContribution  (cop,d,u_f,        r_f,r_e    )
  call AddEdgeContribution  (cop,d,    u_e,    r_f,r_e,r_v)
  call AddVertexContribution(cop,d,        u_v,    r_e,r_v)

end subroutine AddTransformedPrimaryPart

!===============================================================================
! Internals

!-------------------------------------------------------------------------------
!> \brief   Adds the contribution of the faces to the residual
!> \author  Immo Huismann

subroutine AddFaceContribution(cop,d,u_f,r_f,r_e)
  class(CondensedOperators1D), intent(in) :: cop
  real(RDP), intent(in)    :: d(0:,:)      !< metric coefficients  (0:3,   n_e)
  real(RDP), intent(in)    :: u_f(:,:,:,:) !< values   on faces    (n,n,6, n_e)
  real(RDP), intent(inout) :: r_f(:,:,:,:) !< residual on faces    (n,n,6, n_e)
  real(RDP), intent(inout) :: r_e(  :,:,:) !< residual on edges    (  n,12,n_e)

  integer   :: n, n_element
  real(RDP) :: M_00, L_00, L_0p

  n         = size(u_f,1)
  n_element = size(u_f,4)

  M_00 = cop%w(0)
  L_00 = cop%L(0,0)
  L_0p = cop%L(0,1+n)

  call FaceToEdge(n,n_element,M_00,          cop%L_0I_S,cop%L_pI_S,d,u_f,r_e)
  call FaceToFace(n,n_element,M_00,L_00,L_0p,cop%eig,              d,u_f,r_f)

end subroutine AddFaceContribution

!-------------------------------------------------------------------------------
!> \brief   Performs edge to edge operations of primary part
!> \author  Immo Huismann

subroutine AddEdgeContribution(cop,d,u_e,r_f,r_e,r_v)
  class(CondensedOperators1D), intent(in) :: cop
  real(RDP), intent(in)    :: d(0:,:)      !< metric coefficients  (0:3,   n_e)
  real(RDP), intent(in)    :: u_e(  :,:,:) !< values on the edges  (  n,12,n_e)
  real(RDP), intent(inout) :: r_f(:,:,:,:) !< residual on faces    (n,n, 6,n_e)
  real(RDP), intent(inout) :: r_e(  :,:,:) !< residual on edges    (  n,12,n_e)
  real(RDP), intent(inout) :: r_v(    :,:) !< residual on vertices (     8,n_e)

  integer   :: n, n_element
  real(RDP) :: M_00, L_00, L_0p
  real(RDP) :: L_0I   (size(cop%S,1)), L_pI   (size(cop%S,1))
  real(RDP) :: L_0I_S (size(cop%S,1)), L_pI_S (size(cop%S,1))
  real(RDP) :: L_I0   (size(cop%S,1)), L_Ip   (size(cop%S,1))
  real(RDP) :: S_T_LI0(size(cop%S,1)), S_T_LIp(size(cop%S,1))
  real(RDP) :: eig(size(cop%eig))

  n         = size(u_e,1)
  n_element = size(u_e,3)

  M_00 = cop%w(0)
  L_00 = cop%L(0,0)
  L_0p = cop%L(0,1+n)
  L_pI = cop%L(1+n,1:n)
  L_0I = cop%L(0,  1:n)
  L_Ip = cop%L(1:n,1+n)
  L_I0 = cop%L(1:n,0  )

  L_0I_S  = cop%L_0I_S
  L_pI_S  = cop%L_pI_S
  S_T_LI0 = cop%S_T_L_I0
  S_T_LIp = cop%S_T_L_Ip

  eig     = cop%eig

  call EdgeToVertex(n,n_element,M_00,L_0I_S,L_pI_S,  d,u_e,r_v)
  call EdgeToEdge  (n,n_element,M_00,L_00,L_0p,eig,  d,u_e,    r_e)
  call EdgeToFace  (n,n_element,M_00,S_T_LI0,S_T_LIp,d,u_e,        r_f)

end subroutine AddEdgeContribution

!-------------------------------------------------------------------------------
!> \brief   Add the vertices' contribution to the primary part
!> \author  Immo Huismann

subroutine AddVertexContribution(cop,d,u_v,r_e,r_v)
  class(CondensedOperators1D), intent(in) :: cop !< condensed 1D operators
  real(RDP), intent(in)    :: d(:,0:)      !< metric coefficients  (    n_e,0:3)
  real(RDP), intent(in)    :: u_v(    :,:) !< values on vertices   (    n_e, 6)
  real(RDP), intent(inout) :: r_e(  :,:,:) !< residual on edges    (  n,n_e,12)
  real(RDP), intent(inout) :: r_v(    :,:) !< residual on vertices (    n_e, 8)

  integer   :: n, n_element
  real(RDP) :: M_00, L_00, L_0p
  real(RDP) :: S_T_L_I0(size(cop%S,1))
  real(RDP) :: S_T_L_Ip(size(cop%S,1))

  n         = size(r_e,1)
  n_element = size(r_e,3)

  M_00     = cop%w(0)
  L_00     = cop%L(0,0)
  L_0p     = cop%L(0,1+n)

  S_T_L_I0 = cop%S_T_L_I0
  S_T_L_Ip = cop%S_T_L_Ip

  call VertexToVertex(n_element,M_00,L_00,L_0p,        d,u_v,r_v)
  call VertexToEdge(n,n_element,M_00,S_T_L_I0,S_T_L_Ip,d,u_v,    r_e)

end subroutine AddVertexContribution

!===============================================================================
! Particular terms

!-------------------------------------------------------------------------------
!> \brief   Face to face term
!> \author  Immo Huismann

subroutine FaceToFace(n,n_element,M_00,L_00,L_0p,eig,d,u_f,r_f)
  integer,   intent(in) :: n                !< points per edge
  integer,   intent(in) :: n_element        !< number of elements
  real(RDP), intent(in) :: M_00             !< M_{00}
  real(RDP), intent(in) :: L_00             !< L_{00}
  real(RDP), intent(in) :: L_0p             !< L_{0p}
  real(RDP), intent(in) :: eig(n)           !< gen. eigenvalues of L_{II}
  real(RDP), intent(in) :: d(0:3,n_element) !< metric coefficients

  real(RDP), intent(in)    :: u_f(n,n,N_FACES,n_element) !< values on faces
  real(RDP), intent(inout) :: r_f(n,n,N_FACES,n_element) !< result on edges

  integer :: i,j,e

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_f,r_f)
  !$acc loop
  !$omp do private(i,j,e)
  do e = 1, n_element
    !...........................................................................
    ! x_1 faces
    !$acc loop collapse(2)
    do j = 1, n
    do i = 1, n
      r_f(i,j,1,e) =                                    r_f(i,j,1,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,1,e)           &
          +           d(1,e) * L_00                   * u_f(i,j,1,e)           &
          +           d(2,e) * M_00 * eig(i)          * u_f(i,j,1,e)           &
          +           d(3,e) * M_00 *          eig(j) * u_f(i,j,1,e)           &
          +           d(1,e) * L_0p                   * u_f(i,j,2,e)

      r_f(i,j,2,e) =                                    r_f(i,j,2,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,2,e)           &
          +           d(1,e) * L_00                   * u_f(i,j,2,e)           &
          +           d(2,e) * M_00 * eig(i)          * u_f(i,j,2,e)           &
          +           d(3,e) * M_00 *          eig(j) * u_f(i,j,2,e)           &
          +           d(1,e) * L_0p                   * u_f(i,j,1,e)
    end do
    end do
    !$acc end loop
    
    !...........................................................................
    ! x_2 faces
    !$acc loop collapse(2)
    do j = 1, n
    do i = 1, n
      r_f(i,j,3,e) =                                    r_f(i,j,3,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,3,e)           &
          +           d(2,e) * L_00                   * u_f(i,j,3,e)           &
          +           d(1,e) * M_00 * eig(i)          * u_f(i,j,3,e)           &
          +           d(3,e) * M_00 *          eig(j) * u_f(i,j,3,e)           &
          +           d(2,e) * L_0p                   * u_f(i,j,4,e)

      r_f(i,j,4,e) =                                    r_f(i,j,4,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,4,e)           &
          +           d(2,e) * L_00                   * u_f(i,j,4,e)           &
          +           d(1,e) * M_00 * eig(i)          * u_f(i,j,4,e)           &
          +           d(3,e) * M_00 *          eig(j) * u_f(i,j,4,e)           &
          +           d(2,e) * L_0p                   * u_f(i,j,3,e)
    end do
    end do
    !$acc end loop
    
    !...........................................................................
    ! x_3 faces
    !$acc loop collapse(2)
    do j = 1, n
    do i = 1, n
      r_f(i,j,5,e) =                                    r_f(i,j,5,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,5,e)           &
          +           d(3,e) * L_00                   * u_f(i,j,5,e)           &
          +           d(1,e) * M_00 * eig(i)          * u_f(i,j,5,e)           &
          +           d(2,e) * M_00 *          eig(j) * u_f(i,j,5,e)           &
          +           d(3,e) * L_0p                   * u_f(i,j,6,e)

      r_f(i,j,6,e) =                                    r_f(i,j,6,e)           &
          +           d(0,e) * M_00                   * u_f(i,j,6,e)           &
          +           d(3,e) * L_00                   * u_f(i,j,6,e)           &
          +           d(1,e) * M_00 * eig(i)          * u_f(i,j,6,e)           &
          +           d(2,e) * M_00 *          eig(j) * u_f(i,j,6,e)           &
          +           d(3,e) * L_0p                   * u_f(i,j,5,e)
    end do
    end do
    !$acc end loop
    
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine FaceToFace

!-------------------------------------------------------------------------------
!> \brief   Face to edge term
!> \author  Immo Huismann

subroutine FaceToEdge(n,n_element,M_00,L_0I_S,L_pI_S,d,u_f,r_e)
  integer,   intent(in) :: n                !< points per edge
  integer,   intent(in) :: n_element        !< number of elements
  real(RDP), intent(in) :: M_00             !< M_{00}
  real(RDP), intent(in) :: L_0I_S(n)        !< S^T L_{I0}
  real(RDP), intent(in) :: L_pI_S(n)        !< S^T L_{Ip}
  real(RDP), intent(in) :: d(0:3,n_element) !< metric coefficients

  real(RDP), intent(in)    :: u_f(n,n,N_FACES,n_element) !< values on faces
  real(RDP), intent(inout) :: r_e(  n,N_EDGES,n_element) !< result on edges

  real(RDP) :: M_00_L_0I_S(n) !< M_00 L_{I0} S^T
  real(RDP) :: M_00_L_pI_S(n) !< M_00 L_{Ip} S^T

  integer :: i,j,e

  M_00_L_0I_S = M_00 * L_0I_S
  M_00_L_pI_S = M_00 * L_pI_S

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_f,r_e)
  !$acc loop collapse(2)
  !$omp do private(i,j,e)
  do e = 1, n_element
  do i = 1, n
    do j = 1, n
  
      r_e(i, 1,e) =                             r_e(i  , 1,e)                    &
          +           d(2,e) * M_00_L_0I_S(j) * u_f(i,j, 5,e)                    &
          +           d(3,e) * M_00_L_0I_S(j) * u_f(i,j, 3,e)
  
      r_e(i, 2,e) =                             r_e(i  , 2,e)                    &
          +           d(2,e) * M_00_L_pI_S(j) * u_f(i,j, 5,e)                    &
          +           d(3,e) * M_00_L_0I_S(j) * u_f(i,j, 4,e)
  
      r_e(i, 3,e) =                             r_e(i  , 3,e)                    &
          +           d(2,e) * M_00_L_0I_S(j) * u_f(i,j, 6,e)                    &
          +           d(3,e) * M_00_L_pI_S(j) * u_f(i,j, 3,e)
  
      r_e(i, 4,e) =                             r_e(i  , 4,e)                    &
          +           d(2,e) * M_00_L_pI_S(j) * u_f(i,j, 6,e)                    &
          +           d(3,e) * M_00_L_pI_S(j) * u_f(i,j, 4,e)
  
      r_e(i, 5,e) =                             r_e(i  , 5,e)                    &
          +           d(3,e) * M_00_L_0I_S(j) * u_f(i,j, 1,e)                    &
          +           d(1,e) * M_00_L_0I_S(j) * u_f(j,i, 5,e)
  
      r_e(i, 6,e) =                             r_e(i  , 6,e)                    &
          +           d(3,e) * M_00_L_0I_S(j) * u_f(i,j, 2,e)                    &
          +           d(1,e) * M_00_L_pI_S(j) * u_f(j,i, 5,e)
  
      r_e(i, 7,e) =                             r_e(i  , 7,e)                    &
          +           d(3,e) * M_00_L_pI_S(j) * u_f(i,j, 1,e)                    &
          +           d(1,e) * M_00_L_0I_S(j) * u_f(j,i, 6,e)
  
      r_e(i, 8,e) =                             r_e(i  , 8,e)                    &
          +           d(3,e) * M_00_L_pI_S(j) * u_f(i,j, 2,e)                    &
          +           d(1,e) * M_00_L_pI_S(j) * u_f(j,i, 6,e)
  
      r_e(i, 9,e) =                             r_e(i  , 9,e)                    &
          +           d(2,e) * M_00_L_0I_S(j) * u_f(j,i, 1,e)                    &
          +           d(1,e) * M_00_L_0I_S(j) * u_f(j,i, 3,e)
  
      r_e(i,10,e) =                             r_e(i  ,10,e)                    &
          +           d(2,e) * M_00_L_0I_S(j) * u_f(j,i, 2,e)                    &
          +           d(1,e) * M_00_L_pI_S(j) * u_f(j,i, 3,e)
  
      r_e(i,11,e) =                             r_e(i  ,11,e)                    &
          +           d(2,e) * M_00_L_pI_S(j) * u_f(j,i, 1,e)                    &
          +           d(1,e) * M_00_L_0I_S(j) * u_f(j,i, 4,e)
  
      r_e(i,12,e) =                             r_e(i  ,12,e)                    &
          +           d(2,e) * M_00_L_pI_S(j) * u_f(j,i, 2,e)                    &
          +           d(1,e) * M_00_L_pI_S(j) * u_f(j,i, 4,e)
  
    end do
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine FaceToEdge

!-------------------------------------------------------------------------------
!> \brief   Primary part for edge -> face
!> \author  Immo Huismann

subroutine EdgeToFace(n,n_element,M_00,S_T_LI0,S_T_LIp,d,u_e,r_f)
  integer,   intent(in) :: n                !< points per edge
  integer,   intent(in) :: n_element        !< number of elements
  real(RDP), intent(in) :: M_00             !< M_{00}
  real(RDP), intent(in) :: S_T_LI0(n)       !< S^T L_{I0}
  real(RDP), intent(in) :: S_T_LIp(n)       !< S^T L_{Ip}
  real(RDP), intent(in) :: d(0:3,n_element) !< metric coefficients

  real(RDP), intent(in)    :: u_e(  n,N_EDGES,n_element) !< values on edges
  real(RDP), intent(inout) :: r_f(n,n,N_FACES,n_element) !< result on faces

  real(RDP) :: M_00_S_T_LI0(n) !< M_00 S^T L_{I0}
  real(RDP) :: M_00_S_T_LIp(n) !< M_00 S^T L_{Ip}

  integer :: i,j,e

  M_00_S_T_LI0 = M_00 * S_T_LI0
  M_00_S_T_LIp = M_00 * S_T_LIp

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_f)
  !$acc loop collapse(3)
  !$omp do private(i,j,e)
  do e = 1, n_element
  do j = 1, n
  do i = 1, n

    r_f(i,j,1,e) =                               r_f(i,j, 1,e)                 &
        +           d(3,e) * M_00_S_T_LI0(  j) * u_e(i,   5,e)                 &
        +           d(3,e) * M_00_S_T_LIp(  j) * u_e(i,   7,e)                 &
        +           d(2,e) * M_00_S_T_LI0(i  ) * u_e(  j, 9,e)                 &
        +           d(2,e) * M_00_S_T_LIp(i  ) * u_e(  j,11,e)

    r_f(i,j,2,e) =                               r_f(i,j, 2,e)                 &
        +           d(3,e) * M_00_S_T_LI0(  j) * u_e(i,   6,e)                 &
        +           d(3,e) * M_00_S_T_LIp(  j) * u_e(i,   8,e)                 &
        +           d(2,e) * M_00_S_T_LI0(i  ) * u_e(  j,10,e)                 &
        +           d(2,e) * M_00_S_T_LIp(i  ) * u_e(  j,12,e)

    r_f(i,j,3,e) =                               r_f(i,j, 3,e)                 &
        +           d(3,e) * M_00_S_T_LI0(  j) * u_e(i,   1,e)                 &
        +           d(3,e) * M_00_S_T_LIp(  j) * u_e(i,   3,e)                 &
        +           d(1,e) * M_00_S_T_LI0(i  ) * u_e(  j, 9,e)                 &
        +           d(1,e) * M_00_S_T_LIp(i  ) * u_e(  j,10,e)

    r_f(i,j,4,e) =                               r_f(i,j, 4,e)                 &
        +           d(3,e) * M_00_S_T_LI0(  j) * u_e(i,   2,e)                 &
        +           d(3,e) * M_00_S_T_LIp(  j) * u_e(i,   4,e)                 &
        +           d(1,e) * M_00_S_T_LI0(i  ) * u_e(  j,11,e)                 &
        +           d(1,e) * M_00_S_T_LIp(i  ) * u_e(  j,12,e)

    r_f(i,j,5,e) =                               r_f(i,j, 5,e)                 &
        +           d(2,e) * M_00_S_T_LI0(  j) * u_e(i,   1,e)                 &
        +           d(2,e) * M_00_S_T_LIp(  j) * u_e(i,   2,e)                 &
        +           d(1,e) * M_00_S_T_LI0(i  ) * u_e(  j, 5,e)                 &
        +           d(1,e) * M_00_S_T_LIp(i  ) * u_e(  j, 6,e)

    r_f(i,j,6,e) =                               r_f(i,j, 6,e)                 &
        +           d(2,e) * M_00_S_T_LI0(  j) * u_e(i,   3,e)                 &
        +           d(2,e) * M_00_S_T_LIp(  j) * u_e(i,   4,e)                 &
        +           d(1,e) * M_00_S_T_LI0(i  ) * u_e(  j, 7,e)                 &
        +           d(1,e) * M_00_S_T_LIp(i  ) * u_e(  j, 8,e)

  end do
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine EdgeToFace

!-------------------------------------------------------------------------------
!> \brief   Edge to edge interaction
!> \author  Immo Huismann

subroutine EdgeToEdge(n,n_element,M_00,L_00,L_0p,eig,d,u_e,r_e)
  integer,   intent(in) :: n              !< points per edge
  integer,   intent(in) :: n_element      !< number of elements
  real(RDP), intent(in) :: d(0:3,n_element) !< metric coefficient * lambda
  real(RDP), intent(in) :: M_00           !< M_{00}
  real(RDP), intent(in) :: L_00           !< L_{00}
  real(RDP), intent(in) :: L_0p           !< L_{0p}
  real(RDP), intent(in) :: eig(n)         !< eigenvalues of L_II

  real(RDP), intent(in)    :: u_e(n,N_EDGES,n_element) !< values on edges
  real(RDP), intent(inout) :: r_e(n,N_EDGES,n_element) !< result on edges

  integer :: i, e

  real(RDP) :: f_0, f_1, f_2

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_e)
  !$acc loop gang worker vector
  !$omp do private(i,e,f_0,f_1,f_2)
  do e = 1, n_element
    !...........................................................................
    ! x_1 edges
    !$acc loop vector
    do i = 1, n

      f_0 =  d(0,e) * M_00 * M_00           & ! lambda * M * M * M
          +  d(1,e) * M_00 * M_00  * eig(i) & !          M * M * L
          +  d(2,e) * L_00 * M_00           & !          M * L * M
          +  d(3,e) * M_00 * L_00             !          L * M * M

      f_1 =  d(2,e) * M_00 * L_0p             !          M * L * M
      f_2 =  d(3,e) * M_00 * L_0p             !          L * M * M

      r_e(i, 1,e) =      r_e(i, 1,e)                                              &
          + f_0 *        u_e(i, 1,e)                                              &
          + f_1 *        u_e(i, 2,e)                                              &
          + f_2 *        u_e(i, 3,e)

      r_e(i, 2,e) =      r_e(i, 2,e)                                              &
          + f_0 *        u_e(i, 2,e)                                              &
          + f_1 *        u_e(i, 1,e)                                              &
          + f_2 *        u_e(i, 4,e)

      r_e(i, 3,e) =      r_e(i, 3,e)                                              &
          + f_0 *        u_e(i, 3,e)                                              &
          + f_1 *        u_e(i, 4,e)                                              &
          + f_2 *        u_e(i, 1,e)

      r_e(i, 4,e) =      r_e(i, 4,e)                                              &
          +        f_0 * u_e(i, 4,e)                                              &
          +        f_1 * u_e(i, 3,e)                                              &
          +        f_2 * u_e(i, 2,e)
    end do
    !$acc end loop
    
    !.............................................................................
    ! x_2 edges
    !$acc loop vector
    do i = 1, n
      f_0 =  d(0,e) * M_00 * M_00           & ! lambda * M * M * M
          +  d(2,e) * M_00 * M_00  * eig(i) & !          M * M * L
          +  d(1,e) * L_00 * M_00           & !          M * L * M
          +  d(3,e) * M_00 * L_00             !          L * M * M

      f_1 =  d(1,e) * M_00 * L_0p             !          M * L * M
      f_2 =  d(3,e) * M_00 * L_0p             !          L * M * M

      r_e(i, 5,e) =      r_e(i, 5,e)                                              &
          + f_0 *        u_e(i, 5,e)                                              &
          + f_1 *        u_e(i, 6,e)                                              &
          + f_2 *        u_e(i, 7,e)

      r_e(i, 6,e) =      r_e(i, 6,e)                                              &
          + f_0 *        u_e(i, 6,e)                                              &
          + f_1 *        u_e(i, 5,e)                                              &
          + f_2 *        u_e(i, 8,e)

      r_e(i, 7,e) =      r_e(i, 7,e)                                              &
          + f_0 *        u_e(i, 7,e)                                              &
          + f_1 *        u_e(i, 8,e)                                              &
          + f_2 *        u_e(i, 5,e)

      r_e(i, 8,e) =      r_e(i, 8,e)                                              &
          +        f_0 * u_e(i, 8,e)                                              &
          +        f_1 * u_e(i, 7,e)                                              &
          +        f_2 * u_e(i, 6,e)
    end do
    !$acc end loop
    
    !.............................................................................
    ! x_3 edges
    !$acc loop vector
    do i = 1, n
      f_0 =  d(0,e) * M_00 * M_00           & ! lambda * M * M * M
          +  d(3,e) * M_00 * M_00  * eig(i) & !          M * M * L
          +  d(1,e) * L_00 * M_00           & !          M * L * M
          +  d(2,e) * M_00 * L_00             !          L * M * M

      f_1 =  d(1,e) * M_00 * L_0p             !          M * L * M
      f_2 =  d(2,e) * M_00 * L_0p             !          L * M * M

      r_e(i, 9,e) =      r_e(i, 9,e)                                              &
          + f_0 *        u_e(i, 9,e)                                              &
          + f_1 *        u_e(i,10,e)                                              &
          + f_2 *        u_e(i,11,e)

      r_e(i,10,e) =      r_e(i,10,e)                                              &
          + f_0 *        u_e(i,10,e)                                              &
          + f_1 *        u_e(i, 9,e)                                              &
          + f_2 *        u_e(i,12,e)

      r_e(i,11,e) =      r_e(i,11,e)                                              &
          + f_0 *        u_e(i,11,e)                                              &
          + f_1 *        u_e(i,12,e)                                              &
          + f_2 *        u_e(i, 9,e)

      r_e(i,12,e) =      r_e(i,12,e)                                              &
          +        f_0 * u_e(i,12,e)                                              &
          +        f_1 * u_e(i,11,e)                                              &
          +        f_2 * u_e(i,10,e)

    end do
    !$acc end loop

  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine EdgeToEdge

!-------------------------------------------------------------------------------
!> \brief   Edge to vertex interaction
!> \author  Immo Huismann

subroutine EdgeToVertex(n,n_element,M_00,L_0I_S,L_pI_S,d,u_e,r_v)
  integer,   intent(in)    :: n
  integer,   intent(in)    :: n_element
  real(RDP), intent(in)    :: M_00
  real(RDP), intent(in)    :: L_0I_S(n)
  real(RDP), intent(in)    :: L_pI_S(n)
  real(RDP), intent(in)    :: d(0:3,           n_element)
  real(RDP), intent(in)    :: u_e(n,N_EDGES,   n_element)
  real(RDP), intent(inout) :: r_v(  N_VERTICES,n_element)

  integer   :: i, e
  real(RDP) :: tmp_1,tmp_2, factor

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_v)
  !$acc loop gang worker vector
  !$omp do private(i,e,tmp_1,tmp_2,factor)
  do e = 1, n_element

    !---------------------------------------------------------------------------
    ! x_1 edges

    factor = M_00 * M_00 * d(1,e)

    !...........................................................................
    ! first one
    tmp_1  = 0
    tmp_2  = 0

    !$acc loop vector reduction(+:tmp_1,tmp_2)
    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 1,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 1,e)
    end do
    !$acc end loop
    
    r_v(1,e) = r_v(1,e) + tmp_1 * factor
    r_v(2,e) = r_v(2,e) + tmp_2 * factor

    !...........................................................................
    ! second one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 2,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 2,e)
    end do

    r_v(3,e) = r_v(3,e) + tmp_1 * factor
    r_v(4,e) = r_v(4,e) + tmp_2 * factor

    !...........................................................................
    ! third one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 3,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 3,e)
    end do

    r_v(5,e) = r_v(5,e) + tmp_1 * factor
    r_v(6,e) = r_v(6,e) + tmp_2 * factor

    !...........................................................................
    ! Fourth one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 4,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 4,e)
    end do

    r_v(7,e) = r_v(7,e) + tmp_1 * factor
    r_v(8,e) = r_v(8,e) + tmp_2 * factor


    !---------------------------------------------------------------------------
    ! x_2 edges

    factor = M_00 * M_00 * d(2,e)

    !...........................................................................
    ! first one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 5,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 5,e)
    end do

    r_v(1,e) = r_v(1,e) + tmp_1 * factor
    r_v(3,e) = r_v(3,e) + tmp_2 * factor

    !...........................................................................
    ! second one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 6,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 6,e)
    end do

    r_v(2,e) = r_v(2,e) + tmp_1 * factor
    r_v(4,e) = r_v(4,e) + tmp_2 * factor

    !...........................................................................
    ! third one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 7,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 7,e)
    end do

    r_v(5,e) = r_v(5,e) + tmp_1 * factor
    r_v(7,e) = r_v(7,e) + tmp_2 * factor

    !...........................................................................
    ! Fourth one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 8,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 8,e)
    end do

    r_v(6,e) = r_v(6,e) + tmp_1 * factor
    r_v(8,e) = r_v(8,e) + tmp_2 * factor

    !---------------------------------------------------------------------------
    ! x_3 edges

    factor = M_00 * M_00 * d(3,e)

    !...........................................................................
    ! first one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i, 9,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i, 9,e)
    end do

    r_v(1,e) = r_v(1,e) + tmp_1 * factor
    r_v(5,e) = r_v(5,e) + tmp_2 * factor

    !...........................................................................
    ! second one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i,10,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i,10,e)
    end do

    r_v(2,e) = r_v(2,e) + tmp_1 * factor
    r_v(6,e) = r_v(6,e) + tmp_2 * factor

    !...........................................................................
    ! third one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i,11,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i,11,e)
    end do

    r_v(3,e) = r_v(3,e) + tmp_1 * factor
    r_v(7,e) = r_v(7,e) + tmp_2 * factor

    !...........................................................................
    ! Fourth one
    tmp_1  = 0
    tmp_2  = 0

    do i = 1, n
      tmp_1 = tmp_1 + L_0I_S(i) * u_e(i,12,e)
      tmp_2 = tmp_2 + L_pI_S(i) * u_e(i,12,e)
    end do

    r_v(4,e) = r_v(4,e) + tmp_1 * factor
    r_v(8,e) = r_v(8,e) + tmp_2 * factor

  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine EdgeToVertex

!-------------------------------------------------------------------------------
!> \brief   Vertex to edge interaction
!> \author  Immo Huismann

subroutine VertexToEdge(n,n_element,M_00,S_T_L_I0,S_T_L_Ip,d,u_v,r_e)
  integer,   intent(in)    :: n
  integer,   intent(in)    :: n_element
  real(RDP), intent(in)    :: M_00
  real(RDP), intent(in)    :: S_T_L_I0(n)
  real(RDP), intent(in)    :: S_T_L_Ip(n)
  real(RDP), intent(in)    :: d(0:3,           n_element)
  real(RDP), intent(in)    :: u_v(  N_VERTICES,n_element)
  real(RDP), intent(inout) :: r_e(n,N_EDGES,   n_element)

  integer   :: i, e
  real(RDP) :: tmp, factor

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_v,r_e)
  !$acc loop
  !$omp do private(i,e,tmp,factor)
  do e = 1, n_element
    !...........................................................................
    ! x_1 edges
    factor = M_00 * M_00 * d(1,e)

    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(1,e) + S_T_L_Ip(i) * u_v(2,e)
      r_e(i, 1,e) = r_e(i, 1,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(3,e) + S_T_L_Ip(i) * u_v(4,e)
      r_e(i, 2,e) = r_e(i, 2,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(5,e) + S_T_L_Ip(i) * u_v(6,e)
      r_e(i, 3,e) = r_e(i, 3,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(7,e) + S_T_L_Ip(i) * u_v(8,e)
      r_e(i, 4,e) = r_e(i, 4,e) + factor * tmp
    end do

    !...........................................................................
    ! x_2 edges
    factor = M_00 * M_00 * d(2,e)

    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(1,e) + S_T_L_Ip(i) * u_v(3,e)
      r_e(i, 5,e) = r_e(i, 5,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(2,e) + S_T_L_Ip(i) * u_v(4,e)
      r_e(i, 6,e) = r_e(i, 6,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(5,e) + S_T_L_Ip(i) * u_v(7,e)
      r_e(i, 7,e) = r_e(i, 7,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(6,e) + S_T_L_Ip(i) * u_v(8,e)
      r_e(i, 8,e) = r_e(i, 8,e) + factor * tmp
    end do

    !...........................................................................
    ! x_3 edges
    factor = M_00 * M_00 * d(3,e)

    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(1,e) + S_T_L_Ip(i) * u_v(5,e)
      r_e(i, 9,e) = r_e(i, 9,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(2,e) + S_T_L_Ip(i) * u_v(6,e)
      r_e(i,10,e) = r_e(i,10,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(3,e) + S_T_L_Ip(i) * u_v(7,e)
      r_e(i,11,e) = r_e(i,11,e) + factor * tmp
    end do
    do i = 1, n
      tmp         = S_T_L_I0(i) * u_v(4,e) + S_T_L_Ip(i) * u_v(8,e)
      r_e(i,12,e) = r_e(i,12,e) + factor * tmp
    end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine VertexToEdge

!-------------------------------------------------------------------------------
!> \brief   Primary part vertex to vertex in transformed condensed system
!> \author  Immo Huismann

subroutine VertexToVertex(n_element,M_00,L_00,L_0p,d,u_v,r_v)
  integer,   intent(in)    :: n_element
  real(RDP), intent(in)    :: M_00
  real(RDP), intent(in)    :: L_00
  real(RDP), intent(in)    :: L_0p
  real(RDP), intent(in)    :: d  (0:3,n_element)
  real(RDP), intent(in)    :: u_v(N_VERTICES,n_element) !< values on vertices
  real(RDP), intent(inout) :: r_v(N_VERTICES,n_element) !< result on vertices

  integer   :: e
  real(RDP) :: M_00_M_00_M_00
  real(RDP) :: L_00_M_00_M_00
  real(RDP) :: L_0p_M_00_M_00

  M_00_M_00_M_00 = M_00 * M_00 * M_00
  L_00_M_00_M_00 = L_00 * M_00 * M_00
  L_0p_M_00_M_00 = L_0p * M_00 * M_00

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_v,r_v)
  !$acc loop
  !$omp do private(e)
  do e = 1, n_element

    r_v(1,e) = r_v(1,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(1,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(1,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(2,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(3,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(5,e)

    r_v(2,e) = r_v(2,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(2,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(2,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(1,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(4,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(6,e)

    r_v(3,e) = r_v(3,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(3,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(3,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(4,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(1,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(7,e)

    r_v(4,e) = r_v(4,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(4,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(4,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(3,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(2,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(8,e)

    r_v(5,e) = r_v(5,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(5,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(5,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(6,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(7,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(1,e)

    r_v(6,e) = r_v(6,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(6,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(6,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(5,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(8,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(2,e)

    r_v(7,e) = r_v(7,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(7,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(7,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(8,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(5,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(3,e)

    r_v(8,e) = r_v(8,e)                                                        &
        + M_00_M_00_M_00 *  d(0,e)                    * u_v(8,e)               &
        + L_00_M_00_M_00 * (d(1,e) + d(2,e) + d(3,e)) * u_v(8,e)               &
        + L_0p_M_00_M_00 *  d(1,e)                    * u_v(7,e)               &
        + L_0p_M_00_M_00 *  d(2,e)                    * u_v(6,e)               &
        + L_0p_M_00_M_00 *  d(3,e)                    * u_v(4,e)

  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine VertexToVertex

!===============================================================================

end module Transformed_Primary_Part
