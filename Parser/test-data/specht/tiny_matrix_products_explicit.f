!> \file       tiny_matrix_products_explicit.f
!> \brief      matrix products, for known matrix sizes
!> \author     Immo Huismann
!> \date       2015/04/20
!> \copyright  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!===============================================================================

module Tiny_Matrix_Products_Explicit
  use Kind_Parameters, only: RDP
  use ACC_Parameters,  only: ACC_EXEC_QUEUE
  implicit none
  private

  public ::    SquareTransposeMatrixProduct_Generated
  public :: AddSquareTransposeMatrixProduct_Generated

contains

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C, generated version choosing different implementations
!> \author  Immo Huismann

subroutine AddSquareTransposeMatrixProduct_Generated(k,m,A,B,C)
  integer,   intent(in)    :: k!< Matrix dim.
  integer,   intent(in)    :: m!< number of vectors
  real(RDP), intent(in)    :: A(k,k)!< matrix to multiply with
  real(RDP), intent(in)    :: B(k,m)!< matrix to multiply
  real(RDP), intent(inout) :: C(k,m)!< result

  select case(k)
  case( 1)
    call AddSquareTransposeMatrixProduct_01(m,A,B,C)
  case( 2)
    call AddSquareTransposeMatrixProduct_02(m,A,B,C)
  case( 3)
    call AddSquareTransposeMatrixProduct_03(m,A,B,C)
  case( 4)
    call AddSquareTransposeMatrixProduct_04(m,A,B,C)
  case( 5)
    call AddSquareTransposeMatrixProduct_05(m,A,B,C)
  case( 6)
    call AddSquareTransposeMatrixProduct_06(m,A,B,C)
  case( 7)
    call AddSquareTransposeMatrixProduct_07(m,A,B,C)
  case( 8)
    call AddSquareTransposeMatrixProduct_08(m,A,B,C)
  case( 9)
    call AddSquareTransposeMatrixProduct_09(m,A,B,C)
  case(10)
    call AddSquareTransposeMatrixProduct_10(m,A,B,C)
  case(11)
    call AddSquareTransposeMatrixProduct_11(m,A,B,C)
  case(12)
    call AddSquareTransposeMatrixProduct_12(m,A,B,C)
  case(13)
    call AddSquareTransposeMatrixProduct_13(m,A,B,C)
  case(14)
    call AddSquareTransposeMatrixProduct_14(m,A,B,C)
  case(15)
    call AddSquareTransposeMatrixProduct_15(m,A,B,C)
  case(16)
    call AddSquareTransposeMatrixProduct_16(m,A,B,C)
  case default
    call AddSquareTransposeMatrixProduct_XX(k,m,A,B,C)
  end select

end subroutine AddSquareTransposeMatrixProduct_Generated
!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size k x k
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_XX(k,m,A_T,B,C)
  integer,   intent(in)    :: k
  integer,   intent(in)    :: m
  real(RDP), intent(in)    :: A_T(k,k)
  real(RDP), intent(in)    :: B  (k,m)
  real(RDP), intent(inout) :: C  (k,m)

  integer   :: i, j, l ! loop indices
  real(RDP) :: tmp     ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,l,tmp) gang worker vector
  !$omp do private(i,j,l,tmp)
  do j = 1, m
  do i = 1, k
    tmp = 0
    do l = 1, k
      tmp = tmp + A_T(l,i) * B(l,j)
    end do

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_XX


!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 1 x 1
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_01(m,A_T,B,C)
  integer,   parameter   :: N = 1
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_01

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 2 x 2
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_02(m,A_T,B,C)
  integer,   parameter   :: N = 2
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_02

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 3 x 3
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_03(m,A_T,B,C)
  integer,   parameter   :: N = 3
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_03

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 4 x 4
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_04(m,A_T,B,C)
  integer,   parameter   :: N = 4
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_04

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 5 x 5
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_05(m,A_T,B,C)
  integer,   parameter   :: N = 5
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_05

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 6 x 6
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_06(m,A_T,B,C)
  integer,   parameter   :: N = 6
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_06

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 7 x 7
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_07(m,A_T,B,C)
  integer,   parameter   :: N = 7
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_07

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 8 x 8
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_08(m,A_T,B,C)
  integer,   parameter   :: N = 8
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_08

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 9 x 9
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_09(m,A_T,B,C)
  integer,   parameter   :: N = 9
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_09

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 10 x 10
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_10(m,A_T,B,C)
  integer,   parameter   :: N = 10
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_10

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 11 x 11
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_11(m,A_T,B,C)
  integer,   parameter   :: N = 11
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_11

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 12 x 12
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_12(m,A_T,B,C)
  integer,   parameter   :: N = 12
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_12

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 13 x 13
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_13(m,A_T,B,C)
  integer,   parameter   :: N = 13
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_13

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 14 x 14
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_14(m,A_T,B,C)
  integer,   parameter   :: N = 14
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_14

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 15 x 15
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_15(m,A_T,B,C)
  integer,   parameter   :: N = 15
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j) &
           + A_T(15,i) * B(15,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_15

!-------------------------------------------------------------------------------
!> \brief   C <- A B + C for small square matrix A of size 16 x 16
!> \author  Immo Huismann
subroutine AddSquareTransposeMatrixProduct_16(m,A_T,B,C)
  integer,   parameter   :: N = 16
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j) &
           + A_T(15,i) * B(15,j) &
           + A_T(16,i) * B(16,j)

    C(i,j) = C(i,j) + tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine AddSquareTransposeMatrixProduct_16

!-------------------------------------------------------------------------------
!> \brief   C <- A B, generated version choosing different implementations
!> \author  Immo Huismann

subroutine SquareTransposeMatrixProduct_Generated(k,m,A,B,C)
  integer,   intent(in)    :: k!< Matrix dim.
  integer,   intent(in)    :: m!< number of vectors
  real(RDP), intent(in)    :: A(k,k)!< matrix to multiply with
  real(RDP), intent(in)    :: B(k,m)!< matrix to multiply
  real(RDP), intent(out)   :: C(k,m)!< result

  select case(k)
  case( 1)
    call SquareTransposeMatrixProduct_01(m,A,B,C)
  case( 2)
    call SquareTransposeMatrixProduct_02(m,A,B,C)
  case( 3)
    call SquareTransposeMatrixProduct_03(m,A,B,C)
  case( 4)
    call SquareTransposeMatrixProduct_04(m,A,B,C)
  case( 5)
    call SquareTransposeMatrixProduct_05(m,A,B,C)
  case( 6)
    call SquareTransposeMatrixProduct_06(m,A,B,C)
  case( 7)
    call SquareTransposeMatrixProduct_07(m,A,B,C)
  case( 8)
    call SquareTransposeMatrixProduct_08(m,A,B,C)
  case( 9)
    call SquareTransposeMatrixProduct_09(m,A,B,C)
  case(10)
    call SquareTransposeMatrixProduct_10(m,A,B,C)
  case(11)
    call SquareTransposeMatrixProduct_11(m,A,B,C)
  case(12)
    call SquareTransposeMatrixProduct_12(m,A,B,C)
  case(13)
    call SquareTransposeMatrixProduct_13(m,A,B,C)
  case(14)
    call SquareTransposeMatrixProduct_14(m,A,B,C)
  case(15)
    call SquareTransposeMatrixProduct_15(m,A,B,C)
  case(16)
    call SquareTransposeMatrixProduct_16(m,A,B,C)
  case default
    call SquareTransposeMatrixProduct_XX(k,m,A,B,C)
  end select

end subroutine SquareTransposeMatrixProduct_Generated
!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size k x k
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_XX(k,m,A_T,B,C)
  integer,   intent(in)    :: k
  integer,   intent(in)    :: m
  real(RDP), intent(in)    :: A_T(k,k)
  real(RDP), intent(in)    :: B  (k,m)
  real(RDP), intent(out)   :: C  (k,m)

  integer   :: i, j, l ! loop indices
  real(RDP) :: tmp     ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,l,tmp) gang worker vector
  !$omp do private(i,j,l,tmp)
  do j = 1, m
  do i = 1, k
    tmp = 0
    do l = 1, k
      tmp = tmp + A_T(l,i) * B(l,j)
    end do

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_XX


!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 1 x 1
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_01(m,A_T,B,C)
  integer,   parameter   :: N = 1
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_01

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 2 x 2
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_02(m,A_T,B,C)
  integer,   parameter   :: N = 2
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_02

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 3 x 3
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_03(m,A_T,B,C)
  integer,   parameter   :: N = 3
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_03

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 4 x 4
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_04(m,A_T,B,C)
  integer,   parameter   :: N = 4
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_04

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 5 x 5
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_05(m,A_T,B,C)
  integer,   parameter   :: N = 5
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_05

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 6 x 6
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_06(m,A_T,B,C)
  integer,   parameter   :: N = 6
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_06

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 7 x 7
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_07(m,A_T,B,C)
  integer,   parameter   :: N = 7
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_07

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 8 x 8
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_08(m,A_T,B,C)
  integer,   parameter   :: N = 8
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_08

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 9 x 9
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_09(m,A_T,B,C)
  integer,   parameter   :: N = 9
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_09

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 10 x 10
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_10(m,A_T,B,C)
  integer,   parameter   :: N = 10
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_10

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 11 x 11
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_11(m,A_T,B,C)
  integer,   parameter   :: N = 11
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_11

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 12 x 12
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_12(m,A_T,B,C)
  integer,   parameter   :: N = 12
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_12

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 13 x 13
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_13(m,A_T,B,C)
  integer,   parameter   :: N = 13
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_13

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 14 x 14
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_14(m,A_T,B,C)
  integer,   parameter   :: N = 14
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_14

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 15 x 15
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_15(m,A_T,B,C)
  integer,   parameter   :: N = 15
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j) &
           + A_T(15,i) * B(15,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_15

!-------------------------------------------------------------------------------
!> \brief   C <- A B for small square matrix A of size 16 x 16
!> \author  Immo Huismann
subroutine SquareTransposeMatrixProduct_16(m,A_T,B,C)
  integer,   parameter   :: N = 16
  integer,   intent(in)  :: m
  real(RDP), intent(in)  :: A_T(N,N)
  real(RDP), intent(in)  :: B  (N,m)
  real(RDP), intent(out) :: C  (N,m)

  integer   :: i, j ! loop indices
  real(RDP) :: tmp  ! reduction variable

  !$acc parallel present(B,C) copyin(A_T) async(ACC_EXEC_QUEUE)
  !$acc loop collapse(2) private(i,j,tmp)
  !$omp do private(i,j,tmp)
  do j = 1, m
  do i = 1, N
    tmp    =                     &
           + A_T( 1,i) * B( 1,j) &
           + A_T( 2,i) * B( 2,j) &
           + A_T( 3,i) * B( 3,j) &
           + A_T( 4,i) * B( 4,j) &
           + A_T( 5,i) * B( 5,j) &
           + A_T( 6,i) * B( 6,j) &
           + A_T( 7,i) * B( 7,j) &
           + A_T( 8,i) * B( 8,j) &
           + A_T( 9,i) * B( 9,j) &
           + A_T(10,i) * B(10,j) &
           + A_T(11,i) * B(11,j) &
           + A_T(12,i) * B(12,j) &
           + A_T(13,i) * B(13,j) &
           + A_T(14,i) * B(14,j) &
           + A_T(15,i) * B(15,j) &
           + A_T(16,i) * B(16,j)

    C(i,j) = tmp
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine SquareTransposeMatrixProduct_16

!===============================================================================

end module Tiny_Matrix_Products_Explicit
