!> \file       matrix_operations.f
!> \brief      Provides explicit shape routines for simple matrix operations
!> \author     Immo Huismann
!> \date       2014/08/14
!> \copyright  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This module provides routines in explicit size for some simple matrix and
!> array operations.
!>
!> Currently the operations of diagonal matrices on matrices, scalar products
!> and the transposition of triad dimensions are supported.
!>
!> \todo
!> * Create unit tests.
!> * OpenACC and OpenMP in unit tests
!> * Check OpenMP parallelization of the routines
!===============================================================================

module Matrix_Operations
  ! HiBASE modules
  use Kind_Parameters, only: RDP

  ! Specht base modules
  use ACC_Parameters,  only: ACC_EXEC_QUEUE
  implicit none
  private

  public ::        DiagonalMatrixProduct ! application of a diagonal matrix
  public ::     AddDiagonalMatrixProduct ! as above, but adding to result vector
  public :: InPlaceDiagonalMatrixProduct ! as first, but inplace operation

  public ::    StackedTranspose          ! transposition of multiple matrices
  public :: AddStackedTranspose          ! transposition of multiple matrices

  !-----------------------------------------------------------------------------
  !> \brief   A matrix product with a diagonal matrix as operator
  !> \author  Immo Huismann

  interface DiagonalMatrixProduct
    module procedure       DiagonalMatrixProduct1
    module procedure       DiagonalMatrixProduct2
    module procedure       DiagonalMatrixProduct3
    module procedure ScaledDiagonalMatrixProduct2
    module procedure ScaledDiagonalMatrixProduct22
    module procedure ScaledDiagonalMatrixProduct3
  end interface DiagonalMatrixProduct

  !-----------------------------------------------------------------------------
  !> \brief   A matrix product with a diagonal matrix as operator
  !> \author  Immo Huismann

  interface InPlaceDiagonalMatrixProduct
    module procedure       InPlaceDiagonalMatrixProduct3
    module procedure ScaledInPlaceDiagonalMatrixProduct3
  end interface InPlaceDiagonalMatrixProduct

  !-----------------------------------------------------------------------------
  !> \brief   An additive matrix product with a diagonal matrix as operator
  !> \author  Immo Huismann

  interface AddDiagonalMatrixProduct
    module procedure AddDiagonalMatrixProduct1
    module procedure AddDiagonalMatrixProduct2
    module procedure AddDiagonalMatrixProduct3
    module procedure AddScaledDiagonalMatrixProduct2
  end interface AddDiagonalMatrixProduct

contains

! Wrappers =====================================================================

!-------------------------------------------------------------------------------
!> \brief   Applies a diagonal matrix to the values
!> \author  Immo Huismann

subroutine DiagonalMatrixProduct1(diag,values,res)
  real(RDP), intent(in)    :: diag  (:)       !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = product([size(values,2),size(values,3),size(values,4)])

  call DiagonalMatrixProduct_ES(k,m,diag,values,res)

end subroutine DiagonalMatrixProduct1

!-------------------------------------------------------------------------------
!> \brief   Applies a diagonal matrix to the values
!> \author  Immo Huismann

subroutine DiagonalMatrixProduct2(diag,values,res)
  real(RDP), intent(in)    :: diag  (:,:)     !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = product([size(values,3),size(values,4)])

  call DiagonalMatrixProduct_ES(k,m,diag,values,res)

end subroutine DiagonalMatrixProduct2

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine DiagonalMatrixProduct3(diag,values,res)
  real(RDP), intent(in)    :: diag  (:,:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = size(values,4)

  call DiagonalMatrixProduct_ES(k,m,diag,values,res)

end subroutine DiagonalMatrixProduct3

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine InPlaceDiagonalMatrixProduct3(diag,values)
  real(RDP), intent(in)    :: diag  (:,:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(inout) :: values(:,:,:,:) !< Input data

  integer :: k,m

  k = size(diag)
  m = size(values,4)

  call InPlaceDiagonalMatrixProduct_ES(k,m,diag,values)

end subroutine InPlaceDiagonalMatrixProduct3

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine ScaledInPlaceDiagonalMatrixProduct3(diag,scales,values)
  real(RDP), intent(in)    :: diag  (:,:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales(      :) !< elementwise scale
  real(RDP), intent(inout) :: values(:,:,:,:) !< Input data

  integer :: k,m

  k = size(diag)
  m = size(scales)

  call ScaledInPlaceDiagonalMatrixProduct_ES(k,m,diag,scales,values)

end subroutine ScaledInPlaceDiagonalMatrixProduct3

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine ScaledDiagonalMatrixProduct2(diag,scales,values,res)
  real(RDP), intent(in)    :: diag  (:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales    (:) !< Scaling factors
  real(RDP), intent(in)    :: values(:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:) !< Matrix to store result in

  integer :: k,m

  k = size(diag)
  m = size(scales)

  call ScaledDiagonalMatrixProduct_ES(k,m,diag,scales,values,res)

end subroutine ScaledDiagonalMatrixProduct2

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine ScaledDiagonalMatrixProduct22(diag,scales,values,res)
  real(RDP), intent(in)    :: diag  (:,:)     !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales    (:,:) !< Scaling factors
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:,:) !< Matrix to store result in

  integer :: k,m

  k = size(diag)
  m = size(scales)

  call ScaledDiagonalMatrixProduct_ES(k,m,diag,scales,values,res)

end subroutine ScaledDiagonalMatrixProduct22

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine ScaledDiagonalMatrixProduct3(diag,scales,values,res)
  real(RDP), intent(in)    :: diag  (:,:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales      (:) !< Scaling factors
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(  out) :: res   (:,:,:,:) !< Matrix to store result in

  integer :: k,m

  k = size(diag)
  m = size(scales)

  call ScaledDiagonalMatrixProduct_ES(k,m,diag,scales,values,res)

end subroutine ScaledDiagonalMatrixProduct3

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine AddDiagonalMatrixProduct1(diag,values,res)
  real(RDP), intent(in)    :: diag  (:)       !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(inout) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = product([size(values,2),size(values,3),size(values,4)])

  call AddDiagonalMatrixProduct_ES (k,m,diag,values,res)

end subroutine AddDiagonalMatrixProduct1

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine AddDiagonalMatrixProduct2(diag,values,res)
  real(RDP), intent(in)    :: diag  (:,:)     !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(inout) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = product([size(values,3),size(values,4)])

  call AddDiagonalMatrixProduct_ES(k,m,diag,values,res)

end subroutine AddDiagonalMatrixProduct2

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine AddDiagonalMatrixProduct3(diag,values,res)
  real(RDP), intent(in)    :: diag  (:,:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(:,:,:,:) !< Input data
  real(RDP), intent(inout) :: res   (:,:,:,:) !< Matrix to store the result in

  integer :: k,m

  k = size(diag)
  m = size(values,4)

  call AddDiagonalMatrixProduct_ES(k,m,diag,values,res)

end subroutine AddDiagonalMatrixProduct3

!-------------------------------------------------------------------------------
!> \brief   This interface provides DGEMM for a square matrix A
!> \author  Immo Huismann

subroutine AddScaledDiagonalMatrixProduct2(diag,scales,values,res)
  real(RDP), intent(in)    :: diag  (:,:)   !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales    (:) !< Scaling factors
  real(RDP), intent(in)    :: values(:,:,:) !< Input data
  real(RDP), intent(inout) :: res   (:,:,:) !< Matrix to store result in

  integer :: k,m

  k = size(diag)
  m = size(scales)

  call AddScaledDiagonalMatrixProduct_ES(k,m,diag,scales,values,res)

end subroutine AddScaledDiagonalMatrixProduct2

! Implementations ==============================================================

!-------------------------------------------------------------------------------
!> \brief   Provides a diagonal matrix multiplication
!> \author  Immo Huismann
!>
!> \details
!> Explicit size application of a diagonal matrix on a vector.
!> As it is implemented in explicit size with only two array dimensions instead
!> of four or six, it is easily vectorized by the compiler.
!>
!> This routine is not designed to be used directly. Rather one of the wrappers
!> with explicit shape should be used. Mind that this routine is not
!> thread-safe, as it uses OpenACC and OpenMP to parallelize the operations.

subroutine DiagonalMatrixProduct_ES (k,m,diag,values,res)
  integer  , intent(in)    :: k           !< size of first  dim of matrix A
  integer  , intent(in)    :: m           !< size of second dim of B
  real(RDP), intent(in)    :: diag  (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(k,m) !< Input data
  real(RDP), intent(  out) :: res   (k,m) !< Matrix to store the result im

  integer   :: i,j ! loop indices

  ! Mind that the copy of the diag matrix is not allowed in the data statement,
  ! unless the data statement receives the 'async' keyword.
  ! Obmitting the async might lead to the parallel region starting without the
  ! copy process being finished.

  !$acc data present(values,res) copyin(diag) async(ACC_EXEC_QUEUE)

  !$acc parallel async(ACC_EXEC_QUEUE)

  !$acc loop independent collapse(2) private(i,j)
  !$omp do private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    res(i,j) = diag(i) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel

  !$acc end data

end subroutine DiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> \brief   Provides a diagonal matrix multiplication
!> \author  Immo Huismann
!>
!> \details
!> Explicit size application of a diagonal matrix on a vector.
!> As it is implemented in explicit size with only two array dimensions instead
!> of four or six, it is easily vectorized by the compiler.
!>
!> This routine is not designed to be used directly. Rather one of the wrappers
!> with explicit shape should be used. Mind that this routine is not
!> thread-safe, as it uses OpenACC and OpenMP to parallelize the operations.

subroutine InPlaceDiagonalMatrixProduct_ES (k,m,diag,values)
  integer  , intent(in)    :: k           !< size of first  dim of matrix A
  integer  , intent(in)    :: m           !< size of second dim of B
  real(RDP), intent(in)    :: diag  (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(inout) :: values(k,m) !< data to work on

  integer   :: i,j ! loop indices

  ! Mind that the copy of the diag matrix is not allowed in the data statement,
  ! unless the data statement receives the 'async' keyword.
  ! Obmitting the async might lead to the parallel region starting without the
  ! copy process being finished.

  !$acc data present(values) copyin(diag) async(ACC_EXEC_QUEUE)
  !$acc parallel                          async(ACC_EXEC_QUEUE)

  !$acc loop collapse(2) private(i,j)
  !$omp do               private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    values(i,j) = diag(i) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine InPlaceDiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> \brief   Provides a diagonal matrix multiplication
!> \author  Immo Huismann
!>
!> \details
!> Explicit size application of a diagonal matrix on a vector.
!> As it is implemented in explicit size with only two array dimensions instead
!> of four or six, it is easily vectorized by the compiler.
!>
!> This routine is not designed to be used directly. Rather one of the wrappers
!> with explicit shape should be used. Mind that this routine is not
!> thread-safe, as it uses OpenACC and OpenMP to parallelize the operations.

subroutine ScaledDiagonalMatrixProduct_ES (k,m,diag,factors,values,res)
  integer  , intent(in)    :: k            !< size of first  dim of matrix A
  integer  , intent(in)    :: m            !< size of second dim of B
  real(RDP), intent(in)    :: diag   (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: factors(  m) !< Scaling factors
  real(RDP), intent(in)    :: values (k,m) !< Input data
  real(RDP), intent(  out) :: res    (k,m) !< Matrix to store the result im

  integer   :: i,j ! loop indices

  ! Mind that the copy of the diag matrix is not allowed in the data statement,
  ! unless the data statement receives the 'async' keyword.
  ! Obmitting the async might lead to the parallel region starting without the
  ! copy process being finished.

  !$acc data present(values,res) copyin(diag, factors) async(ACC_EXEC_QUEUE)
  !$acc parallel                                       async(ACC_EXEC_QUEUE)

  !$acc loop collapse(2) private(i,j)
  !$omp do               private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    res(i,j) = diag(i) * factors(j) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine ScaledDiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> \brief   Provides a diagonal matrix multiplication
!> \author  Immo Huismann
!>
!> \details
!> Explicit size application of a diagonal matrix on a vector.
!> As it is implemented in explicit size with only two array dimensions instead
!> of four or six, it is easily vectorized by the compiler.
!>
!> This routine is not designed to be used directly. Rather one of the wrappers
!> with explicit shape should be used. Mind that this routine is not
!> thread-safe, as it uses OpenACC and OpenMP to parallelize the operations.

subroutine AddScaledDiagonalMatrixProduct_ES (k,m,diag,factors,values,res)
  integer  , intent(in)    :: k            !< size of first  dim of matrix A
  integer  , intent(in)    :: m            !< size of second dim of B
  real(RDP), intent(in)    :: diag   (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: factors(  m) !< Scaling factors
  real(RDP), intent(in)    :: values (k,m) !< Input data
  real(RDP), intent(inout) :: res    (k,m) !< Matrix to store the result im

  integer   :: i,j ! loop indices

  ! Mind that the copy of the diag matrix is not allowed in the data statement,
  ! unless the data statement receives the 'async' keyword.
  ! Obmitting the async might lead to the parallel region starting without the
  ! copy process being finished.

  !$acc data present(values,res) copyin(diag, factors) async(ACC_EXEC_QUEUE)
  !$acc parallel                                       async(ACC_EXEC_QUEUE)

  !$acc loop collapse(2) private(i,j)
  !$omp do               private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    res(i,j) = res(i,j) + diag(i) * factors(j) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine AddScaledDiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> \brief   Provides a diagonal matrix multiplication
!> \author  Immo Huismann
!>
!> \details
!> Explicit size application of a diagonal matrix on a vector.
!> As it is implemented in explicit size with only two array dimensions instead
!> of four or six, it is easily vectorized by the compiler.
!>
!> This routine is not designed to be used directly. Rather one of the wrappers
!> with explicit shape should be used. Mind that this routine is not
!> thread-safe, as it uses OpenACC and OpenMP to parallelize the operations.

subroutine ScaledInPlaceDiagonalMatrixProduct_ES (k,m,diag,scales,values)
  integer  , intent(in)    :: k           !< size of first  dim of matrix A
  integer  , intent(in)    :: m           !< size of second dim of B
  real(RDP), intent(in)    :: diag  (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: scales(  m) !< elementwise scale
  real(RDP), intent(inout) :: values(k,m) !< data to work on

  integer   :: i,j ! loop indices

  ! Mind that the copy of the diag matrix is not allowed in the data statement,
  ! unless the data statement receives the 'async' keyword.
  ! Obmitting the async might lead to the parallel region starting without the
  ! copy process being finished.

  !$acc data present(values) copyin(diag,scales) async(ACC_EXEC_QUEUE)
  !$acc parallel                                 async(ACC_EXEC_QUEUE)

  !$acc loop collapse(2) private(i,j)
  !$omp do               private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    values(i,j) = diag(i) * scales(j) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

  !$acc end data

end subroutine ScaledInPlaceDiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> Same as DiagonalMatrixProduct_ES, only differing by adding to res instead of
!> setting it to the result of diag times values.

subroutine AddDiagonalMatrixProduct_ES (k,m,diag,values,res)
  integer  , intent(in)    :: k      !< size of first  dim of matrix A
  integer  , intent(in)    :: m      !< size of second dim of B
  real(RDP), intent(in)    :: diag  (k  ) !< Diagonal matrix to use B on
  real(RDP), intent(in)    :: values(k,m) !< Input data
  real(RDP), intent(inout) :: res   (k,m) !< Matrix to store the result im

  integer   :: i,j ! loop indices

  ! copying in async queue 2, so that the computation is not hindered
  !$acc data present(values,res) copyin(diag) async(ACC_EXEC_QUEUE)
  !$acc parallel                              async(ACC_EXEC_QUEUE)

  !$acc loop collapse(2) private(i,j)
  !$omp do               private(i,j)
  do j = 1, m
  do i = 1, k
    ! point-wise multiplication
    res(i,j) = res(i,j) + diag(i) * values(i,j)
  end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine AddDiagonalMatrixProduct_ES

!-------------------------------------------------------------------------------
!> \brief   A transpose operations working on the first two dimensions of a triad
!> \author  Immo Huismann
!>
!> \details
!> By itself, this routine is useless. Its goal lies in providing a means of
!> directly working on different dimensions of a data set by using the first
!> dimension. This in turn enables a better performance, as the cache lines are
!> fully utilized, or on a GPGPU the memory accesses are coalesced.

subroutine StackedTranspose(n_1,n_2,n_stack,in,out)
  integer  , intent(in)  :: n_1     !< first  dimension of in, second of out
  integer  , intent(in)  :: n_2     !< second dimension of in, first  of out
  integer  , intent(in)  :: n_stack !< stacked dimension of the transpose

  real(RDP), intent(in)  ::  in(n_1, n_2, n_stack) !< input vector
  real(RDP), intent(out) :: out(n_2, n_1, n_stack) !< its transpose

  integer :: i,j,k ! loop indices


  ! Transposing the first two dimensions. As typically storing data is more
  ! costly than loading data, the j loop is chosen as the inner loop. This
  ! delivers a slightly better performance, as the memory writes are aligned.

  !$acc data present(in,out)
  !$acc parallel async(ACC_EXEC_QUEUE)

  !$acc loop collapse(3) private(i,j,k)
  !$omp do               private(i,j,k)
  do k = 1,n_stack
    do i = 1, n_1
    do j = 1, n_2
      out(j,i,k) = in(i,j,k)
    end do
    end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine StackedTranspose

!-------------------------------------------------------------------------------
!> \brief   A transpose operations working on the first two dimensions of a triad.
!> \author  Immo Huismann
!>
!> \details
!> By itself, this routine is useless. Its goal lies in providing a means of
!> directly working on different dimensions of a data set by using the first
!> dimension. This in turn enables a better performance, as the cache lines are
!> fully utilized, or on a GPGPU the memory accesses are coalesced.

subroutine AddStackedTranspose(n_1,n_2,n_stack,in,res)
  integer  , intent(in ) :: n_1     !< first  dimension of in, second of out
  integer  , intent(in ) :: n_2     !< second dimension of in, first  of out
  integer  , intent(in ) :: n_stack !< stacked dimension of the transpose

  real(RDP), intent(in )   ::  in(n_1, n_2, n_stack) !< input vector
  real(RDP), intent(inout) :: res(n_2, n_1, n_stack) !< its transpose

  integer :: i,j,k ! loop indices

  ! Transposing the first two dimensions. As typically storing data is more
  ! costly than loading data, the j loop is chosen as the inner loop. This
  ! delivers a slightly better performance, as the memory writes are aligned.

  !$acc data present(in,res)
  !$acc parallel async(ACC_EXEC_QUEUE)

  !$acc loop collapse(3) private(i,j,k)
  !$omp do               private(i,j,k)
  do k = 1,n_stack
    do i = 1, n_1
    do j = 1, n_2
      res(j,i,k) = res(j,i,k) + in(i,j,k)
    end do
    end do
  end do
  !$omp end do
  !$acc end loop

  !$acc end parallel
  !$acc end data

end subroutine AddStackedTranspose

!===============================================================================

end module Matrix_Operations
