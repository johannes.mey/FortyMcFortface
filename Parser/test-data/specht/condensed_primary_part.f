!> \file      condensed_primary_part.f
!> \brief     Primary part for the condensed operator
!> \author    Immo Huismann
!> \date      2015/02/17
!> \copyright Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module Condensed_Primary_Part
  use Kind_parameters,             only: RDP
  use Constants,                   only: HALF
  use Standard_Operators_1D,       only: StandardOperators1D
  use Factored_Matrix_Products_2D, only: AddFactoredMatrixProduct2D

  use ACC_Parameters,              only: ACC_EXEC_QUEUE
  use Element_Boundary_Parameters, only: N_VERTICES, N_FACES, N_EDGES
  use Geometry_Coefficients,       only: GetHelmholtzCoefficients
  implicit none
  private

  public :: AddHelmholtzOpCondensedPrimary

  !-----------------------------------------------------------------------------
  !> \brief   Adds the condensed primary part to the result
  !> \author  Immo Huismann

  interface AddHelmholtzOpCondensedPrimary
    module procedure AddHelmholtzOpCondensedPrimary_ClassicInterface
    module procedure AddHelmholtzOpCondensedPrimary_ShortInterface
  end interface AddHelmholtzOpCondensedPrimary

contains

!-------------------------------------------------------------------------------
!> \brief   Calculates the primary part for the condensed system, adds it to r
!> \author  Immo Huismann

subroutine AddHelmholtzOpCondensedPrimary_ClassicInterface                     &
    (op,h,lambda,u_f,u_e,u_v,r_f,r_e,r_v,tmp_f_1,tmp_f_2)
  class(StandardOperators1D), intent(in) :: op     !< 1D operators
  real(RDP),                  intent(in) :: h(:,:) !< element width
  real(RDP),                  intent(in) :: lambda !< Helmholtz parameter

  real(RDP), intent(in)    :: u_f(:,:,:,:)         !< u values on faces
  real(RDP), intent(in)    :: u_e(  :,:,:)         !< u values on edges
  real(RDP), intent(in)    :: u_v(    :,:)         !< u values on vertices

  real(RDP), intent(inout) :: r_f(:,:,:,:)         !< r values on faces
  real(RDP), intent(inout) :: r_e(  :,:,:)         !< r values on edges
  real(RDP), intent(inout) :: r_v(    :,:)         !< r values on vertices

  real(RDP), intent(out)   :: tmp_f_1(:,:,:,:)     !< scratch values on faces
  real(RDP), intent(out)   :: tmp_f_2(:,:,:,:)     !< scratch values on faces

  integer :: n         ! number of internal points on one edge
  integer :: n_element ! number of elements

  real(RDP), allocatable, save :: d(:,:) ! element-wise Helmholtz coefficients

  !.............................................................................
  ! Initialization

  ! Get the problem size
  n         = size(r_f,1)
  n_element = size(r_f,4)

  ! allocate and set the Helmholtz coefficients
  call GetHelmholtzCoefficients(lambda, h, d)

  !$acc data copyin(d) async(ACC_EXEC_QUEUE)

  ! Add the primary parts ......................................................
  call HelmholtzOpPrimFaceToFace    (n,n_element,op,d,u_f,r_f,tmp_f_1,tmp_f_2)
  call HelmholtzOpPrimEdgeToFace    (n,n_element,op,d,u_e,r_f)
  call HelmholtzOpPrimFaceToEdge    (n,n_element,op,d,u_f,r_e)
  call HelmholtzOpPrimEdgeToEdge    (n,n_element,op,d,u_e,r_e)
  call HelmholtzOpPrimVertexToEdge  (n,n_element,op,d,u_v,r_e)
  call HelmholtzOpPrimEdgeToVertex  (n,n_element,op,d,u_e,r_v)
  call HelmholtzOpPrimVertexToVertex(n,n_element,op,d,u_v,r_v)

  !$acc end data

  ! Deallocate shared variables ................................................
  !$omp single
  deallocate(d)
  !$omp end single

end subroutine AddHelmholtzOpCondensedPrimary_ClassicInterface

!-------------------------------------------------------------------------------
!> \brief   Calculates the primary part for the condensed system, adds it to r
!> \author  Immo Huismann
!>
!> \details
!> This version gets the helmholtz coefficients directly and won't calculate
!> them every iteration

subroutine AddHelmholtzOpCondensedPrimary_ShortInterface                       &
    (op,d,u_f,u_e,u_v,r_f,r_e,r_v,tmp_f_1,tmp_f_2)
  class(StandardOperators1D), intent(in) :: op      !< 1D operators
  real(RDP),                  intent(in) :: d(0:,:) !< Helmholtz coefficients

  real(RDP), intent(in)    :: u_f(:,:,:,:)          !< u values on faces
  real(RDP), intent(in)    :: u_e(  :,:,:)          !< u values on edges
  real(RDP), intent(in)    :: u_v(    :,:)          !< u values on vertices

  real(RDP), intent(inout) :: r_f(:,:,:,:)          !< r values on faces
  real(RDP), intent(inout) :: r_e(  :,:,:)          !< r values on edges
  real(RDP), intent(inout) :: r_v(    :,:)          !< r values on vertices

  real(RDP), intent(out)   :: tmp_f_1(:,:,:,:)      !< scratch values on faces
  real(RDP), intent(out)   :: tmp_f_2(:,:,:,:)      !< scratch values on faces

  integer :: n         ! number of internal points on one edge
  integer :: n_element ! number of elements

  !.............................................................................
  ! Initialization

  ! Get the problem size
  n         = size(r_f,1)
  n_element = size(r_f,4)

  ! Add the primary parts ......................................................
  call HelmholtzOpPrimFaceToFace    (n,n_element,op,d,u_f,r_f,tmp_f_1,tmp_f_2)
  call HelmholtzOpPrimEdgeToFace    (n,n_element,op,d,u_e,r_f)
  call HelmholtzOpPrimFaceToEdge    (n,n_element,op,d,u_f,r_e)
  call HelmholtzOpPrimEdgeToEdge    (n,n_element,op,d,u_e,r_e)
  call HelmholtzOpPrimVertexToEdge  (n,n_element,op,d,u_v,r_e)
  call HelmholtzOpPrimEdgeToVertex  (n,n_element,op,d,u_e,r_v)
  call HelmholtzOpPrimVertexToVertex(n,n_element,op,d,u_v,r_v)

end subroutine AddHelmholtzOpCondensedPrimary_ShortInterface

! Bound to Bound operations ====================================================

!-------------------------------------------------------------------------------
!> \brief   Calculates vertex to vertex interaction in the Helmholtz operator
!> \author  Immo Huismann
!>
!> \details
!> Mind that the properties
!>
!>    L_{0p} == L_{p0}
!>    L_{00} == L_{pp}
!>    w_{0}  == w_{p}
!>
!> are being exploited to shorten all terms.
!> These properties hold for the GLL and GL points (they do for all symmetric
!> node distribution), but for other base choices they might not.
!>
!> Explicit size for an easier compilation with OpenACC.

subroutine HelmholtzOpPrimVertexToVertex(n,n_element,op,d,u,r)
  integer,                    intent(in) :: n         !< po - 1
  integer,                    intent(in) :: n_element !< number of elements
  class(StandardOperators1D), intent(in) :: op        !< 1D operators

  real(RDP), intent(in)    :: d(0:3,n_element)        !< element helmholtz coef.
  real(RDP), intent(in)    :: u(N_VERTICES,n_element) !< variable u on vertices
  real(RDP), intent(inout) :: r(N_VERTICES,n_element) !< r = H u, r on vertices

  real(RDP) :: laplace_factor_00, laplace_factor_0p, mass_factor
  real(RDP) :: L_00, L_0p, w_0          ! extracts from the system matrices
  real(RDP) :: d_xy_00,d_xz_00, d_yz_00 ! geometry coefficients from 0 to 0
  real(RDP) :: d_xy_0p,d_xz_0p, d_yz_0p ! geometry coefficients from 0 to p
  real(RDP) :: d_xyz                    ! coefficient from mass term
  integer   :: e                        ! element loop index

  ! Calculate the factors common to each element ...............................
  ! Mind that the properties
  !
  !    L_{0p} == L_{p0}
  !    L_{00} == L_{pp}
  !    w_{0}  == w_{p}
  !
  ! are being exploited to shorten all terms

  L_00 = op%L(0,0  )
  L_0p = op%L(0,n+1)
  w_0  = op%w(0)

  mass_factor       = w_0 * w_0 * w_0
  laplace_factor_00 = w_0 * w_0 * L_00
  laplace_factor_0p = w_0 * w_0 * L_0p

  ! vertex to vertex interaction ...............................................

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u,r)
  !$acc loop
  !$omp do private(e,d_xyz,d_xy_00,d_xy_0p,d_xz_00,d_xz_0p,d_yz_00,d_yz_0p)
  do e = 1, n_element
    ! set the metric coefficients
    d_xyz   = d(0,e) * mass_factor

    d_xy_00 = d(3,e) * laplace_factor_00
    d_xy_0p = d(3,e) * laplace_factor_0p

    d_xz_00 = d(2,e) * laplace_factor_00
    d_xz_0p = d(2,e) * laplace_factor_0p

    d_yz_00 = d(1,e) * laplace_factor_00
    d_yz_0p = d(1,e) * laplace_factor_0p

    ! r(0,0,0,e)
    ! depends on       self          u(0,0,0,e) -- u(1,e)
    !                   + x          u(p,0,0,e) -- u(2,e)
    !                   + y          u(0,p,0,e) -- u(3,e)
    !                   + z          u(0,0,p,e) -- u(5,e)
    r(1,e) =                                       r(1,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(1,e)                      &
         + (                            d_yz_0p) * u(2,e)                      &
         + (                 d_xz_0p           ) * u(3,e)                      &
         + (        d_xy_0p                    ) * u(5,e)

    ! r(p,0,0,e)
    ! depends on       self          u(p,0,0,e) -- u(2,e)
    !                   - x          u(0,0,0,e) -- u(1,e)
    !                   + y          u(p,p,0,e) -- u(4,e)
    !                   + z          u(p,0,p,e) -- u(6,e)
    r(2,e) =                                       r(2,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(2,e)                      &
         + (                            d_yz_0p) * u(1,e)                      &
         + (                  d_xz_0p          ) * u(4,e)                      &
         + (        d_xy_0p                    ) * u(6,e)

    ! r(0,p,0,e)
    ! depends on       self          u(0,p,0,e) -- u(3,e)
    !                   + x          u(p,p,0,e) -- u(4,e)
    !                   - y          u(0,0,0,e) -- u(1,e)
    !                   + z          u(0,p,p,e) -- u(7,e)
    r(3,e) =                                       r(3,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(3,e)                      &
         + (                            d_yz_0p) * u(4,e)                      &
         + (                  d_xz_0p          ) * u(1,e)                      &
         + (        d_xy_0p                    ) * u(7,e)

    ! r(p,p,0,e)
    ! depends on       self          u(p,p,0,e) -- u(4,e)
    !                   - x          u(0,p,0,e) -- u(3,e)
    !                   - y          u(p,0,0,e) -- u(2,e)
    !                   + z          u(p,p,p,e) -- u(8,e)
    r(4,e) =                                       r(4,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(4,e)                      &
         + (                            d_yz_0p) * u(3,e)                      &
         + (                  d_xz_0p          ) * u(2,e)                      &
         + (        d_xy_0p                    ) * u(8,e)

    ! r(0,0,p,e)
    ! depends on       self          u(0,0,p,e) -- u(5,e)
    !                   + x          u(p,0,p,e) -- u(6,e)
    !                   + y          u(0,p,p,e) -- u(7,e)
    !                   - z          u(0,0,p,e) -- u(5,e)
    r(5,e) =                                       r(5,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(5,e)                      &
         + (                            d_yz_0p) * u(6,e)                      &
         + (                  d_xz_0p          ) * u(7,e)                      &
         + (        d_xy_0p                    ) * u(1,e)

    ! r(p,0,p,e)
    ! depends on       self          u(p,0,p,e) -- u(6,e)
    !                   - x          u(0,0,p,e) -- u(5,e)
    !                   + y          u(p,p,p,e) -- u(8,e)
    !                   - z          u(p,0,0,e) -- u(2,e)
    r(6,e) =                                       r(6,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(6,e)                      &
         + (                            d_yz_0p) * u(5,e)                      &
         + (                  d_xz_0p          ) * u(8,e)                      &
         + (        d_xy_0p                    ) * u(2,e)

    ! r(0,p,p,e)
    ! depends on       self          u(0,p,p,e) -- u(7,e)
    !                   + x          u(p,p,p,e) -- u(8,e)
    !                   - y          u(0,0,p,e) -- u(5,e)
    !                   - z          u(0,p,0,e) -- u(3,e)
    r(7,e) =                                       r(7,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(7,e)                      &
         + (                            d_yz_0p) * u(8,e)                      &
         + (                 d_xz_0p           ) * u(5,e)                      &
         + (        d_xy_0p                    ) * u(3,e)

    ! r(p,p,p,e)
    ! depends on       self          u(p,p,p,e) -- u(8,e)
    !                   - x          u(0,p,p,e) -- u(7,e)
    !                   - y          u(p,0,p,e) -- u(6,e)
    !                   - z          u(p,p,0,e) -- u(4,e)
    r(8,e) =                                       r(8,e)                      &
         + (d_xyz + d_xy_00 + d_xz_00 + d_yz_00) * u(8,e)                      &
         + (                            d_yz_0p) * u(7,e)                      &
         + (                  d_xz_0p          ) * u(6,e)                      &
         + (        d_xy_0p                    ) * u(4,e)

  end do
  !$omp end do
  !$acc end parallel


end subroutine HelmholtzOpPrimVertexToVertex

!-------------------------------------------------------------------------------
!> \brief   Vertex to edge interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann

subroutine HelmholtzOpPrimVertexToEdge(n,n_element,op,d,u_v,r_e)
  integer,   intent(in)    :: n                     !< points per inner line
  integer,   intent(in)    :: n_element             !< number of elements
  class(StandardOperators1D), intent(in) :: op      !< polynomial system
  real(RDP), intent(in)    :: d(0:3,n_element)      !< helmholtz factors
  real(RDP), intent(in)    :: u_v(  N_VERTICES,n_element) !< variable u
  real(RDP), intent(inout) :: r_e(n,N_EDGES,   n_element) !< r = H u

  real(RDP) :: mat_i0(n), mat_ip(n)                     ! matrices to use
  real(RDP) :: d_1_0, d_1_p, d_2_0, d_2_p, d_3_0, d_3_p ! matrix entry * metric
  integer   :: i, e                                     ! loop indices

  ! Initialization .............................................................

  ! Set the rows of the matrices
  mat_i0 =  op%L(1:n,0  ) * op%w(0) * op%w(0)
  mat_ip =  op%L(1:n,n+1) * op%w(0) * op%w(0)

  ! Computation ................................................................

  ! Scalar to vector -> a perfectly nested loop

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_v,r_e)
  !$acc loop collapse(2) private(d_1_0,d_1_p,d_2_0,d_2_p,d_3_0,d_3_p)
  !$omp do private(i,e, d_1_0,d_1_p,d_2_0,d_2_p,d_3_0,d_3_p)
  do e = 1, n_element
  do i = 1, n
    ! Compute factors for point and element
    d_1_0 = d(1,e) * mat_i0(i); d_1_p = d(1,e) * mat_ip(i)
    d_2_0 = d(2,e) * mat_i0(i); d_2_p = d(2,e) * mat_ip(i)
    d_3_0 = d(3,e) * mat_i0(i); d_3_p = d(3,e) * mat_ip(i)

    !...........................................................................
    ! x_1 edges
    r_e(i, 1,e) = r_e(i, 1,e) + d_1_0 * u_v(1,e) + d_1_p * u_v(2,e)
    r_e(i, 2,e) = r_e(i, 2,e) + d_1_0 * u_v(3,e) + d_1_p * u_v(4,e)
    r_e(i, 3,e) = r_e(i, 3,e) + d_1_0 * u_v(5,e) + d_1_p * u_v(6,e)
    r_e(i, 4,e) = r_e(i, 4,e) + d_1_0 * u_v(7,e) + d_1_p * u_v(8,e)

    !...........................................................................
    ! x_2 edges
    r_e(i, 5,e) = r_e(i, 5,e) + d_2_0 * u_v(1,e) + d_2_p * u_v(3,e)
    r_e(i, 6,e) = r_e(i, 6,e) + d_2_0 * u_v(2,e) + d_2_p * u_v(4,e)
    r_e(i, 7,e) = r_e(i, 7,e) + d_2_0 * u_v(5,e) + d_2_p * u_v(7,e)
    r_e(i, 8,e) = r_e(i, 8,e) + d_2_0 * u_v(6,e) + d_2_p * u_v(8,e)

    !...........................................................................
    ! x_3 edges
    r_e(i, 9,e) = r_e(i, 9,e) + d_3_0 * u_v(1,e) + d_3_p * u_v(5,e)
    r_e(i,10,e) = r_e(i,10,e) + d_3_0 * u_v(2,e) + d_3_p * u_v(6,e)
    r_e(i,11,e) = r_e(i,11,e) + d_3_0 * u_v(3,e) + d_3_p * u_v(7,e)
    r_e(i,12,e) = r_e(i,12,e) + d_3_0 * u_v(4,e) + d_3_p * u_v(8,e)

  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine HelmholtzOpPrimVertexToEdge

!-------------------------------------------------------------------------------
!> \brief   Edge to vertex interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann
!>
!> \todo
!> * maybe think about reimplementing it with a loop over the edges
!>  * could have fewer loads, though that is probably wasted on this routine

subroutine HelmholtzOpPrimEdgeToVertex(n,n_element,op,d,u_e,r_v)
  integer,   intent(in)    :: n                           !< points per edge
  integer,   intent(in)    :: n_element                   !< number of elements
  class(StandardOperators1D), intent(in) :: op            !< 1D operators
  real(RDP), intent(in)    :: d(0:3,n_element)            !< helmholtz factors
  real(RDP), intent(in)    :: u_e(n,N_EDGES,   n_element) !< variable u on edges
  real(RDP), intent(inout) :: r_v(  N_VERTICES,n_element) !< r = H u on vertices

  real(RDP) :: mat_i0(n), mat_ip(n)  ! operators
  integer   :: i, e

  ! Initialization .............................................................
  ! Set the rows of the matrices
  mat_i0 =  op%L(1:n,0  ) * op%w(0) * op%w(0)
  mat_ip =  op%L(1:n,n+1) * op%w(0) * op%w(0)

  ! Computation ................................................................


  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_v)
  !$acc loop
  !$omp do private(i,e)
  do e = 1, n_element
  do i = 1, n
    r_v(1,e) = r_v(1,e) + mat_I0(i) * d(1,e) * u_e(i, 1,e)                     &
                        + mat_I0(i) * d(2,e) * u_e(i, 5,e)                     &
                        + mat_I0(i) * d(3,e) * u_e(i, 9,e)

    r_v(2,e) = r_v(2,e) + mat_Ip(i) * d(1,e) * u_e(i, 1,e)                     &
                        + mat_I0(i) * d(2,e) * u_e(i, 6,e)                     &
                        + mat_I0(i) * d(3,e) * u_e(i,10,e)

    r_v(3,e) = r_v(3,e) + mat_I0(i) * d(1,e) * u_e(i, 2,e)                     &
                        + mat_Ip(i) * d(2,e) * u_e(i, 5,e)                     &
                        + mat_I0(i) * d(3,e) * u_e(i,11,e)

    r_v(4,e) = r_v(4,e) + mat_Ip(i) * d(1,e) * u_e(i, 2,e)                     &
                        + mat_Ip(i) * d(2,e) * u_e(i, 6,e)                     &
                        + mat_I0(i) * d(3,e) * u_e(i,12,e)

    r_v(5,e) = r_v(5,e) + mat_I0(i) * d(1,e) * u_e(i, 3,e)                     &
                        + mat_I0(i) * d(2,e) * u_e(i, 7,e)                     &
                        + mat_Ip(i) * d(3,e) * u_e(i, 9,e)

    r_v(6,e) = r_v(6,e) + mat_Ip(i) * d(1,e) * u_e(i, 3,e)                     &
                        + mat_I0(i) * d(2,e) * u_e(i, 8,e)                     &
                        + mat_Ip(i) * d(3,e) * u_e(i,10,e)

    r_v(7,e) = r_v(7,e) + mat_I0(i) * d(1,e) * u_e(i, 4,e)                     &
                        + mat_Ip(i) * d(2,e) * u_e(i, 7,e)                     &
                        + mat_Ip(i) * d(3,e) * u_e(i,11,e)

    r_v(8,e) = r_v(8,e) + mat_Ip(i) * d(1,e) * u_e(i, 4,e)                     &
                        + mat_Ip(i) * d(2,e) * u_e(i, 8,e)                     &
                        + mat_Ip(i) * d(3,e) * u_e(i,12,e)

  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine HelmholtzOpPrimEdgeToVertex

!-------------------------------------------------------------------------------
!> \brief   Edge to vertex interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann

subroutine HelmholtzOpPrimEdgeToEdge(n,n_element,op,d,u_e,r_e)
  integer,   intent(in)    :: n                 !< points per inner line
  integer,   intent(in)    :: n_element         !< number of elements
  class(StandardOperators1D), intent(in) :: op  !< 1D operators
  real(RDP), intent(in)    :: d(0:3,n_element)  !< element helmholtz coeffs
  real(RDP), intent(in)    :: u_e(n,N_EDGES,n_element) !< variable u on edges
  real(RDP), intent(inout) :: r_e(n,N_EDGES,n_element) !< r = H u    on edges

  real(RDP) :: w_0            ! first coefficient in mass matrix
  real(RDP) :: L_00           ! coefficient (0,0) in laplace matrix
  real(RDP) :: L_0p           ! coefficient (0,p) in laplace matrix
  real(RDP) :: w_I(n)         ! inner part of mass    matrix
  real(RDP) :: L_I_w_w(n,n)   ! inner part of laplace matrix * w_0^2
  real(RDP) :: factors(3)     ! metric factors * matrices

  integer   :: i,j,e

  ! initialization .............................................................

  ! Set the operators
  w_0  = op%w(0)
  L_00 = op%L(0,0  )
  L_0p = op%L(0,n+1)
  w_I  = op%w(1:n)
  L_I_w_w  = op%L(1:n,1:n) * w_0 * w_0

  ! Laplace term in edge direction .............................................


  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_e)
  !$acc loop collapse(2)
  !$omp do private(i,j,e)
  do e = 1, n_element
  do i = 1, n
    do j = 1, n
      r_e(i, 1,e) = r_e(i, 1,e) + d(1,e) * L_I_w_w(i,j) * u_e(j, 1,e)
      r_e(i, 2,e) = r_e(i, 2,e) + d(1,e) * L_I_w_w(i,j) * u_e(j, 2,e)
      r_e(i, 3,e) = r_e(i, 3,e) + d(1,e) * L_I_w_w(i,j) * u_e(j, 3,e)
      r_e(i, 4,e) = r_e(i, 4,e) + d(1,e) * L_I_w_w(i,j) * u_e(j, 4,e)

      r_e(i, 5,e) = r_e(i, 5,e) + d(2,e) * L_I_w_w(i,j) * u_e(j, 5,e)
      r_e(i, 6,e) = r_e(i, 6,e) + d(2,e) * L_I_w_w(i,j) * u_e(j, 6,e)
      r_e(i, 7,e) = r_e(i, 7,e) + d(2,e) * L_I_w_w(i,j) * u_e(j, 7,e)
      r_e(i, 8,e) = r_e(i, 8,e) + d(2,e) * L_I_w_w(i,j) * u_e(j, 8,e)

      r_e(i, 9,e) = r_e(i, 9,e) + d(3,e) * L_I_w_w(i,j) * u_e(j, 9,e)
      r_e(i,10,e) = r_e(i,10,e) + d(3,e) * L_I_w_w(i,j) * u_e(j,10,e)
      r_e(i,11,e) = r_e(i,11,e) + d(3,e) * L_I_w_w(i,j) * u_e(j,11,e)
      r_e(i,12,e) = r_e(i,12,e) + d(3,e) * L_I_w_w(i,j) * u_e(j,12,e)
    end do
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

  ! Laplace term to other edges ................................................


  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_e)
  !$acc loop collapse(2) private(factors)
  !$omp do private(i,e)
  do e = 1, n_element
  do i = 1, n

    !...........................................................................
    ! x_1 edges
    ! Calculate the metric factors, then use them
    factors(1) = w_I(i) * w_0 * (d(0,e) * w_0 + L_00 * (d(2,e) + d(3,e)))
    factors(2) = w_I(i) * w_0 * L_0p * d(2,e)
    factors(3) = w_I(i) * w_0 * L_0p * d(3,e)

    r_e(i, 1,e) =      r_e(i, 1,e)                                              &
        + factors(1) * u_e(i, 1,e)                                              &
        + factors(2) * u_e(i, 2,e)                                              &
        + factors(3) * u_e(i, 3,e)

    r_e(i, 2,e) =      r_e(i, 2,e)                                              &
        + factors(1) * u_e(i, 2,e)                                              &
        + factors(2) * u_e(i, 1,e)                                              &
        + factors(3) * u_e(i, 4,e)

    r_e(i, 3,e) =      r_e(i, 3,e)                                              &
        + factors(1) * u_e(i, 3,e)                                              &
        + factors(2) * u_e(i, 4,e)                                              &
        + factors(3) * u_e(i, 1,e)

    r_e(i, 4,e) =      r_e(i, 4,e)                                              &
        + factors(1) * u_e(i, 4,e)                                              &
        + factors(2) * u_e(i, 3,e)                                              &
        + factors(3) * u_e(i, 2,e)

    !...........................................................................
    ! x_2 edges
    ! Calculate the metric factors, then use them
    factors(1) = w_I(i) * w_0 * (d(0,e) * w_0 + L_00 * (d(1,e) + d(3,e)))
    factors(2) = w_I(i) * w_0 * L_0p * d(1,e)
    factors(3) = w_I(i) * w_0 * L_0p * d(3,e)

    r_e(i, 5,e) =      r_e(i, 5,e)                                              &
        + factors(1) * u_e(i, 5,e)                                              &
        + factors(2) * u_e(i, 6,e)                                              &
        + factors(3) * u_e(i, 7,e)

    r_e(i, 6,e) =      r_e(i, 6,e)                                              &
        + factors(1) * u_e(i, 6,e)                                              &
        + factors(2) * u_e(i, 5,e)                                              &
        + factors(3) * u_e(i, 8,e)

    r_e(i, 7,e) =      r_e(i, 7,e)                                              &
        + factors(1) * u_e(i, 7,e)                                              &
        + factors(2) * u_e(i, 8,e)                                              &
        + factors(3) * u_e(i, 5,e)

    r_e(i, 8,e) =      r_e(i, 8,e)                                              &
        + factors(1) * u_e(i, 8,e)                                              &
        + factors(2) * u_e(i, 7,e)                                              &
        + factors(3) * u_e(i, 6,e)

    !...........................................................................
    ! x_3 edges
    ! Calculate the metric factors, then use them
    factors(1) = w_I(i) * w_0 * (d(0,e) * w_0 + L_00 * (d(1,e) + d(2,e)))
    factors(2) = w_I(i) * w_0 * L_0p * d(1,e)
    factors(3) = w_I(i) * w_0 * L_0p * d(2,e)

    r_e(i, 9,e) =      r_e(i, 9,e)                                              &
        + factors(1) * u_e(i, 9,e)                                              &
        + factors(2) * u_e(i,10,e)                                              &
        + factors(3) * u_e(i,11,e)

    r_e(i,10,e) =      r_e(i,10,e)                                              &
        + factors(1) * u_e(i,10,e)                                              &
        + factors(2) * u_e(i, 9,e)                                              &
        + factors(3) * u_e(i,12,e)

    r_e(i,11,e) =      r_e(i,11,e)                                              &
        + factors(1) * u_e(i,11,e)                                              &
        + factors(2) * u_e(i,12,e)                                              &
        + factors(3) * u_e(i, 9,e)

    r_e(i,12,e) =      r_e(i,12,e)                                              &
        + factors(1) * u_e(i,12,e)                                              &
        + factors(2) * u_e(i,11,e)                                              &
        + factors(3) * u_e(i,10,e)

  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine HelmholtzOpPrimEdgeToEdge

!-------------------------------------------------------------------------------
!> \brief   Edge to face interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann

subroutine HelmholtzOpPrimEdgeToFace(n,n_element,op,d,u_e,r_f)
  integer,   intent(in)    :: n                     !< points per inner line
  integer,   intent(in)    :: n_element             !< number of elements
  class(StandardOperators1D), intent(in) :: op      !< 1D operators
  real(RDP), intent(in)    :: d(0:3,n_element)      !< helmholtz coefficients
  real(RDP), intent(in)    :: u_e(  n,N_EDGES,n_element) !< variable u on edges
  real(RDP), intent(inout) :: r_f(n,n,N_FACES,n_element) !< r = H u    on faces

  integer   :: e,i,j                                          ! loop indices

  real(RDP) :: w_I(n), L_I0(n), L_Ip(n)                       ! operators

  ! initialization .............................................................

  ! Set the row matrices
  w_I  = op%w(1:n)
  L_I0 = op%L(1:n,0  ) * op%w(0)
  L_Ip = op%L(1:n,n+1) * op%w(0)

  ! x_1 edges ..................................................................

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_e,r_f)
  !$acc loop collapse(3)
  !$omp do private(i,j,e)
  do e = 1, n_element
  do j = 1, n
  do i = 1, n

    ! x_1 faces
    r_f(i,j,1,e) =                        r_f(i,j, 1,e)                        &
        + d(3,e) * w_I(i  ) * L_I0(  j) * u_e(i,   5,e)                        &
        + d(3,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   7,e)                        &
        + d(2,e) * w_I(  j) * L_I0(i  ) * u_e(  j, 9,e)                        &
        + d(2,e) * w_I(  j) * L_Ip(i  ) * u_e(  j,11,e)

    r_f(i,j,2,e) =                        r_f(i,j, 2,e)                        &
        + d(3,e) * w_I(i  ) * L_I0(  j) * u_e(i,   6,e)                        &
        + d(3,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   8,e)                        &
        + d(2,e) * w_I(  j) * L_I0(i  ) * u_e(  j,10,e)                        &
        + d(2,e) * w_I(  j) * L_Ip(i  ) * u_e(  j,12,e)

    ! x_2 faces
    r_f(i,j,3,e) =                        r_f(i,j, 3,e)                        &
        + d(3,e) * w_I(i  ) * L_I0(  j) * u_e(i,   1,e)                        &
        + d(3,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   3,e)                        &
        + d(1,e) * w_I(  j) * L_I0(i  ) * u_e(  j, 9,e)                        &
        + d(1,e) * w_I(  j) * L_Ip(i  ) * u_e(  j,10,e)

    r_f(i,j,4,e) =                        r_f(i,j, 4,e)                        &
        + d(3,e) * w_I(i  ) * L_I0(  j) * u_e(i,   2,e)                        &
        + d(3,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   4,e)                        &
        + d(1,e) * w_I(  j) * L_I0(i  ) * u_e(  j,11,e)                        &
        + d(1,e) * w_I(  j) * L_Ip(i  ) * u_e(  j,12,e)

    ! x_3 faces
    r_f(i,j,5,e) =                        r_f(i,j, 5,e)                        &
        + d(2,e) * w_I(i  ) * L_I0(  j) * u_e(i,   1,e)                        &
        + d(2,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   2,e)                        &
        + d(1,e) * w_I(  j) * L_I0(i  ) * u_e(  j, 5,e)                        &
        + d(1,e) * w_I(  j) * L_Ip(i  ) * u_e(  j, 6,e)

    r_f(i,j,6,e) =                        r_f(i,j, 6,e)                        &
        + d(2,e) * w_I(i  ) * L_I0(  j) * u_e(i,   3,e)                        &
        + d(2,e) * w_I(i  ) * L_Ip(  j) * u_e(i,   4,e)                        &
        + d(1,e) * w_I(  j) * L_I0(i  ) * u_e(  j, 7,e)                        &
        + d(1,e) * w_I(  j) * L_Ip(i  ) * u_e(  j, 8,e)

  end do
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine HelmholtzOpPrimEdgeToFace

!-------------------------------------------------------------------------------
!> \brief   Face to edge interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann
!>
!> \todo
!> * Put the computation of d_i into an outer routine

subroutine HelmholtzOpPrimFaceToEdge(n,n_element,op,d,u_f,r_e)
  integer,   intent(in)    :: n                     !< points per inner line
  integer,   intent(in)    :: n_element             !< number of elements
  class(StandardOperators1D), intent(in) :: op      !< 1D operators
  real(RDP), intent(in)    :: d(0:3,n_element)      !< helmholtz coefficients
  real(RDP), intent(in)    :: u_f(n,n,N_FACES,n_element) !< variable u on faces
  real(RDP), intent(inout) :: r_e(  n,N_EDGES,n_element) !< r = H u    on edges

  integer   :: e,i,j                                          ! loop indices

  real(RDP) :: w_I(n), L_0I(n), L_pI(n)                       ! operators

  ! initialization .............................................................

  ! Set the row matrices
  w_I  = op%w(1:n)
  L_0I = op%L(0  ,1:n) * op%w(0)
  L_pI = op%L(n+1,1:n) * op%w(0)

  ! One loop to rule them all ..................................................

  ! A three times nested loop is utilized. One for the matrix multiplication,
  ! two for the points themselve.
  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_f,r_e)
  !$acc loop collapse(2)
  !$omp do private(i,j,e)
  do e = 1, n_element
  do i = 1, n
    do j = 1, n
      r_e(i, 1,e) =                     r_e(i  , 1,e)                            &
          + d(2,e) * w_I(i) * L_0I(j) * u_f(i,j, 5,e)                            &
          + d(3,e) * w_I(i) * L_0I(j) * u_f(i,j, 3,e)

      r_e(i, 2,e) =                     r_e(i  , 2,e)                            &
          + d(2,e) * w_I(i) * L_pI(j) * u_f(i,j, 5,e)                            &
          + d(3,e) * w_I(i) * L_0I(j) * u_f(i,j, 4,e)

      r_e(i, 3,e) =                     r_e(i  , 3,e)                            &
          + d(2,e) * w_I(i) * L_0I(j) * u_f(i,j, 6,e)                            &
          + d(3,e) * w_I(i) * L_pI(j) * u_f(i,j, 3,e)

      r_e(i, 4,e) =                     r_e(i  , 4,e)                            &
          + d(2,e) * w_I(i) * L_pI(j) * u_f(i,j, 6,e)                            &
          + d(3,e) * w_I(i) * L_pI(j) * u_f(i,j, 4,e)

      r_e(i, 5,e) =                     r_e(i  , 5,e)                            &
          + d(3,e) * w_I(i) * L_0I(j) * u_f(i,j, 1,e)                            &
          + d(1,e) * w_I(i) * L_0I(j) * u_f(j,i, 5,e)

      r_e(i, 6,e) =                     r_e(i  , 6,e)                            &
          + d(3,e) * w_I(i) * L_0I(j) * u_f(i,j, 2,e)                            &
          + d(1,e) * w_I(i) * L_pI(j) * u_f(j,i, 5,e)

      r_e(i, 7,e) =                     r_e(i  , 7,e)                            &
          + d(3,e) * w_I(i) * L_pI(j) * u_f(i,j, 1,e)                            &
          + d(1,e) * w_I(i) * L_0I(j) * u_f(j,i, 6,e)

      r_e(i, 8,e) =                     r_e(i  , 8,e)                            &
          + d(3,e) * w_I(i) * L_pI(j) * u_f(i,j, 2,e)                            &
          + d(1,e) * w_I(i) * L_pI(j) * u_f(j,i, 6,e)

      r_e(i, 9,e) =                     r_e(i  , 9,e)                            &
          + d(2,e) * w_I(i) * L_0I(j) * u_f(j,i, 1,e)                            &
          + d(1,e) * w_I(i) * L_0I(j) * u_f(j,i, 3,e)

      r_e(i,10,e) =                     r_e(i  ,10,e)                            &
          + d(2,e) * w_I(i) * L_0I(j) * u_f(j,i, 2,e)                            &
          + d(1,e) * w_I(i) * L_pI(j) * u_f(j,i, 3,e)

      r_e(i,11,e) =                     r_e(i  ,11,e)                            &
          + d(2,e) * w_I(i) * L_pI(j) * u_f(j,i, 1,e)                            &
          + d(1,e) * w_I(i) * L_0I(j) * u_f(j,i, 4,e)

      r_e(i,12,e) =                     r_e(i  ,12,e)                            &
          + d(2,e) * w_I(i) * L_pI(j) * u_f(j,i, 2,e)                            &
          + d(1,e) * w_I(i) * L_pI(j) * u_f(j,i, 4,e)

    end do
  end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine HelmholtzOpPrimFaceToEdge

!-------------------------------------------------------------------------------
!> \brief   Face to face interaction in the Helmholtz operator r = H u
!> \author  Immo Huismann
!>
!> \todo
!> * Put the computation of d_i into an outer routine
!> * Put the treatment of one direction's faces into a separate subroutine
!>   * leads to a more cache-friendly implementation (most crucial part of this
!>     module)
!>
!> \details
!> For each edge two loops are present for the laplace terms. This is due to a
!> compiler bug (with O3) in ifort.

subroutine HelmholtzOpPrimFaceToFace(n,n_element,op,d,u_f,r_f,tmp_f_1,tmp_f_2)
  integer,   intent(in)    :: n                    !< points per inner line
  integer,   intent(in)    :: n_element            !< number of elements
  class(StandardOperators1D), intent(in) :: op     !< 1D operators
  real(RDP), intent(in)    :: d(0:3,n_element)     !< helmholtz factors
  real(RDP), intent(in)    :: u_f    (n,n,N_FACES,n_element) !< variable u on faces
  real(RDP), intent(inout) :: r_f    (n,n,N_FACES,n_element) !< r = H u    on edges
  real(RDP), intent(out)   :: tmp_f_1(n,n,N_FACES,n_element) !< scratch values
  real(RDP), intent(out)   :: tmp_f_2(n,n,N_FACES,n_element) !< scratch values

  integer   :: i,j ! loop indices

  real(RDP) :: M_II(n,n), w_I(n), L_II(n,n), L_0p, w_0,L_00     ! operators

  ! initialization .............................................................

  ! Set the row matrices
  w_0  = op%w(0)
  w_I  = op%w(1:n)
  L_00 = op%L(0,  0  )
  L_II = op%L(1:n,1:n)
  L_0p = op%L(0,  n+1)
  M_II = reshape([((w_I(i) * w_I(j), i = 1, n), j = 1,n)], [n,n])

  ! Work in each of the three directions seperatedly
  call FaceToFace&
      (n,n_element,d,w_0,L_00,L_0p,w_I,M_II,L_II,u_f,r_f,tmp_f_1,tmp_f_2)


end subroutine HelmholtzOpPrimFaceToFace

!===============================================================================
! Suboperators

subroutine FaceToFace&
    (n,n_element,d,w_0,L_00,L_0p,w_I,M_II,L_I,u_f,r_f,tmp_f_1,tmp_f_2)
  integer,   intent(in) :: n              !< points per edge
  integer,   intent(in) :: n_element      !< number of elements
  real(RDP), intent(in) :: d(0:3,n_element) !< mass term coefficient
  real(RDP), intent(in) :: w_0            !< coefficient (0,0) of mass matrix
  real(RDP), intent(in) :: L_00           !< coefficient (0,0) of laplace matrix
  real(RDP), intent(in) :: L_0p           !< coefficient (0,p) of laplace matrix
  real(RDP), intent(in) :: w_I(n)         !< inner part of mass matrix
  real(RDP), intent(in) :: M_II(n,n)      !< inner part of 2D mass matrix
  real(RDP), intent(in) :: L_I (n,n)      !< inner part of laplace matrix

  real(RDP), intent(in)    :: u_f    (n,n,N_FACES,n_element)
  real(RDP), intent(inout) :: r_f    (n,n,N_FACES,n_element)
  real(RDP), intent(out)   :: tmp_f_1(n,n,N_FACES,n_element)
  real(RDP), intent(out)   :: tmp_f_2(n,n,N_FACES,n_element)

  ! laplace factors on faces, in first and second dimension on face
  real(RDP), allocatable, save :: d_f_1(:,:), d_f_2(:,:)

  integer   :: e,i,j

  !.............................................................................
  ! Initialization of on-face laplace factors

  !$omp single
  allocate(d_f_1(N_FACES, n_element))
  allocate(d_f_2(N_FACES, n_element))
  !$omp end single

  !$omp do private(e)
  do e = 1, n_element
    ! metric factor for first dimension of face
    d_f_1(1, e) = d(2,e)
    d_f_1(2, e) = d(2,e)
    d_f_1(3, e) = d(1,e)
    d_f_1(4, e) = d(1,e)
    d_f_1(5, e) = d(1,e)
    d_f_1(6, e) = d(1,e)

    ! metric factor for second dimension of face
    d_f_2(1, e) = d(3,e)
    d_f_2(2, e) = d(3,e)
    d_f_2(3, e) = d(3,e)
    d_f_2(4, e) = d(3,e)
    d_f_2(5, e) = d(2,e)
    d_f_2(6, e) = d(2,e)
  end  do
  !$omp  end do

  !.............................................................................
  ! Mass term + orthogonal components

  !$acc parallel async(ACC_EXEC_QUEUE) present(d,u_f,r_f)
  !$acc loop
  !$omp  do private(i,j,e)
  do e  = 1, n_element
    !$acc loop collapse(2)
    do  j = 1, n
    do  i = 1, n
       r_f(i,j,1,e) = r_f(i,j,1,e)                                             &
           + M_II(i,j) * (d(0,e) * w_0 + d(1,e) * L_00) * u_f(i,j,1,e)         &
           + M_II(i,j) *                 d(1,e) * L_0p  * u_f(i,j,2,e)
       r_f(i,j,2,e) = r_f(i,j,2,e)                                             &
           + M_II(i,j) * (d(0,e) * w_0 + d(1,e) * L_00) * u_f(i,j,2,e)         &
           + M_II(i,j) *                 d(1,e) * L_0p  * u_f(i,j,1,e)
    end do
    end do
    !$acc end loop
    !$acc loop collapse(2)
    do j = 1, n
    do i = 1, n
      r_f(i,j,3,e) = r_f(i,j,3,e)                                              &
          + M_II(i,j) * (d(0,e) * w_0 + d(2,e) * L_00) * u_f(i,j,3,e)          &
          + M_II(i,j) *                 d(2,e) * L_0p  * u_f(i,j,4,e)
      r_f(i,j,4,e) = r_f(i,j,4,e)                                              &
          + M_II(i,j) * (d(0,e) * w_0 + d(2,e) * L_00) * u_f(i,j,4,e)          &
          + M_II(i,j) *                 d(2,e) * L_0p  * u_f(i,j,3,e)
    end do
    end do
    !$acc end loop
    !$acc loop collapse(2)
    do j = 1, n
    do i = 1, n
      r_f(i,j,5,e) = r_f(i,j,5,e)                                              &
          + M_II(i,j) * (d(0,e) * w_0 + d(3,e) * L_00) * u_f(i,j,5,e)          &
          + M_II(i,j) *                 d(3,e) * L_0p  * u_f(i,j,6,e)
      r_f(i,j,6,e) = r_f(i,j,6,e)                                              &
          + M_II(i,j) * (d(0,e) * w_0 + d(3,e) * L_00) * u_f(i,j,6,e)          &
          + M_II(i,j) *                 d(3,e) * L_0p  * u_f(i,j,5,e)
    end do
    end do
    !$acc end loop
  end do
  !$omp end do
  !$acc end parallel

  !.............................................................................
  ! Laplace factors

  ! Laplace in first  direction of face
  call AddFactoredMatrixProduct2D(L_I,w_I * w_0,d_f_1,u_f,r_f,tmp_f_1,tmp_f_2)
  ! Laplace in second direction of face
  call AddFactoredMatrixProduct2D(w_I,L_I * w_0,d_f_2,u_f,r_f,tmp_f_1,tmp_f_2)

  !.............................................................................
  ! Epilogue
  !$omp single
  deallocate(d_f_1,d_f_2)
  !$omp end single

end subroutine FaceToFace

!===============================================================================

end module Condensed_Primary_Part
