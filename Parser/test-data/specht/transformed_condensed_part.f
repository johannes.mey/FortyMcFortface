!> \file      eigenspace_condensed_part.f
!> \brief     Condensed part for condensed face eigenspace solver
!> \author    Immo Huismann
!> \date      2015/05/23
!> \copyright Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> \details
!> This module provides the primary part for the condensed system eigenspace
!> solver.
!===============================================================================

module Transformed_Condensed_Part
  use Kind_Parameters,             only: RDP
  use Constants,                   only: HALF

  use ACC_Parameters,              only: ACC_EXEC_QUEUE
  use Condensed_SEM_Operators,     only: CondensedOperators1D
  use Geometry_Coefficients,       only: GetLaplaceCoefficients
  use Element_Boundary_Parameters, only: N_FACES
  implicit none
  private

  public :: SubtractTransformedCondensedPart

contains

!-------------------------------------------------------------------------------
!> \brief   Subtracts the condensed part of the eigenspace system from r.
!> \author  Immo Huismann
!>
!> \details
!> Serves as a wrapper to the real routine, but with far fewer arguments.

subroutine SubtractTransformedCondensedPart(cop,d,D_inv,u_f,r_f)
  class(CondensedOperators1D), intent(in) :: cop !< condensed operators
  real(RDP), intent(in)    :: d(0:,:)            !< Helmholtz parameters
  real(RDP), intent(in)    :: D_inv(:,:,:,:)     !< eigenvalues of iHii
  real(RDP), intent(in)    :: u_f(:,:,:,:)       !< variable u on faces
  real(RDP), intent(inout) :: r_f(:,:,:,:)       !< result on faces

  integer :: n, n_element         ! points per edge, number of elements

  ! Prologue ...................................................................
  n         = size(D_inv,1)
  n_element = size(D_inv,4)

  ! computation ................................................................

  ! map from faces to inner element eigenspace.
  ! This is done with the transpose of the transformation matrix and the
  ! Helmholtz suboperator corresponding to the specific face

  call Operator(n,n_element,cop%S_T_L_I0,cop%S_T_L_Ip,d,u_f,D_inv,r_f)


end subroutine SubtractTransformedCondensedPart

!-------------------------------------------------------------------------------
!> \brief   Explicit size implementation of the transformed condensed part
!> \author  Immo Huismann
!>
!> \details
!> This implementation uses a small working set (one temporary), leading to few
!> load and store operations from memory.

subroutine Operator(n,n_element,S_T_L_I0,S_T_L_Ip,d,u_f,D_inv,r_f)
  integer,   intent(in)    :: n                !< points per edge
  integer,   intent(in)    :: n_element        !< number of elements
  real(RDP), intent(in)    :: S_T_L_I0(n)      !< \f$S^{T} L_{I0}\f$
  real(RDP), intent(in)    :: S_T_L_Ip(n)      !< \f$S^{T} L_{Ip}\f$
  real(RDP), intent(in)    :: d(0:3,n_element) !< Helmholtz coefficients
  real(RDP), intent(in)    :: u_f  (  n,n,N_FACES,n_element) !< u face values
  real(RDP), intent(in)    :: D_inv(n,n,n,n_element)         !< D^{-1}
  real(RDP), intent(inout) :: r_f  (  n,n,N_FACES,n_element) !< r face values

  real(RDP) :: v(n,n,n)
  real(RDP) :: L_0I_S(n), L_pI_S(n)
  integer   :: i,j,k,e

  ! Small structure exploitation
  L_0I_S = S_T_L_I0
  L_pI_S = S_T_L_Ip

  ! Loop over all elements
  !$acc parallel async(ACC_EXEC_QUEUE) present(d,D_inv,u_f,r_f)
  !$acc loop private(v)
  !$omp do private(i,j,k,e,v)
  do e = 1, n_element
    ! Gather contributions .....................................................
    !$acc loop collapse(3)
    do k = 1, n
    do j = 1, n
    do i = 1, n
      v(i,j,k) = d(1,e) * S_T_L_I0(i) * u_f(  j,k,1,e)                         &
               + d(1,e) * S_T_L_Ip(i) * u_f(  j,k,2,e)                         &
               + d(2,e) * S_T_L_I0(j) * u_f(i,  k,3,e)                         &
               + d(2,e) * S_T_L_Ip(j) * u_f(i,  k,4,e)                         &
               + d(3,e) * S_T_L_I0(k) * u_f(i,j,  5,e)                         &
               + d(3,e) * S_T_L_Ip(k) * u_f(i,j,  6,e)

      v(i,j,k) = D_inv(i,j,k,e) * v(i,j,k)
    end do
    end do
    end do
    !$acc end loop
    
    ! Scatter contributions ....................................................
    ! First direction
    !$acc loop collapse(2)
    do k = 1, n
    do j = 1, n
      do i = 1, n
        r_f(  j,k,1,e) = r_f(  j,k,1,e) - d(1,e) * L_0I_S(i) * v(i,j,k)
        r_f(  j,k,2,e) = r_f(  j,k,2,e) - d(1,e) * L_pI_S(i) * v(i,j,k)
      end do
    end do
    end do

    ! Second direction
    !$acc loop collapse(2)
    do k = 1, n
    do i = 1, n
      do j = 1, n
        r_f(i,  k,3,e) = r_f(i,  k,3,e) - d(2,e) * L_0I_S(j) * v(i,j,k)
        r_f(i,  k,4,e) = r_f(i,  k,4,e) - d(2,e) * L_pI_S(j) * v(i,j,k)
      end do
    end do
    end do
    !$acc end loop
    
    ! Third direction
    do k = 1, n
      !$acc loop collapse(2)
      do j = 1, n
      do i = 1, n
        r_f(i,j,  5,e) = r_f(i,j,  5,e) - d(3,e) * L_0I_S(k) * v(i,j,k)
        r_f(i,j,  6,e) = r_f(i,j,  6,e) - d(3,e) * L_pI_S(k) * v(i,j,k)
      end do
      end do
      !$acc end loop
    end do
  end do
  !$omp end do
  !$acc end loop
  !$acc end parallel

end subroutine Operator

!===============================================================================

end module Transformed_Condensed_Part
