! simple pragma

!$omp bla

! simple continued pragma

!$omp do &
more

! correct continued pragma

!$omp do&
!$omp more

!$omp do&
!$omp&more

!$omp do&
!$omp&really&
!$omp an&
!$omp awful&
!$omp&lot