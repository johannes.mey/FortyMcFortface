! normal continued line

a = b &
+ c

var&
iable = "value"

! continued line with comment

a = b & ! some comment
+ c

! continued line with blank line inbetween

a = b &

+ c

a = b &


+ c

! continued line with comment line inbetween

a = b &
! some comment
+ c

a = b &
! some comment
! more comment
+ c

a = b &
! comment, then space line

! more comment
+ c

end
