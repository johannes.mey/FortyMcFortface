a = "my&
& string"

a = 'my&
& string'

a = 'my&
& string"

! false positives
a = 'my'&
& string

a = "my"&
& string

a = "my'&
& string

a = 'my"&
& string