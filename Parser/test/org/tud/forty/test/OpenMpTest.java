package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

import java.io.File;
import java.util.Iterator;

public class OpenMpTest extends TestBase {
    @DataProvider(name = "OpenMP")
    public static Iterator<Object[]> fortranRuleProvider() {
        return ruleProvider("test-data/OpenMp");
    }

    @Test(dataProvider = "OpenMP")
    public void testOpenMpParser(File f) throws Exception {
        printFile(f);
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "OpenMP")
    public void testOpenMpPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

    @Test(dataProvider = "OpenMP")
    public void testOpenMpSimilarity(File f) throws Exception {
        testParse(f, false, false, true, true, Root.class);
    }

    @Test(dataProvider = "OpenMP")
    public void testOpenMpEquality(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }
}
