package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

import java.io.File;
import java.util.Iterator;

public class OpenAccTest extends TestBase {
    @DataProvider(name = "OpenACC")
    public static Iterator<Object[]> fortranRuleProvider() {
        return ruleProvider("test-data/OpenAcc");
    }

    @Test(dataProvider = "OpenACC", groups = {"parser"})
    public void testOpenAccParser(File f) throws Exception {
        printFile(f);
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "OpenACC", groups = {"prettyprinter"})
    public void testOpenAccPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, false, Root.class);
    }

    @Test(dataProvider = "OpenACC", groups = {"prettyprinter"})
    public void testOpenAccPrettyPrinterCompare(File f) throws Exception {
        testParse(f, true, false, true, true, Root.class);
    }

    @Test(dataProvider = "OpenACC", groups = {"prettyprinter"})
    public void testOpenAccPrettyPrinterCompareWithWhitespace(File f) throws Exception {
        testParse(f, true, false, true, false, Root.class);
    }
}
