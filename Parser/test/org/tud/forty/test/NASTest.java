package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

import java.io.File;
import java.util.Iterator;

public class NASTest extends TestBase {


    @DataProvider(name = "nas")
    public static Iterator<Object[]> fortranNasProvider() {
        return ruleProvider("test-data/nas");
    }

    @Test(dataProvider = "nas", groups = {"parser"})
    public void testNASParser(File f) throws Exception {
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "nas", groups = {"prettyprinter"})
    public void testNASPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

    @Test(dataProvider = "nas", groups = {"compare"})
    public void testCompareNASPrettyPrinter(File f) throws Exception {
        testParse(f, false, false, true, true, Root.class);
    }

    @Test(dataProvider = "nas", groups = {"compare"})
    public void testCompareNASPrettyPrinterWithSpaces(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }
}
