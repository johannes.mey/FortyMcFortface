package org.tud.forty.test;

import java.io.File;
import java.util.Iterator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

public class SpechtTest extends TestBase {


    @DataProvider(name = "specht")
    public static Iterator<Object[]> fortranNasProvider() {
        return ruleProvider("test-data/specht");
    }

    @Test(dataProvider = "specht", groups = {"parser"})
    public void testSpechtParser(File f) throws Exception {
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "specht", groups = {"prettyprinter"})
    public void testSpechtPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

    @Test(dataProvider = "specht", groups = {"compare"})
    public void testCompareSpechtPrettyPrinter(File f) throws Exception {
        testParse(f, false, false, true, true, Root.class);
    }

    @Test(dataProvider = "specht", groups = {"compare"})
    public void testCompareSpechtPrettyPrinterWithSpaces(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }

    @Test(dataProvider = "specht", groups = {"compare"})
    public void testParsePrintParseSpecht(File f) throws Exception {
        testParsePrintParse(f);
    }

}
