package org.tud.forty.test;

import java.io.File;
import java.util.Iterator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ContinuedLinesTest extends TestBase {

  @DataProvider(name = "continued_lines")
  public static Iterator<Object[]> fortranContinuedLinesProvider() {
    return ruleProvider("test-data/continued_lines");
  }

  @Test(dataProvider = "continued_lines", groups = {"parser"})
  public void testContinuedLinesSanitize(File f) throws Exception {
    testSanitize(f);
  }
}
