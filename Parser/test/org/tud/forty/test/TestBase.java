package org.tud.forty.test;

import org.testng.Assert;
import org.tud.forty.ast.*;
import org.tud.forty.parser.SlottableFortranParser;
import xtc.parser.Result;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TestBase {

    public static Iterator<Object[]> ruleProvider(String dir) {
        List<Object[]> files = new ArrayList<Object[]>();

        File folder = new File(dir);

        File[] fileArray = folder.listFiles();
        Arrays.sort(fileArray);

        for (File file : fileArray) {
            if (file.isFile()) {
                files.add(new Object[]{file});
            }
        }
        return files.iterator();
    }

    protected String trim(String s){
        StringBuffer b = new StringBuffer();
        for (String line : s.split("\n")) {
            for (String word : line.split("\\s")) {
                b.append(word);
            }
            b.append("\n");
        }
        return b.toString();
    }

    protected void printFile(File f) throws Exception{
        FileReader reader = new FileReader(f);
        BufferedReader br = new BufferedReader(reader);
        StringBuffer sb = new StringBuffer();

        String line;
        while ((line = br.readLine()) != null)
        {
            sb.append(line).append("\n");
        }

        System.out.println(sb.toString());
    }

    private String trimCode(String code) {
        System.out.println(">>" + code + "<<");
        String lastLine = code.split("\n")[code.split("\n").length-1];
        code = code.replaceAll("\n" + lastLine + "  ","\n");

        code = code.replaceAll("^\\p{Blank}*\n", "");
        code = code.replaceAll("\n\\p{Blank}*$", "");
        System.out.println(">>" + code + "<<");
        code = code.trim() + "\n";
        System.out.println(">>" + code + "<<");
        return code;
    }

    protected void testSanitize(File f) throws Exception {
        Assert.assertTrue(f.exists());

        Reader reader;

        FileReader t = new FileReader(f);
        StringBuilder sw = new StringBuilder();
        int i;
        while ((i = t.read()) != -1) {
            sw.append((char)i);
        }
        Assert.assertEquals(sw.toString(),SlottableFortranParser.removeLineBreaks(sw.toString()));

    }

    protected void testParse(File f, boolean pp, boolean ast, boolean compare, boolean ignore_spaces, Class clazz) throws Exception {
        testParse(f, pp, ast, compare, ignore_spaces, clazz, false);
    }

    private String reprint(Reader reader) throws Exception {
        SlottableFortranParser parser = SlottableFortranParser.createParser(reader, "");
        Result r = parser.proot(0);
        if (!r.hasValue()) {
            System.out.println(parser.format(r.parseError()));
            if (r.parseError().index > 0) {
                Assert.assertTrue(r.hasValue(), "ParseError:" + parser.location(r.parseError().index) + ":(" + r.parseError().index + ") " + r.parseError().msg + ".");
            } else {
                Assert.assertTrue(r.hasValue(), "ParseError:" + parser.location(r.parseError().index) + ":(" + r.parseError().index + ") " + r.parseError().msg + ".");
            }
        }
        PrettyPrinter pp = new PrettyPrinter("  ", false);
        ((ASTNode) r.semanticValue()).prettyPrint(pp);
        return pp.toString();
    }

    protected void testParsePrintParse(File file) throws Exception {
        FileReader firstReader = new FileReader(file);
        String print1 = reprint(firstReader);
        //System.out.println(print1);
        StringReader secondReader = new StringReader(print1);
        String print2 = reprint(secondReader);
        Assert.assertEquals(print2,print1);
    }

    protected void testParse(File file, boolean testPrettyPrinter, boolean printAst, boolean compareOutput, boolean compareOutputIgnoringSpaces, Class clazz, boolean trim) throws Exception {
        Assert.assertTrue(file.exists());

        Reader reader;

        if (trim) {
            FileReader t = new FileReader(file);
            StringWriter sw = new StringWriter();
            int i;
            while ((i = t.read()) != -1) {
                sw.append((char)i);
            }
            reader = new StringReader(trimCode(sw.toString()));
        } else {
            reader = new FileReader(file);
        }



        SlottableFortranParser parser = SlottableFortranParser.createParser(reader, "");
        Result r;
        if(clazz.equals(Expr.class)) {
            r = parser.pexpr(0);
        } else if (clazz.equals(ExecutableConstruct.class)) {
            r = parser.pexecutable_construct(0);
        } else if (clazz.equals(ExecutionPartConstruct.class)) {
            r = parser.pexecution_part_construct(0);
        } else {
            r = parser.proot(0);
        }
        if (!r.hasValue()) {
            if (r.parseError().index > 0) {
                System.err.println(parser.format(r.parseError()));
                Assert.assertTrue(r.hasValue(), "ParseError in " + file.getName());
            } else {
                Assert.assertTrue(r.hasValue(), "ParseError in " + file.getName() + ".");
            }
        }

        ((ASTNode) r.semanticValue()).fixTreeStructure();
        ((ASTNode) r.semanticValue()).checkTreeStructure();

        if (testPrettyPrinter || compareOutput) {
            PrettyPrinter s = new PrettyPrinter("  ", false);
            ((ASTNode) r.semanticValue()).prettyPrint(s);

            int ln = 1;
            if (testPrettyPrinter) {
                System.out.println("\n===============================================");
                System.out.println("== " + file.getName());
                System.out.println("===============================================");
                for (String sl : s.toString().split("\n")) {
                    System.out.println(String.format("%03d  ", ln++) + sl);
                }
            }
            if (compareOutput) {
                reader.close();
                reader = new FileReader(file);
                BufferedReader br = new BufferedReader(reader);
                StringBuffer sb = new StringBuffer();

                String line;
                while ((line = br.readLine()) != null)
                {
                    sb.append(line).append("\n");
                }

                if (compareOutputIgnoringSpaces) {
                    Assert.assertEquals(trim(s.toString()), SlottableFortranParser.removeLineBreaks(trim(sb.toString())));
                } else {
                    Assert.assertEquals(s.toString(), sb.toString());
                }
            }
        }
        if (printAst) {
            System.out.println("===============================================");
            if(clazz.equals(Expr.class)) {
                System.out.println(((Expr) r.semanticValue()).getAstStringWithSource());
            } else if(clazz.equals(ExecutableConstruct.class)) {
                System.out.println(((ExecutableConstruct) r.semanticValue()).getAstStringWithSource());
            } else if(clazz.equals(ExecutionPartConstruct.class)) {
                System.out.println(((ExecutionPartConstruct) r.semanticValue()).getAstStringWithSource());
            } else {
                System.out.println(((Root) r.semanticValue()).getAstStringWithSource());
            }
            System.out.println("===============================================");
        }
        reader.close();
    }
}
