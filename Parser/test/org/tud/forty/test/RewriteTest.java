package org.tud.forty.test;

import java.io.File;
import java.util.Iterator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

public class RewriteTest extends TestBase {

    @DataProvider(name = "rewrites")
    public static Iterator<Object[]> fortranRuleProvider() {
        return ruleProvider("test-data/rewrites");
    }

    @Test(dataProvider = "rewrites", groups = {"parser"})
    public void testRuleParser(File f) throws Exception {
        printFile(f);
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "rewrites", groups = {"prettyprinter"})
    public void testRulePrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }
}
