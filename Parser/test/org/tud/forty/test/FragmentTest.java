package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.ExecutableConstruct;
import org.tud.forty.ast.ExecutionPartConstruct;
import org.tud.forty.ast.Expr;

import java.io.File;
import java.util.Iterator;

public class FragmentTest extends TestBase {

    @DataProvider(name = "exprs")
    public static Iterator<Object[]> fortranExprProvider() {
        return ruleProvider("test-data/fragments/Expr");
    }

    @DataProvider(name = "executableconstruct")
    public static Iterator<Object[]> fortranExecutableConstructProvider() {
        return ruleProvider("test-data/fragments/ExecutableConstruct");
    }

    @Test(dataProvider = "exprs")
    public void testFragmentExprParser(File f) throws Exception {
        testParse(f, false, true, false, true, Expr.class);
    }

    @Test(dataProvider = "executableconstruct")
    public void testFragmentExecutableConstructParser(File f) throws Exception {
        testParse(f, false, true, false, true, ExecutableConstruct.class, true);
    }

    @Test(dataProvider = "executableconstruct")
    public void testFragmentExecutionPartConstructParser(File f) throws Exception {
        testParse(f, false, true, false, true, ExecutionPartConstruct.class, true);
    }

}
