package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

import java.io.File;
import java.util.Iterator;

public class RuleTest extends TestBase {

    @DataProvider(name = "rules")
    public static Iterator<Object[]> fortranRuleProvider() {
        return ruleProvider("test-data/rules");
    }

    @DataProvider(name = "full")
    public static Iterator<Object[]> fortranFullProvider() {
        return ruleProvider("test-data/full");
    }

    @DataProvider(name = "inc")
    public static Iterator<Object[]> fortranIncProvider() {
        return ruleProvider("test-data/inc");
    }

    @Test(dataProvider = "rules", groups = {"parser"})
    public void testRuleParser(File f) throws Exception {
        printFile(f);
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "rules", groups = {"prettyprinter"})
    public void testRulePrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

//    @Test(dataProvider = "full")
//    public void testFull(File f) throws Exception {
//        testParse(f, true, true);
//    }

    @Test(dataProvider = "inc", groups = {"parser"})
    public void testIncrementalParser(File f) throws Exception {
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "inc", groups = {"prettyprinter"})
    public void testIncrementalPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

    @Test(dataProvider = "inc", groups = {"compare"})
    public void testCompareIncrementalPrettyPrinter(File f) throws Exception {
        testParse(f, false, false, true, true, Root.class);
    }

    @Test(dataProvider = "inc", groups = {"compare"})
    public void testCompareIncrementalPrettyPrinterWithSpaces(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }
}
