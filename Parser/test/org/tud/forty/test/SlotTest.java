package org.tud.forty.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tud.forty.ast.Root;

import java.io.File;
import java.util.Iterator;

public class SlotTest extends TestBase {
    @DataProvider(name = "Slots")
    public static Iterator<Object[]> fortranSlotTestProvider() {
        return ruleProvider("test-data/slots");
    }

    @Test(dataProvider = "Slots")
    public void testSlotsParser(File f) throws Exception {
        printFile(f);
        testParse(f, false, true, false, true, Root.class);
    }

    @Test(dataProvider = "Slots")
    public void testSlotsPrettyPrinter(File f) throws Exception {
        testParse(f, true, false, false, true, Root.class);
    }

    @Test(dataProvider = "Slots")
    public void testSlotsSimilarity(File f) throws Exception {
        testParse(f, false, false, true, true, Root.class);
    }

    @Test(dataProvider = "Slots")
    public void testSlotsEquality(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }

    @Test(dataProvider = "Slots")
    public void testSlotsName(File f) throws Exception {
        testParse(f, false, false, true, false, Root.class);
    }

}
