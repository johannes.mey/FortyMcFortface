package org.tud.forty;

import org.tud.forty.parser.SlottableFortranParser;
import xtc.parser.Result;

import java.io.*;

public class DrFort {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("No file name provided!");
            System.err.println("Usage: java -jar DrFort <filename>");
            System.exit(-1);
        } else {
            File f = new File(args[0]);
            if (!f.exists()) {
                System.err.println("file '" + args[0] + "' does not exist!");
                System.err.println("Usage: java -jar DrFort <filename>");
                System.exit(-1);
            } else if (!f.isFile()) {
                System.err.println("'" + args[0] + "' is no file!");
                System.err.println("Usage: java -jar DrFort <filename>");
                System.exit(-1);
            }
            try {
                Reader reader = new FileReader(f);
                SlottableFortranParser parser = new SlottableFortranParser(reader, "");
                Result r = parser.proot(0);

                if (r.hasValue()) {
                    ((org.tud.forty.ast.ASTNode)(r.semanticValue())).fixTreeStructure();
                    drast.views.gui.DrASTGUI debugger = new drast.views.gui.DrASTGUI(r.semanticValue());
                    debugger.run();
                } else {
                    System.err.println("Problems while parsing file '" + args[0] + "'!");
                    System.err.println(r.parseError().index + ": " + r.parseError().msg);
                }
            } catch (FileNotFoundException e) {
                System.err.println("file '" + args[0] + "' does not exist!");
                System.err.println("Usage: java -jar DrFort <filename>");
            } catch (IOException e) {
                System.err.println("Problems while parsing file '" + args[0] + "'!");
                e.printStackTrace(System.err);
            }
        }
    }
}
