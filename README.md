Forty McFortface is a parser for the Fortran programming language according to
the Fortran 2008 ISO standard. Additionally, the pragma languages OpenACC 2.5
and OpenMP 4.0 are supported.

The parser includes a Fortran grammer written in JastAdd and generates syntax
trees according to it.

Besides the possibility to extend the parser specification itself, it is
possible to parse only *fragments* of Fortran, e.g. expressions or statements
and to put placeholders, so-called *slots* into the program code instead of
those fragments.

Please note that the parser is in a very early stage of development and may not
be working as expected in some cases.

License:

The parser is avialable under a two-clause BSD license.

It uses four other projects:
* Forty generates a [JastAdd](https://bitbucket.org/jastadd/jastadd2) AST. JastAdd is available under a modified BSD license.
* The test cases for the parser originate from [OpenFortran](https://github.com/OpenFortranProject/open-fortran-parser/) and are available under a BSD license.
* The debugging program [DrAST](https://bitbucket.org/jastadd/drast) from  is also available under a BSD license.
* The [RATS!](https://cs.nyu.edu/rgrimm/xtc) parser generator is used. Its license is GPL, but the runtime libraries required are LGPL.
